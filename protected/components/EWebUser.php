<?php
class EWebUser extends CWebUser{
    protected $_model;
   
    public function isAdministrator(){
        return Yii::app()->user->role == UserLevel::ADMINISTRATOR;
    }
    public function isAdminbidang(){
        return Yii::app()->user->role == UserLevel::ADMINBIDANG;
    }
     public function isVisitor(){
        return Yii::app()->user->role == UserLevel::VISITOR;
    }
 
    protected function loadUser(){
        if ($this->_model === null){
            $this->_model = user::model()->findByPk($this->id);
        }
        return $this->_model;
    }
    public function getRole(){
        return $this->getState('level');
    }

    public function getRecord(){
        return $this->getState('record');
    }    
}
?>