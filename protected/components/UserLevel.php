<?php
class UserLevel {
    const ADMINISTRATOR  = 1;
    const ADMINBIDANG  = 2;
    const VISITOR  = 3;
    
    public static function getLabel ($user){
        if ($status == self::ADMINISTRATOR)
            return ADMINISTRATOR;
        if ($status == self::ADMINBIDANG)
            return ADMINBIDANG;
        if ($status == self::VISITOR)
            return VISITOR;
    }
    public static function getList(){
        return array (
        self::ADMINISTRATOR => self::getLabel(self::ADMINISTRATOR),
        self::ADMINBIDANG => self::getLabel(self::ADMINBIDANG),
        self::VISITOR => self::getLabel(self::VISITOR),
        );
    }
}
?>