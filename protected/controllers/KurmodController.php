<?php

class KurmodController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>'Yii::app()->user->isAdministrator()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>'Yii::app()->user->isAdminbidang()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>'Yii::app()->user->isVisitor()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),			
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$kurmod=KurmodDetail::model()->findByAttributes(array('km_id'=>$id));
                $haha=new CDbCriteria;
                $criteria=new CDbCriteria;
                $criteria->compare('km_id',$id);
 
                $kurmoddetail=new CActiveDataProvider('KurmodDetail',
                     array('criteria'=>$criteria,)
                );
		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'kurmoddetail'=>$kurmoddetail,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Kurmod;
		$model2=new KurmodDetail;
		$model->created = date('Y-m-d h-m-s');
		$model->updated = '0000-00-00 00-00-00';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kurmod']))
		{
			$model->attributes=$_POST['Kurmod'];
			if($model->save()){
				if(isset($_POST['nama_dokumen']))
				{
				$total = count($_POST['nama_dokumen']);
		    		for ($i = 0; $i <= $total; $i++)
		    		{
		    			if(isset($_POST['nama_dokumen'][$i]))
		    			{
							$jiakakak = new KurmodDetail;
							$dariPc = CUploadedFile::getInstancesByName('cover_dokumen');
							$jiakakak->km_id=$model->km_id;
		        			$jiakakak->nama_dokumen = $_POST['nama_dokumen'][$i];
							$jiakakak->jns_dokumen = $_POST['jns_dokumen'][$i];
							$jiakakak->tanggal = $_POST['tanggal'][$i];
							$jiakakak->tim_penyusun = $_POST['tim_penyusun'][$i];
							$jiakakak->cover_dokumen = $_POST['nama_dokumen'][$i].".jpg";
							$jiakakak->created = date('Y-m-d h-m-s');
							$jiakakak->updated = '0000-00-00 00-00-00';
							if($jiakakak->save()){
								if(isset($dariPc) && count($dariPc) > 0)
								{
									foreach($dariPc as $a=>$aa)
									{
										$aa->saveAs(Yii::app()->basePath.'/../Dokumen/CoverDokumen/'.$jiakakak->nama_dokumen.".jpg");
									}
								}
							};
		    			}	
					}
					$this->redirect(array('view','id'=>$model->km_id));
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
			'model2'=>$model2,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-m-s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Kurmod']))
		{
			$model->attributes=$_POST['Kurmod'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->km_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Kurmod');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Kurmod('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Kurmod']))
			$model->attributes=$_GET['Kurmod'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Kurmod the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Kurmod::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Kurmod $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kurmod-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
