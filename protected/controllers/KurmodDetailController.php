<?php

class KurmodDetailController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','upload','cover','hapus','viewDokumen','viewLog','createKurmod'),
				'expression'=>'Yii::app()->user->isAdministrator()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','upload','cover','hapus'),
				'expression'=>'Yii::app()->user->isAdminbidang()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','upload','cover','hapus'),
				'expression'=>'Yii::app()->user->isVisitor()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),			
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionViewLog($id)
	{
		$log=new LogKurmod('getKurmod');
		$log->unsetAttributes();  // clear any default values
		if(isset($_GET['LogKurmod']))
			$log->attributes=$_GET['LogKurmod'];

		$this->render('viewLog',array(
			'model'=>$this->loadModel($id),
			'log'=>$log
		));
	}

	public function actionViewDokumen($id)
	{	
		$this->layout="tes";
		$this->render('viewDokumen',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreateKurmod($bidang)
	{
		$model=new KurmodDetail;
		$model->created = date('Y-m-d h-m-s');
		$model->updated = '0000-00-00 00-00-00';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KurmodDetail']))
		{
			$model->attributes=$_POST['KurmodDetail'];

			$tmp=CUploadedFile::getInstance($model,'cover_dokumen'); 
			$model->cover_dokumen = 'Cover-('.date('ymdHis').')'.$model->nama_dokumen.'.'.$tmp->extensionName; 
			$tmp->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/CoverDokumen/'.$model->cover_dokumen);

			$tmp2=CUploadedFile::getInstance($model,'file_dokumen'); 
			$model->file_dokumen = date('ymdHis').$model->nama_dokumen.'.'.$tmp2->extensionName; 
			$tmp2->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/CoverDokumen/'.$model->file_dokumen);

			if($model->save()){
				$this->redirect(array('/pelatihan/view?type='.$bidang));
			}
		}

		$this->render('createKurmod',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($prof_id)
	{
		$model=new KurmodDetail;
		$model->created = date('Y-m-d h-m-s');
		$model->updated = '0000-00-00 00-00-00';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KurmodDetail']))
		{
			$model->attributes=$_POST['KurmodDetail'];
			$namaDokuemn=$_POST['KurmodDetail']['nama_dokumen'];
			$jnsDokumen=$_POST['KurmodDetail']['jns_dokumen'];
			$timPenyusun=$_POST['KurmodDetail']['tim_penyusun'];
			$coverDokumen=$_POST['KurmodDetail']['cover_dokumen'];
			$fileDokumen=$_POST['KurmodDetail']['file_dokumen'];
			foreach($namaDokuemn as $key => $val) {

				$tmp[$key]=CUploadedFile::getInstance($model,'cover_dokumen['.$key.']'); 
				$coverDokumenName[$key] = 'Cover-('.date('ymdHis').')'.$namaDokuemn[$key].'.'.$tmp[$key]->extensionName; 
				$tmp[$key]->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/CoverDokumen/'.$coverDokumenName[$key]);

				$tmp2[$key]=CUploadedFile::getInstance($model,'file_dokumen['.$key.']'); 
				$fileDokumenName[$key] = date('ymdHis').$namaDokuemn[$key].'.'.$tmp2[$key]->extensionName; 
				$tmp2[$key]->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/CoverDokumen/'.$fileDokumenName[$key]);

				YII::app()->db->createCommand()->insert('kurmod_detail', array(
				    'prof_id'=>$prof_id,
				    'nama_dokumen'=>$namaDokuemn[$key],	
				    'jns_dokumen'=>$jnsDokumen[$key],	
				    'tim_penyusun'=>$timPenyusun[$key],
				    'cover_dokumen'=>$coverDokumenName[$key],
				    'file_dokumen'=>$fileDokumenName[$key],
				    'created'=>date('Y-m-d H:i:s')
				));
			}			

			// if($model->save()){
			$this->redirect(array('profilPelatihan/'.$prof_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-i-s');
		$lastName = $model->nama_dokumen;
		$profId = $model->prof_id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$cekLog = LogKurmod::model()->find('kurmodId='.$id.' ORDER BY id DESC');

		if(isset($_POST['KurmodDetail']))
		{
			$model->attributes=$_POST['KurmodDetail'];

			// Check log profile name
			if ($cekLog){
				if ($cekLog->nameChange != $model->nama_dokumen){
					$chage = new LogKurmod;
					$chage->profileId=$profId;
					$chage->kurmodId=$id;
					$chage->initialName=$lastName;
					$chage->nameChange=$model->nama_dokumen;
					$chage->createTime=date('Y-m-d H:i:s');
					$chage->createdBy=Yii::app()->user->id;
					$chage->save(false);
				}
			} else {
				$chage = new LogKurmod;
				$chage->profileId=$profId;
				$chage->kurmodId=$id;
				$chage->initialName=$lastName;
				$chage->nameChange=$model->nama_dokumen;
				$chage->createTime=date('Y-m-d H:i:s');
				$chage->createdBy=Yii::app()->user->id;
				$chage->save(false);				
			}
			// end check

			if($model->save())
				$this->redirect(array('view','id'=>$model->kmd_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionCover($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-i-s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KurmodDetail']))
		{
			$model->attributes=$_POST['KurmodDetail'];
			if(strlen(trim(CUploadedFile::getInstance($model,'cover_dokumen'))) > 0) 
			{ 
				$tmp=CUploadedFile::getInstance($model,'cover_dokumen'); 
				$model->cover_dokumen='Cover-'.date('ymdHis').$model->nama_dokumen.'-'.'('.date('y-m-d').')'.'.'.$tmp->extensionName; 
			}			
			if($model->save())
				if(strlen(trim($model->cover_dokumen)) > 0) {
					$tmp->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/CoverDokumen/'.$model->cover_dokumen);
				}
				$this->redirect(array('view','id'=>$model->prof_id));
		}

		$this->render('updateCover',array(
			'model'=>$model,
		));
	}

	public function actionUpload($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-i-s');
		$lastName = $model->file_dokumen;
		$profId = $model->prof_id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$cekLog = LogKurmod::model()->find('kurmodId='.$id.' ORDER BY id DESC');

		if(isset($_POST['KurmodDetail']))
		{
			$model->attributes=$_POST['KurmodDetail'];
			if(strlen(trim(CUploadedFile::getInstance($model,'file_dokumen'))) > 0) 
			{ 
				$tmp=CUploadedFile::getInstance($model,'file_dokumen'); 
				$model->file_dokumen='Kurmod-'.date('ymdHis').$model->nama_dokumen.'-'.'('.date('y-m-d').')'.'.'.$tmp->extensionName; 
			}

			// Check log profile name
			if ($cekLog){
				if ($cekLog->nameChange != $model->file_dokumen){
					$chage = new LogKurmod;
					$chage->profileId=$profId;
					$chage->kurmodId=$id;					
					$chage->initialFile=$lastName;
					$chage->fileChange=$model->file_dokumen;
					$chage->createTime=date('Y-m-d H:i:s');
					$chage->createdBy=Yii::app()->user->id;
					$chage->save(false);
				}
			} else {
				$chage = new LogKurmod;
				$chage->profileId=$profId;
				$chage->kurmodId=$id;				
				$chage->initialFile=$lastName;
				$chage->fileChange=$model->file_dokumen;
				$chage->createTime=date('Y-m-d H:i:s');
				$chage->createdBy=Yii::app()->user->id;
				$chage->save(false);				
			}
			// end check

			if($model->save())
				if(strlen(trim($model->file_dokumen)) > 0) {
					$tmp->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/CoverDokumen/'.$model->file_dokumen);
				}
				$this->redirect(array('view','id'=>$model->prof_id));
		}

		$this->render('updateDokumen',array(
			'model'=>$model,
		));
	}	

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionHapus($id)
	{
		$model = $this->loadModel($id);
		$model->delete();
		$this->redirect(array('profilPelatihan/'.$model->prof_id));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KurmodDetail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KurmodDetail('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KurmodDetail']))
			$model->attributes=$_GET['KurmodDetail'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return KurmodDetail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=KurmodDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param KurmodDetail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kurmod-detail-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
