<?php

class PelaksanaanPlthController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','hapus','calendarEvents','viewDetail','selectProfile'),
				'expression'=>'Yii::app()->user->isAdministrator()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','viewDetail','hapus','calendarEvents'),
				'expression'=>'Yii::app()->user->isAdminbidang()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','hapus','calendarEvents','viewDetail'),
				'expression'=>'Yii::app()->user->isVisitor()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),			
		);
	}

	public function actionSelectProfile()
	{
		$bidang = $_POST['PelaksanaanPlth']['bidang'];
		$list = ProfilPelatihan::model()->findAll('bidang = :bidang order by nama_pelatihan ASC', array(':bidang'=>$bidang));
		$list = CHtml::listData($list,'prof_id','nama_pelatihan');

		echo CHtml::tag('option',array('value'=>''),'-- Pilih Profil --', true);

		foreach($list as $value=>$nama){
			echo CHtml::tag('option',array('value'=>$value),CHtml::encode($nama), true);
		}
	}

	public function actionCalendarEvents()
	{	 	
	 	$items = array();
	 	$model=PelaksanaanPlth::model()->findAll();	
		foreach ($model as $value) {
			$items[]=array(
				'id'=>$value->pplth_id,
				'title'=>$value->nm_pelatihan,
				'start'=>$value->tgl_plaksana,
				'end'=>$value->tgl_akhir,
			);
		}
	    echo CJSON::encode($items);
	    Yii::app()->end();
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		
		if (@$_GET['asModal']==true)
		{
			$this->renderPartial('view',
				array('model'=>$this->loadModel($id)),false,true
			);
		}
		else{
			$this->render('view',array(
				'model'=>$this->loadModel($id),
			));
		}
	}

	public function actionViewDetail($id)
	{		
		$Peserta=new PesertaPlthn('getDetail');
		$Peserta->unsetAttributes();  // clear any default values
		if(isset($_GET['PesertaPlthn']))
			{$Peserta->attributes=$_GET['PesertaPlthn'];}
		
		$modelPlth=$this->loadModel($id);
		if(isset($_POST['PelaksanaanPlth']))
		{
			$modelPlth->attributes=$_POST['PelaksanaanPlth'];
			if($modelPlth->save())
				$this->redirect(array('viewDetail','id'=>$id));
		}

		$this->render('viewDetail',array(
			'model'=>$this->loadModel($id),
			'Peserta'=>$Peserta,
			'modelPlth'=>$modelPlth
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PelaksanaanPlth;
		$model->created = date('Y-m-d h-m-s');
		$model->updated = '0000-00-00 00-00-00';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PelaksanaanPlth']))
		{
			$model->attributes=$_POST['PelaksanaanPlth'];
			if(strlen(trim(CUploadedFile::getInstance($model,'brt_acara'))) > 0) 
						{ 
							$tmp=CUploadedFile::getInstance($model,'brt_acara'); 
							$model->brt_acara=$model->nm_pelatihan.'-'.'('.date('y-m-d').')'.'.'.$tmp->extensionName; 
						}
			if($model->save()){
				if(strlen(trim($model->brt_acara)) > 0) {
					$tmp->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/BeritaAcara/'.$model->brt_acara);
					}
			}
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-m-s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PelaksanaanPlth']))
		{
			$model->attributes=$_POST['PelaksanaanPlth'];
			if($model->save())
				$this->redirect(array('viewDetail','id'=>$model->pplth_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	public function actionHapus($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(array('index'));
	}
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PelaksanaanPlth');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PelaksanaanPlth('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PelaksanaanPlth']))
			$model->attributes=$_GET['PelaksanaanPlth'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PelaksanaanPlth the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PelaksanaanPlth::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PelaksanaanPlth $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pelaksanaan-plth-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
