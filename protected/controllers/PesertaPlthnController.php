<?php

class PesertaPlthnController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>'Yii::app()->user->isAdministrator()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>'Yii::app()->user->isAdminbidang()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'expression'=>'Yii::app()->user->isVisitor()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),			
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($prof_id)
	{
		$model=new PesertaPlthn;
		$model->created = date('Y-m-d h-m-s');
		$model->updated = '0000-00-00 00-00-00';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PesertaPlthn']))
		{
			$model->attributes=$_POST['PesertaPlthn'];
			$model->pel_id=$prof_id;
			$model->pelatihan=Yii::app()->db->createCommand("SELECT nm_pelatihan FROM pelaksanaan_plth WHERE pplth_id = $prof_id")->queryScalar();;
			$model->balai_diklat=Yii::app()->db->createCommand("SELECT lokasi_plthn FROM pelaksanaan_plth WHERE pplth_id = $prof_id")->queryScalar();;
			$model->tanggal_mulai_diklat=Yii::app()->db->createCommand("SELECT tgl_plaksana FROM pelaksanaan_plth WHERE pplth_id = $prof_id")->queryScalar();;
			$model->tanggal_selesai_diklat=Yii::app()->db->createCommand("SELECT tgl_akhir FROM pelaksanaan_plth WHERE pplth_id = $prof_id")->queryScalar();;
			if($model->save())
				$this->redirect(array('view','id'=>$model->psrt_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-m-s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PesertaPlthn']))
		{
			$model->attributes=$_POST['PesertaPlthn'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->psrt_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PesertaPlthn');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PesertaPlthn('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PesertaPlthn']))
			$model->attributes=$_GET['PesertaPlthn'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PesertaPlthn the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PesertaPlthn::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PesertaPlthn $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='peserta-plthn-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
