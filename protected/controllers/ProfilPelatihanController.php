<?php

class ProfilPelatihanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','cover','viewLog','archive','viewPelatihan'),
				'expression'=>'Yii::app()->user->isAdministrator()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','cover'),
				'expression'=>'Yii::app()->user->isAdminbidang()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','cover'),
				'expression'=>'Yii::app()->user->isVisitor()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),			
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	public function actionViewPelatihan($type)
	{
		$model=new KurmodDetail('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KurmodDetail']))
			$model->attributes=$_GET['KurmodDetail'];

		$this->render('viewPelatihan',array(
			'model'=>$model,
		));		
	}

	public function actionArchive($id)
	{
		$model = $this->loadModel($id);
		$model->isActive=0;
		$model->updated=date('Y-m-d');
		if($model->save()){
			$chage = new LogProfile;
			$chage->profileId=$id;
			$chage->initialName='-';
			$chage->nameChange=$model->nama_pelatihan;
			$chage->createTime=date('Y-m-d H:i:s');
			$chage->createdBy=Yii::app()->user->id;
			$chage->save(false);

			$modelModul = KurmodDetail::model()->findAll('prof_id='.$id);
			foreach ($modelModul as $data) {
				$dataModul = KurmodDetail::model()->findByPk($data->kmd_id);
				$dataModul->isActive=0;
				$dataModul->updated=date('Y-m-d');
				$dataModul->save(false);
			}

			$this->redirect(array('view','id'=>$id));
		}
	}

	public function actionView($id)
	{
		$Kurmod=new KurmodDetail('Kurmod');
		$Kurmod->unsetAttributes();  // clear any default values
		if(isset($_GET['Kurmod']))
			$Kurmod->attributes=$_GET['Kurmod'];

		$PlkPlt=new PelaksanaanPlth('Pelaksana');
		$PlkPlt->unsetAttributes();  // clear any default values
		if(isset($_GET['PelaksanaanPlth']))
			$PlkPlt->attributes=$_GET['PelaksanaanPlth'];

		switch ($_GET['type']) {
			case 'profile':
				$type = 'view_profile';
				break;
			case 'kurikulum':
				$type = 'view_kurikulum';
				break;
			case 'modul':
				$type = 'view_modul';
				break;
			case 'bahan_tayang':
				$type = 'view_bahan_tayang';
				break;
			case 'other':
				$type = 'view_other';
				break;
		}
		$this->render($type,array(
			'model'=>$this->loadModel($id),
			'Kurmod'=>$Kurmod,
			'PlkPlt'=>$PlkPlt
		));
	}

	public function actionViewLog($id)
	{
		$log=new LogProfile('getProfile');
		$log->unsetAttributes();  // clear any default values
		if(isset($_GET['LogProfile']))
			$log->attributes=$_GET['LogProfile'];

		$this->render('viewLog',array(
			'model'=>$this->loadModel($id),
			'log'=>$log
		));
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProfilPelatihan;
		$model->created = date('Y-m-d h-i-s');
		$model->updated = '0000-00-00 00-00-00';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProfilPelatihan']))
		{
			$model->attributes=$_POST['ProfilPelatihan'];
			$model->nama_pelatihan=strtoupper($model->nama_pelatihan);
			// if(strlen(trim(CUploadedFile::getInstance($model,'pedoman'))) > 0) 
			// 			{ 
			// 				$tmp6=CUploadedFile::getInstance($model,'pedoman'); 
			// 				$model->pedoman=$model->nama_pelatihan.'-'.'('.date('y-m-d').')'.'.'.$tmp6->extensionName; 
			// 			}
			if($model->save()){
				// if(strlen(trim($model->pedoman)) > 0) {
				// 	$tmp6->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/Pedoman/'.$model->pedoman);
				// 	}
				$this->redirect(array('view?id='.$model->prof_id.'&type=profile'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-m-s');
		$lastName = $model->nama_pelatihan;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProfilPelatihan']))
		{

			$cekLog = LogProfile::model()->find('profileId='.$id.' ORDER BY id DESC');

			$model->attributes=$_POST['ProfilPelatihan'];
			$model->nama_pelatihan=strtoupper($model->nama_pelatihan);			

			// Check log profile name
			if ($cekLog){
				if ($cekLog->nameChange != $model->nama_pelatihan){
					$chage = new LogProfile;
					$chage->profileId=$id;
					$chage->initialName=$lastName;
					$chage->nameChange=$model->nama_pelatihan;
					$chage->createTime=date('Y-m-d H:i:s');
					$chage->createdBy=Yii::app()->user->id;
					$chage->save(false);
				}
			} else {
				$chage = new LogProfile;
				$chage->profileId=$id;
				$chage->initialName=$lastName;
				$chage->nameChange=$model->nama_pelatihan;
				$chage->createTime=date('Y-m-d H:i:s');
				$chage->createdBy=Yii::app()->user->id;
				$chage->save(false);				
			}
			// end check

			if($model->save())
				$this->redirect(array('view?id='.$model->prof_id.'&type=profile'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionCover($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-i-s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProfilPelatihan']))
		{
			$model->attributes=$_POST['ProfilPelatihan'];
			if(strlen(trim(CUploadedFile::getInstance($model,'coverImage'))) > 0) 
			{ 
				$tmp=CUploadedFile::getInstance($model,'coverImage'); 
				$model->coverImage='Cover-'.$model->nama_pelatihan.'-'.'('.date('y-m-d').')'.'.'.$tmp->extensionName; 
			}			
			if($model->save())
				if(strlen(trim($model->coverImage)) > 0) {
					$tmp->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/CoverDokumen/'.$model->coverImage);
				}
				$this->redirect(array('view','id'=>$model->prof_id));
		}

		$this->render('updateCover',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
     	foreach (KurmodDetail::getKurmod($id) as $data) {
			$this->loadKurmod($data['kmd_id'])->delete();
     	}

		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		
		$model=new ProfilPelatihan('search');
		if(isset($_GET['ProfilPelatihan']))
			$model->attributes=$_GET['ProfilPelatihan'];

		$dataProvider=new CActiveDataProvider('ProfilPelatihan');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ProfilPelatihan('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProfilPelatihan']))
			$model->attributes=$_GET['ProfilPelatihan'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ProfilPelatihan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ProfilPelatihan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadKurmod($id)
	{
		$model=KurmodDetail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ProfilPelatihan $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='profil-pelatihan-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
