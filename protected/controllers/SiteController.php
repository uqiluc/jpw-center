<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be Kerjasama via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionPelatihan()
	{
		$this->render('pelatihan');
	}

	public function actionKalendar()
	{
		$this->render('kalendar');
	}

	public function actionDocuments()
	{
		$this->render('dokumen');
	}

	public function actionLsp()
	{
		$this->render('lsp');
	}

	public function actionKerjasama()
	{
		$this->render('kerjasama');
	}

	public function actionProgramevaluasi()
	{
		$this->render('programevaluasi');
	}

	public function actionLspType()
	{
		$model=new NaskahDinas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NaskahDinas']))
			$model->attributes=$_GET['NaskahDinas'];

		$this->render('lsptype',array(
			'model'=>$model,
		));		
	}

	public function actionViewKerjasama()
	{
		$model=new NaskahDinas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NaskahDinas']))
			$model->attributes=$_GET['NaskahDinas'];

		$this->render('viewKerjasama',array(
			'model'=>$model,
		));		
	}

	public function actionViewDokumen()
	{
		$model=new NaskahDinas('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NaskahDinas']))
			$model->attributes=$_GET['NaskahDinas'];

		$this->render('viewDokumen',array(
			'model'=>$model,
		));		
	}

	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		if(!Yii::app()->user->isGuest)
		{
			$model=new PelaksanaanPlth('search');
			$model->unsetAttributes();  // clear any default values
			if(isset($_GET['PelaksanaanPlth']))
				$model->attributes=$_GET['PelaksanaanPlth'];

			$this->render('index',array(
				'model'=>$model,
			));
		} else {
			$this->actionLogin();
		}			
	}

	public function actionLibrary()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$LogProfile=new LogProfile('search');
		$LogProfile->unsetAttributes();  // clear any default values
		if(isset($_GET['LogProfile']))
			$LogProfile->attributes=$_GET['LogProfile'];

		$LogKurmod=new LogKurmod('search');
		$LogKurmod->unsetAttributes();  // clear any default values
		if(isset($_GET['LogKurmod']))
			$LogKurmod->attributes=$_GET['LogKurmod'];

		$this->render('library',array(
			'LogProfile'=>$LogProfile,
			'LogKurmod'=>$LogKurmod
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = "signin";
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	public function actionCreate()
	{
	    $this->layout="signin";
		$model=new User('validasi');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->active=1;
			$model->acc_status=1;
			$getPassword=$model->password;
			$model->password=md5($model->password);
			$model->em_enk=md5($model->phone);

			if (substr($model->phone, 0,1) == 0){
				$model->phone = '+62'.substr($model->phone,1);
			} else {
				$model->phone = '+62'.$model->phone;
			}

			$model->created=date('Y-m-d h:i:s');
			$model->updated='0000-00-00';
			if($model->save()){
				YII::app()->db->createCommand()->insert('token', array(
				    'customer_id'=>$model->id,
				    'customer_phone'=>$model->phone,
				    'total_token'=>0
				));
    	   		// Yii::app()->user->setFlash('success', 'Akun Sudah Terdaftar, Silahkan Login');
				$this->redirect(array('autoLogin?id='.$model->id.'&link='.$getPassword));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionAutoLogin($username)
	{
		$model=new User;
		$model->nama_lengkap = $username;
		$model->username = $username;
		$model->password = md5($username);
		$model->level = 1;
		$model->status = 1;
		$model->created = date('Y-m-d H:i:s');
		$model->save(false);

		$identity=new UserIdentity($username,$username);
		$identity->authenticate();
		switch($identity->errorCode)
		{
			case UserIdentity::ERROR_NONE:
				$duration=3600*24*30; // 30 days
				Yii::app()->user->login($identity,$duration);
				$this->redirect(Yii::app()->user->returnUrl);
				break;
			case UserIdentity::ERROR_USERNAME_INVALID:
				echo 'Phone tidak ditemukan';
				break;
			default: // UserIdentity::ERROR_PASSWORD_INVALID
				echo 'Password tidak cocok dengan akun mana saja';
				break;
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}