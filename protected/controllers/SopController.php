<?php

class SopController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','dokumen'),
				'expression'=>'Yii::app()->user->isAdministrator()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','dokumen'),
				'expression'=>'Yii::app()->user->isAdminbidang()',
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','dokumen'),
				'expression'=>'Yii::app()->user->isVisitor()',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),			
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Sop;
		$model->tgl_dokumen = date('Y-m-d');
		$model->created = date('Y-m-d h-m-s');
		$model->updated = '0000-00-00 00-00-00';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sop']))
		{
			$model->attributes=$_POST['Sop'];
			if(strlen(trim(CUploadedFile::getInstance($model,'dokumen'))) > 0) 
						{ 
							$tmp=CUploadedFile::getInstance($model,'dokumen'); 
							$model->dokumen=$model->nama_file.'-'.'('.date('y-m-d').')'.'.'.$tmp->extensionName; 
						}
			if($model->save()){
				if(strlen(trim($model->dokumen)) > 0) {
					$tmp->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/SOP/'.$model->dokumen);
					}
			}
				$this->redirect(array('view','id'=>$model->sop_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-m-s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sop']))
		{
			$model->attributes=$_POST['Sop'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->sop_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDokumen($id)
	{
		$model=$this->loadModel($id);
		$model->updated = date('Y-m-d h-m-s');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sop']))
		{
			$model->attributes=$_POST['Sop'];
			if(strlen(trim(CUploadedFile::getInstance($model,'dokumen'))) > 0) 
						{ 
							$tmp=CUploadedFile::getInstance($model,'dokumen'); 
							$model->dokumen=$model->nama_file.'-'.'('.date('y-m-d').')'.'.'.$tmp->extensionName; 
						}
			if($model->save()){
				if(strlen(trim($model->dokumen)) > 0) {
					$tmp->saveAs(Yii::getPathOfAlias('webroot').'/Dokumen/SOP/'.$model->dokumen);
					}
			}
				$this->redirect(array('view','id'=>$model->sop_id));
		}

		$this->render('updateDokumen',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Sop');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Sop('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Sop']))
			$model->attributes=$_GET['Sop'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Sop the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Sop::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Sop $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sop-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
