<table class="appendo-gii" id="<?php echo $id ?>">
	<thead>
		<tr>
			<th>Nama Dokumen</th><th>Jenis Dokumen</th><th>Tanggal</th><th>Tim Penyusun</th><th>Cover Dokumen</th>
		</tr>
	</thead>
	<tbody>
    <?php if ($model->nama_dokumen == null): ?>
		<tr>
			<td><?php echo CHtml::textField('nama_dokumen[]','',array('style'=>'width:120px')); ?></td>
            <td><?php echo CHtml::dropDownList('jns_dokumen[]',"string", 
								array('Modul'=>'Modul',
									'Buku Kurikulum'=>'Buku Kurikulum',
									'Bahan Tayang'=>'Bahan Tayang',
									'Pedoman Pelatihan'=>'Pedoman Pelatihan',
									'Buku Soal'=>'Buku Soal',)
									,array("empty"=>"---",'style'=>'width:100;'));?></td>
			<td><?php echo CHtml::dateField('tanggal[]','',array('style'=>'width:120px')); ?></td>
            <td><?php echo CHtml::textField('tim_penyusun[]','',array('style'=>'width:120px')); ?></td>
            <td><?php echo CHtml::fileField('cover_dokumen[]','',array('style'=>'width:120px')); ?></td>
		</tr>
    <?php else: ?>
        <?php for($i = 0; $i < sizeof($model->nama_dokumen); ++$i): ?>
    		<tr>
    			<td><?php echo CHtml::textField('nama_dokumen[]',$model->nama_dokumen[$i],array('style'=>'width:120px')); ?></td>
                <td><?php echo CHtml::dropDownList('jns_dokumen[]',$model->jns_dokumen[$i],
								array('Modul'=>'Modul',
									'Buku Kurikulum'=>'Buku Kurikulum',
									'Bahan Tayang'=>'Bahan Tayang',
									'Pedoman Pelatihan'=>'Pedoman Pelatihan',
									'Buku Soal'=>'Buku Soal',)
									,array("empty"=>"-- Pilih Jenis Dokumen --",'style'=>'width:100;'));?></td>
				<td><?php echo CHtml::dateField('tanggal[]',$model->tanggal[$i],array('style'=>'width:120px')); ?></td>
                <td><?php echo CHtml::textField('tim_penyusun[]',$model->tim_penyusun[$i],array('style'=>'width:120px')); ?></td>
                <td><?php echo CHtml::fileField('cover_dokumen[]',$model->cover_dokumen[$i],array('style'=>'width:120px')); ?></td>
            </tr>
        <?php endfor; ?>
		<tr>
			<td><?php echo CHtml::textField('nama_dokumen[]','',array('style'=>'width:120px')); ?></td>
            <td><?php echo CHtml::dropDownList('jns_dokumen[]',"string", 
								array('Modul'=>'Modul',
									'Buku Kurikulum'=>'Buku Kurikulum',
									'Bahan Tayang'=>'Bahan Tayang',
									'Pedoman Pelatihan'=>'Pedoman Pelatihan',
									'Buku Soal'=>'Buku Soal',)
									,array("empty"=>"-- Pilih Jenis Dokumen --",'style'=>'width:100;'));?></td>
			<td><?php echo CHtml::dateField('tanggal[]','',array('style'=>'width:120px')); ?></td>
            <td><?php echo CHtml::textField('tim_penyusun[]','',array('style'=>'width:120px')); ?></td>
            <td><?php echo CHtml::fieldField('cover_dokumen[]','',array('style'=>'width:120px')); ?></td>
		</tr>
    <?php endif; ?>
	</tbody>
</table>

<script language='javascript'>
	function validAngka(a)
	{
		if(!/^[0-9.]+$/.test(a.value))
		{
		a.value = a.value.substring(0,a.value.length-1000);
		}
	}
</script>