<?php

/**
 * This is the model class for table "bmn".
 *
 * The followings are the available columns in table 'bmn':
 * @property integer $bmn_id
 * @property integer $tipe
 * @property string $nama_barang
 * @property integer $nup
 * @property string $merk
 * @property string $satuan
 * @property string $thn_peroleh
 * @property string $kondisi
 * @property string $kode_barang
 * @property string $keterangan
 * @property integer $bidang
 * @property string $tgl_dokumen
 * @property string $dokumen
 * @property string $created
 * @property string $updated
 */
class Bmn extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bmn';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_barang', 'required'),
			array('tipe, nup, bidang', 'numerical', 'integerOnly'=>true),
			array('nama_barang, merk, dokumen', 'length', 'max'=>255),
			array('satuan, kondisi, kode_barang', 'length', 'max'=>50),
			array('thn_peroleh', 'length', 'max'=>4),
			array('keterangan,img_url', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bmn_id, tipe, nama_barang, nup, merk, satuan, thn_peroleh, kondisi, kode_barang, keterangan, bidang, tgl_dokumen, dokumen, created, updated, img_url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bmn_id' => 'Bmn',
			'tipe' => 'Tipe',
			'nama_barang' => 'Nama Barang',
			'nup' => 'Nup',
			'merk' => 'Merk',
			'satuan' => 'Satuan',
			'thn_peroleh' => 'Thn Peroleh',
			'kondisi' => 'Kondisi',
			'kode_barang' => 'Kode Barang',
			'keterangan' => 'Keterangan',
			'bidang' => 'Bidang',
			'tgl_dokumen' => 'Tgl Dokumen',
			'dokumen' => 'Dokumen',
			'img_url' => 'Gambar',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bmn_id',$this->bmn_id);
		$criteria->compare('tipe',$this->tipe);
		$criteria->compare('nama_barang',$this->nama_barang,true);
		$criteria->compare('nup',$this->nup);
		$criteria->compare('merk',$this->merk,true);
		$criteria->compare('satuan',$this->satuan,true);
		$criteria->compare('thn_peroleh',$this->thn_peroleh,true);
		$criteria->compare('kondisi',$this->kondisi,true);
		$criteria->compare('kode_barang',$this->kode_barang,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('tgl_dokumen',$this->tgl_dokumen,true);
		$criteria->compare('dokumen',$this->dokumen,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bmn the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
