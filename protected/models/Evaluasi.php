<?php

/**
 * This is the model class for table "dokumen_evaluasi".
 *
 * The followings are the available columns in table 'dokumen_evaluasi':
 * @property integer $id
 * @property integer $category_id
 * @property integer $type
 * @property string $name
 * @property string $file_url
 * @property string $created_at
 */
class Evaluasi extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'dokumen_evaluasi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, name, file_url', 'required'),
			array('category_id, type', 'numerical', 'integerOnly'=>true),
			array('name, file_url', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, category_id, type, name, file_url, created_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Category' => array(self::BELONGS_TO, 'KategoriEvaluasi', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => 'Category',
			'type' => 'Type',
			'name' => 'Name',
			'file_url' => 'File Url',
			'created_at' => 'Created At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('type',$this->type);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('file_url',$this->file_url,true);
		$criteria->compare('created_at',$this->created_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Evaluasi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getType($data) {
		switch ($data) {
			case 1:
				return "Laporan Triwulan";
				break;
			case 2:
				return "Laporan Semester";
				break;
			case 3:
				return "Laporan Masukan Terhadap Pengajar";
				break;
			case 4:
				return "Laporan Manajemen Penyelenggaraan";
				break;
			case 5:
				return "Laporan Masukan Pengajar Terhadap Pemateri";
				break;
			case 6:
				return "Laporan FGD";
				break;
			case 7:
				return "Laporan Lokakarya";
				break;
			case 8:
				return "Laporan Akhir";
				break;
			case 0:
				return "-";
				break;
		}
	}
}
