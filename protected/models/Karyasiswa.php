<?php

/**
 * This is the model class for table "karyasiswa".
 *
 * The followings are the available columns in table 'karyasiswa':
 * @property integer $kry_id
 * @property string $nama_karyasiswa
 * @property string $sts_pdk
 * @property string $thn_masuk
 * @property string $thn_lulus
 * @property string $universitas
 * @property string $prodi
 * @property string $ijazah
 * @property string $ipk
 * @property string $catatan_pdk
 * @property string $created
 * @property string $updated
 */
class Karyasiswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'karyasiswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_karyasiswa, sts_pdk, thn_masuk, thn_lulus, universitas, prodi, ipk, catatan_pdk, mulai_kuliah, perkiraan_tahun_lulus, kelas, nip, no_hp, status_kepegawaian, unit_kerja, progress, created, updated', 'required'),
			array('nama_karyasiswa, universitas, other_status_kepegawaian, prodi, ijazah, unit_kerja, progress', 'length', 'max'=>255),
			array('sts_pdk', 'length', 'max'=>100),
			array('mulai_kuliah, perkiraan_tahun_lulus, kelas, nip, no_hp, status_kepegawaian', 'length', 'max'=>100),
			array('ipk', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kry_id, nama_karyasiswa, sts_pdk, thn_masuk, thn_lulus, other_status_kepegawaian, universitas, prodi, ijazah, ipk, catatan_pdk, mulai_kuliah, perkiraan_tahun_lulus, kelas, nip, no_hp, status_kepegawaian, unit_kerja, progress, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kry_id' => 'Karyasiswa ID',
			'nama_karyasiswa' => 'Nama Karyasiswa',
			'sts_pdk' => 'Status Kelulusan',
			'thn_masuk' => 'Angkatan',
			'thn_lulus' => 'Lulus Tahun',
			'universitas' => 'Universitas',
			'prodi' => 'Program Studi',
			'ijazah' => 'No Ijazah',
			'ipk' => 'IPK',
			'catatan_pdk' => 'Keterangan',
			'mulai_kuliah' => 'Mulai Kuliah',
			'perkiraan_tahun_lulus' => 'Perkiraan Tahun Lulus',
			'kelas' => 'Kelas',
			'nip' => 'NIP/NIM',
			'no_hp' => 'No HP',
			'status_kepegawaian' => 'Status Kepegawaian',
			'other_status_kepegawaian' => 'Lainnya',
			'unit_kerja' => 'Unit Kerja',
			'progress' => 'Progress',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kry_id',$this->kry_id);
		$criteria->compare('nama_karyasiswa',$this->nama_karyasiswa,true);
		$criteria->compare('sts_pdk',$this->sts_pdk,true);
		$criteria->compare('thn_masuk',$this->thn_masuk,true);
		$criteria->compare('thn_lulus',$this->thn_lulus,true);
		$criteria->compare('universitas',$this->universitas,true);
		$criteria->compare('prodi',$this->prodi,true);
		$criteria->compare('ijazah',$this->ijazah,true);
		$criteria->compare('ipk',$this->ipk,true);
		$criteria->compare('catatan_pdk',$this->catatan_pdk,true);
		$criteria->compare('mulai_kuliah',$this->mulai_kuliah,true);
		$criteria->compare('perkiraan_tahun_lulus',$this->perkiraan_tahun_lulus,true);
		$criteria->compare('kelas',$this->kelas,true);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('status_kepegawaian',$this->status_kepegawaian,true);
		$criteria->compare('unit_kerja',$this->unit_kerja,true);
		$criteria->compare('progress',$this->progress,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getUniv($univ,$thn)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kry_id',$this->kry_id);
		$criteria->compare('nama_karyasiswa',$this->nama_karyasiswa,true);
		$criteria->compare('sts_pdk',$this->sts_pdk,true);
		$criteria->compare('thn_masuk',$thn);
		$criteria->compare('thn_lulus',$this->thn_lulus,true);
		$criteria->compare('universitas',$univ);
		$criteria->compare('prodi',$this->prodi,true);
		$criteria->compare('ijazah',$this->ijazah,true);
		$criteria->compare('ipk',$this->ipk,true);
		$criteria->compare('catatan_pdk',$this->catatan_pdk,true);
		$criteria->compare('mulai_kuliah',$this->mulai_kuliah,true);
		$criteria->compare('perkiraan_tahun_lulus',$this->perkiraan_tahun_lulus,true);
		$criteria->compare('kelas',$this->kelas,true);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('no_hp',$this->no_hp,true);
		$criteria->compare('status_kepegawaian',$this->status_kepegawaian,true);
		$criteria->compare('unit_kerja',$this->unit_kerja,true);
		$criteria->compare('progress',$this->progress,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Karyasiswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function reportLulus(){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(kry_id) FROM (karyasiswa)  
			WHERE sts_pdk = "Lulus"
			GROUP BY (sts_pdk)
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}
	public function reportGoing(){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(kry_id) FROM (karyasiswa)  
			WHERE sts_pdk = "On Going"
			GROUP BY (sts_pdk)
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}
	public function reportBermasalah(){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(kry_id) FROM (karyasiswa)  
			WHERE (sts_pdk != "Lulus" AND sts_pdk != "On Going")
			GROUP BY (sts_pdk)
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}		
}
