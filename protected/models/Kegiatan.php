<?php

/**
 * This is the model class for table "kegiatan".
 *
 * The followings are the available columns in table 'kegiatan':
 * @property integer $kg_id
 * @property string $nama_kegiatan
 * @property string $tgl_kegiatan
 * @property string $ktg_kegiatan
 * @property string $lokasi_kegiatan
 * @property string $person_in_charge
 * @property string $created
 * @property string $updated
 */
class Kegiatan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kegiatan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_kegiatan, tgl_kegiatan, ktg_kegiatan, lokasi_kegiatan, person_in_charge, created, updated', 'required'),
			array('nama_kegiatan, lokasi_kegiatan, person_in_charge', 'length', 'max'=>255),
			array('ktg_kegiatan', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kg_id, nama_kegiatan, tgl_kegiatan, ktg_kegiatan, lokasi_kegiatan, person_in_charge, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kg_id' => 'Kegiatan ID',
			'nama_kegiatan' => 'Nama Kegiatan',
			'tgl_kegiatan' => 'Tanggal Kegiatan',
			'ktg_kegiatan' => 'Keterangan Kegiatan',
			'lokasi_kegiatan' => 'Lokasi Kegiatan',
			'person_in_charge' => 'Person In Charge',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kg_id',$this->kg_id);
		$criteria->compare('nama_kegiatan',$this->nama_kegiatan,true);
		$criteria->compare('tgl_kegiatan',$this->tgl_kegiatan,true);
		$criteria->compare('ktg_kegiatan',$this->ktg_kegiatan,true);
		$criteria->compare('lokasi_kegiatan',$this->lokasi_kegiatan,true);
		$criteria->compare('person_in_charge',$this->person_in_charge,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kegiatan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function hari_ini($data){
		$datee = date('d-m-Y',strtotime($data));
		$hari = date ("D",strtotime($data));
	 
		switch($hari){
			case 'Sun':
				$hari_ini = "Minggu";
			break;
	 
			case 'Mon':			
				$hari_ini = "Senin";
			break;
	 
			case 'Tue':
				$hari_ini = "Selasa";
			break;
	 
			case 'Wed':
				$hari_ini = "Rabu";
			break;
	 
			case 'Thu':
				$hari_ini = "Kamis";
			break;
	 
			case 'Fri':
				$hari_ini = "Jumat";
			break;
	 
			case 'Sat':
				$hari_ini = "Sabtu";
			break;
			
			default:
				$hari_ini = "Tidak di ketahui";		
			break;
		}
	 
		return $hari_ini.", ".$datee;
	}	

	public static function getKegiatan(){
		$sql = "SELECT * FROM kegiatan";
		$command = YII::app()->db->createCommand($sql);
		return $command->queryAll();
	}	
}
