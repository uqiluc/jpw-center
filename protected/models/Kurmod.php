<?php

/**
 * This is the model class for table "kurmod".
 *
 * The followings are the available columns in table 'kurmod':
 * @property integer $km_id
 * @property integer $bidang
 * @property string $created
 * @property string $updated
 */
class Kurmod extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kurmod';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bidang, nm_pelatihan, created, updated', 'required'),
			array('bidang', 'numerical', 'integerOnly'=>true),
			array('nm_pelatihan', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('km_id, bidang, nm_pelatihan, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'km_id' => 'Kurmod ID',
			'bidang' => 'Bidang',
			'nm_pelatihan' => 'Nama Pelatihan',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('nm_pelatihan',$this->nm_pelatihan,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function kurmod2($nm_pelatihan)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('nm_pelatihan',$nm_pelatihan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Kurmod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
