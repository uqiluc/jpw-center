<?php

/**
 * This is the model class for table "kurmod_detail".
 *
 * The followings are the available columns in table 'kurmod_detail':
 * @property integer $kmd_id
 * @property string $nama_dokumen
 * @property string $jns_dokumen
 * @property string $tgl_dokumen
 * @property string $tim_penyusun
 * @property string $cover_dokumen
 * @property integer $km_id
 * @property string $created
 * @property string $updated
 */
class KurmodDetail extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kurmod_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kmd_id, km_id', 'numerical', 'integerOnly'=>true),
			array('nama_dokumen, jns_dokumen, tim_penyusun, cover_dokumen,file_dokumen', 'length', 'max'=>255),
			array('tanggal,', 'length', 'max'=>30),
			array('nama_dokumen, jns_dokumen, tim_penyusun, cover_dokumen,file_dokumen,prof_id,tim_penyusun, gdriveUrl', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('kmd_id, nama_dokumen, prof_id, jns_dokumen, tanggal, bulan, tahun, tim_penyusun, cover_dokumen,file_dokumen, km_id, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ProfilPelatihan' => array(self::BELONGS_TO, 'ProfilPelatihan', 'prof_id'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kmd_id' => 'KMD ID',
			'prof_id' => 'Profile Pelatihan',
			'nama_dokumen' => 'Nama Kurmod',
			'jns_dokumen' => 'Jenis Dokumen',
			'tanggal' => 'Tahun Pembuatan',
			'tim_penyusun' => 'Tim Penyusun',
			'cover_dokumen' => 'Cover Dokumen',
			'km_id' => 'KM ID',
			'created' => 'Created',
			'updated' => 'Tanggal Update',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getType($type)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$type);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getKurmodTable($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->join='LEFT JOIN profil_pelatihan c ON c.prof_id=t.prof_id';
		$criteria->addCondition('bidang = '.$id);

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('t.prof_id',$this->prof_id);
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getPerumahan()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->join='LEFT JOIN profil_pelatihan c ON c.prof_id=t.prof_id';
		$criteria->addCondition('bidang = 8');

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('t.prof_id',$this->prof_id);
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getPemukiman()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->join='LEFT JOIN profil_pelatihan c ON c.prof_id=t.prof_id';
		$criteria->addCondition('bidang = 9');

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('t.prof_id',$this->prof_id);		
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getPiw()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->join='LEFT JOIN profil_pelatihan c ON c.prof_id=t.prof_id';
		$criteria->addCondition('bidang = 10');

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('t.prof_id',$this->prof_id);		
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getJalanJembatan()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->join='LEFT JOIN profil_pelatihan c ON c.prof_id=t.prof_id';
		$criteria->addCondition('bidang = 7');

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('t.prof_id',$this->prof_id);
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getPelatihan($type)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->join='LEFT JOIN profil_pelatihan c ON c.prof_id=t.prof_id';
		$criteria->addCondition('bidang = '.$type);

		$criteria->compare('kmd_id',$this->kmd_id);
		$criteria->compare('t.prof_id',$this->prof_id);
		$criteria->compare('nama_dokumen',$this->nama_dokumen,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('tanggal',$this->tanggal);
		$criteria->compare('tim_penyusun',$this->tim_penyusun,true);
		$criteria->compare('cover_dokumen',$this->cover_dokumen,true);
		$criteria->compare('km_id',$this->km_id);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function Modulus($id,$filter){
		switch ($filter) {
			case 'Name':
				$order = 'nama_dokumen';
				break;
			case 'Update':
				$order = 'updated';
				break;
		}

		$sql = "SELECT * FROM kurmod_detail WHERE jns_dokumen = 'Modul' AND prof_id=$id ORDER BY $order ASC";
		$command = YII::app()->db->createCommand($sql);
		return $command->queryAll();
	}	

	public function BahanTayang($id,$filter)
	{
		switch ($filter) {
			case 'Name':
				$order = 'nama_dokumen';
				break;
			case 'Update':
				$order = 'updated';
				break;
		}

		$sql = "SELECT * FROM kurmod_detail WHERE jns_dokumen = 'Bahan Tayang' AND prof_id=$id ORDER BY $order ASC";
		$command = YII::app()->db->createCommand($sql);
		
		if ($command->queryAll() != '') {
			return $command->queryAll();
		} else {
			return "Data tidak ada";
		}
	}	

	public function getStatusReady($data)
	{
		switch ($data) {
			case '0':
				return 'NO';
				break;
			case '1':
				return 'Ready';
				break;
		}
	}	

	public function Kurikulum($id){
		$sql = "SELECT * FROM kurmod_detail WHERE jns_dokumen = 'Buku Kurikulum' AND prof_id=$id ORDER BY kmd_id ASC";
		$command = YII::app()->db->createCommand($sql);
		return $command->queryAll();
	}	

	public function Latihan($id)
	{
		$sql = "SELECT * FROM kurmod_detail WHERE jns_dokumen = 'Pedoman Latihan' AND prof_id=$id ORDER BY kmd_id ASC";
		$command = YII::app()->db->createCommand($sql);
		return $command->queryAll();
	}	

	public function Soal($id){
		$sql = "SELECT * FROM kurmod_detail WHERE jns_dokumen = 'Buku Soal' AND prof_id=$id ORDER BY kmd_id ASC";
		$command = YII::app()->db->createCommand($sql);
		return $command->queryAll();
	}	

	public function Pedoman($id){
		$sql = "SELECT * FROM kurmod_detail WHERE jns_dokumen = 'Pedoman Pelatihan' AND prof_id=$id ORDER BY kmd_id ASC";
		$command = YII::app()->db->createCommand($sql);
		return $command->queryAll();
	}	

	public static function getKurmod($id){
		$sql = "SELECT * FROM kurmod_detail WHERE prof_id = $id";
		$command = YII::app()->db->createCommand($sql);
		return $command->queryAll();
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return KurmodDetail the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
