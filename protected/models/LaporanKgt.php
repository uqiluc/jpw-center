<?php

/**
 * This is the model class for table "laporan_kgt".
 *
 * The followings are the available columns in table 'laporan_kgt':
 * @property integer $lpr_id
 * @property string $nama_laporan
 * @property string $jns_dokumen
 * @property integer $bidang
 * @property string $tgl_dokumen
 * @property string $dokumen
 * @property string $created
 * @property string $updated
 */
class LaporanKgt extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laporan_kgt';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_laporan, jns_dokumen, bidang, tgl_dokumen, dokumen, created, updated', 'required'),
			array('bidang', 'numerical', 'integerOnly'=>true),
			array('nama_laporan, dokumen', 'length', 'max'=>255),
			array('jns_dokumen', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('lpr_id, nama_laporan, jns_dokumen, bidang, tgl_dokumen, dokumen, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Bidang' => array(self::BELONGS_TO, 'Bidang', 'bidang'),						
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lpr_id' => 'Laporan ID',
			'nama_laporan' => 'Nama Laporan',
			'jns_dokumen' => 'Jenis Dokumen',
			'bidang' => 'Bidang',
			'tgl_dokumen' => 'Tanggal Dokumen',
			'dokumen' => 'Dokumen',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lpr_id',$this->lpr_id);
		$criteria->compare('nama_laporan',$this->nama_laporan,true);
		$criteria->compare('jns_dokumen',$this->jns_dokumen,true);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('tgl_dokumen',$this->tgl_dokumen,true);
		$criteria->compare('dokumen',$this->dokumen,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LaporanKgt the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
