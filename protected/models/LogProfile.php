<?php

/**
 * This is the model class for table "media".
 *
 * The followings are the available columns in table 'media':
 * @property integer $md_id
 * @property string $nama_file
 * @property string $jns_file
 * @property integer $bidang
 * @property string $tgl_dokumen
 * @property string $dokumen
 * @property string $created
 * @property string $updated
 */
class LogProfile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'log_profile_pelatihan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, profileId, initialName, nameChange, createTime, createdBy', 'safe'),
			array('id, profileId, initialName, nameChange, createTime, createdBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Profile' => array(self::BELONGS_TO, 'ProfilPelatihan', 'profileId'),
			'Users' => array(self::BELONGS_TO, 'User', 'createdBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'profileId' => 'Profile Saat Ini',
			'initialName' => 'Nama Lama',
			'nameChange' => 'Nama Baru',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('profileId',$this->profileId);
		$criteria->compare('initialName',$this->initialName,true);
		$criteria->compare('nameChange',$this->nameChange,true);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('createdBy',$this->createdBy,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getProfile($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('profileId',$id);
		$criteria->compare('initialName',$this->initialName,true);
		$criteria->compare('nameChange',$this->nameChange,true);
		$criteria->compare('createTime',$this->createTime,true);
		$criteria->compare('createdBy',$this->createdBy,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Media the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
