<?php

/**
 * This is the model class for table "pegawai".
 *
 * The followings are the available columns in table 'pegawai':
 * @property integer $pg_id
 * @property string $nama_pegawai
 * @property string $nip
 * @property string $golongan
 * @property string $jabatan
 * @property integer $bidang
 * @property string $created
 * @property string $updated
 */
class Pegawai extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pegawai';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_pegawai, nip, golongan, jabatan, kelas_jabatan, bidang, pangkat, jenisPegawai,email', 'required'),
			// array('bidang', 'numerical', 'integerOnly'=>true),
			array('nama_pegawai, golongan, jabatan, pangkat', 'length', 'max'=>255),
			array('nip, kelas_jabatan', 'length', 'max'=>30),
			array('email', 'length', 'max'=>50),
			array('foto', 'file', 'allowEmpty'=>true, 'on'=>'updateG', 'types'=>'jpg,jpeg,gif,png' ,'maxSize'=>2097152, 'tooLarge'=>'File has to be smaller than 2MB'),			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pg_id, nama_pegawai, nip, golongan, jabatan, kelas_jabatan, bidang, pangkat, created, updated, jenisPegawai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Bidang' => array(self::BELONGS_TO, 'Bidang', 'bidang'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pg_id' => 'Pegawai ID',
			'nama_pegawai' => 'Nama Pegawai',
			'nip' => 'NIP',
			'golongan' => 'Golongan',
			'jabatan' => 'Jabatan',
			'kelas_jabatan' => 'Kelas Jabatan',
			'bidang' => 'Bagian',
			'pangkat' => 'Pangkat',
			'email' => 'Email',
			'created' => 'Created',
			'updated' => 'Updated',
			'jenisPegawai' => 'Jenis Pegawai',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pg_id',$this->pg_id);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('golongan',$this->golongan,true);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('kelas_jabatan',$this->kelas_jabatan,true);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('pangkat',$this->pangkat);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('jenisPegawai',$this->jenisPegawai,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pegawai the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function date($data){
		if ($data == '0000-00-00')
		{
		    return "-";
		}
		else{
		$bulan = date('m',strtotime($data));
		switch ($bulan) {
			case 1 : $bulan="Januari";
				break;
			case 2 : $bulan="Februari";
				break;
			case 3 : $bulan="Maret";
				break;				
			case 4 : $bulan="April";
				break;				
			case 5 : $bulan="Mei";
				break;
			case 6 : $bulan="Juni";
				break;				
			case 7 : $bulan="Juli";
				break;
			case 8 : $bulan="Agustus";
				break;				
			case 9 : $bulan="September";
				break;				
			case 10 : $bulan="Oktober";
				break;				
			case 11 : $bulan="November";
				break;				
			case 12 : $bulan="Desember";
				break;				
		}
		$datee = date('d',strtotime($data)).' '.$bulan.' '.date('Y',strtotime($data));
		return $datee;
		}
	}

	public function reportPns(){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(pg_id) FROM (pegawai)  
			WHERE jenisPegawai = "PNS"
			GROUP BY (jenisPegawai)
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}

	public function reportNonPns(){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(pg_id) FROM (pegawai)  
			WHERE jenisPegawai = "NON PNS (NRP)"
			GROUP BY (jenisPegawai)
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}

	public function reportKI(){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(pg_id) FROM (pegawai)  
			WHERE jenisPegawai = "KI"
			GROUP BY (jenisPegawai)
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}

	public function getImage($data,$id){
		if ($data == null){
			return CHtml::link(CHtml::image(Yii::app()->request->baseurl."/img/no-user.jpg","image",array("width"=>100)),array("updateG","id"=>$id));
		} else {
			return CHtml::link(CHtml::image(Yii::app()->request->baseurl."/Dokumen/Foto/".$data,"image",array("width"=>100)),array("updateG","id"=>$id));

		}
	}	

	public function getPegawai($id){
		$model = Pegawai::model()->findByPk($id);
		if ($model->foto != null){
			return "<div class='d-flex px-2 py-1'><div>".CHtml::image(Yii::app()->request->baseurl."/Dokumen/Foto/".$model->foto,"image",array("class"=>"avatar avatar-sm me-3"))."</div><div class='d-flex flex-column justify-content-center'><h6 class='mb-0 text-sm'>".CHtml::link($model->nama_pegawai,array("pegawai/view","id"=>$model->pg_id),array("class"=>"badge badge-sm bg-gradient-secondary"))."</h6></div>";
		} else {
			return "<div class='d-flex px-2 py-1'><div>".CHtml::image(Yii::app()->request->baseurl."/img/no-user.jpg","image",array("class"=>"avatar avatar-sm me-3"))."</div><div class='d-flex flex-column justify-content-center'><h6 class='mb-0 text-sm'>".CHtml::link($model->nama_pegawai,array("pegawai/view","id"=>$model->pg_id),array("class"=>"badge badge-sm bg-gradient-secondary"))."</h6></div>";
		}
	}
}
