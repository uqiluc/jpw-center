<?php

/**
 * This is the model class for table "pelaksanaan_plth".
 *
 * The followings are the available columns in table 'pelaksanaan_plth':
 * @property integer $pplth_id
 * @property string $nm_pelatihan
 * @property string $tahun
 * @property string $tgl_plaksana
 * @property string $lokasi_plthn
 * @property integer $bidang
 * @property string $ktg_plthn
 * @property string $jml_peserta
 * @property string $jns_rncana
 * @property string $brt_acara
 * @property string $dtl_psrta
 * @property integer $sts_platihan
 * @property string $created
 * @property string $updated
 */
class PelaksanaanPlth extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pelaksanaan_plth';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nm_pelatihan, tahun, tgl_plaksana, lokasi_plthn, bidang, ktg_plthn, jns_rncana,tgl_akhir', 'required'),
			array('bidang, sts_platihan', 'numerical', 'integerOnly'=>true),
			array('nm_pelatihan, lokasi_plthn, ktg_plthn, brt_acara', 'length', 'max'=>255),
			array('jml_peserta, dtl_psrta', 'length', 'max'=>4),
			array('jns_rncana', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('pplth_id, nm_pelatihan, tahun, tgl_plaksana, lokasi_plthn, bidang, ktg_plthn, jml_peserta, jns_rncana, brt_acara, dtl_psrta, sts_platihan, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Bidang' => array(self::BELONGS_TO, 'Bidang', 'bidang'),						
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pplth_id' => 'Pelaksanaan ID',
			'pel_id' => 'Profile',
			'nm_pelatihan' => 'Nama Pelatihan',
			'tahun' => 'Tahun',
			'tgl_plaksana' => 'Tanggal Awal',
			'tgl_akhir' => 'Tanggal Akhir',
			'lokasi_plthn' => 'Lokasi Pelatihan',
			'bidang' => 'Bidang',
			'ktg_plthn' => 'Keterangan Pelatihan',
			'jml_peserta' => 'Jumlah Peserta',
			'jns_rncana' => 'Jenis Rencana',
			'brt_acara' => 'Berita Acara',
			'dtl_psrta' => 'Detail Peserta',
			'sts_platihan' => 'Status Pelatihan',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pplth_id',$this->pplth_id);
		$criteria->compare('nm_pelatihan',$this->nm_pelatihan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('tgl_plaksana',$this->tgl_plaksana,true);
		$criteria->compare('lokasi_plthn',$this->lokasi_plthn,true);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('ktg_plthn',$this->ktg_plthn,true);
		$criteria->compare('jml_peserta',$this->jml_peserta,true);
		$criteria->compare('jns_rncana',$this->jns_rncana,true);
		$criteria->compare('brt_acara',$this->brt_acara,true);
		$criteria->compare('dtl_psrta',$this->dtl_psrta,true);
		$criteria->compare('sts_platihan',$this->sts_platihan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getDashboard()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$dateNow = date('Y-m');

		$criteria=new CDbCriteria;
		$criteria->addCondition('tgl_plaksana LIKE "%'.$dateNow.'%"');

		$criteria->compare('pplth_id',$this->pplth_id);
		$criteria->compare('nm_pelatihan',$this->nm_pelatihan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('tgl_plaksana',$this->tgl_plaksana,true);
		$criteria->compare('lokasi_plthn',$this->lokasi_plthn,true);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('ktg_plthn',$this->ktg_plthn,true);
		$criteria->compare('jml_peserta',$this->jml_peserta,true);
		$criteria->compare('jns_rncana',$this->jns_rncana,true);
		$criteria->compare('brt_acara',$this->brt_acara,true);
		$criteria->compare('dtl_psrta',$this->dtl_psrta,true);
		$criteria->compare('sts_platihan',$this->sts_platihan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
                    'pageSize'=>4,
                ),
		));
	}

	public function Pelaksana($nm_pelatihan)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pplth_id',$this->pplth_id);
		$criteria->compare('nm_pelatihan',$nm_pelatihan);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('tgl_plaksana',$this->tgl_plaksana,true);
		$criteria->compare('lokasi_plthn',$this->lokasi_plthn,true);
		$criteria->compare('bidang',$this->bidang);
		$criteria->compare('ktg_plthn',$this->ktg_plthn,true);
		$criteria->compare('jml_peserta',$this->jml_peserta,true);
		$criteria->compare('jns_rncana',$this->jns_rncana,true);
		$criteria->compare('brt_acara',$this->brt_acara,true);
		$criteria->compare('dtl_psrta',$this->dtl_psrta,true);
		$criteria->compare('sts_platihan',$this->sts_platihan);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PelaksanaanPlth the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function prd($nilai)
	{
		if ($nilai < 70)
		{
			return "Tidak Lulus";
		}
		elseif ($nilai >= 70.0 && $nilai <= 77.4) {
			return "Baik";
		}
		elseif ($nilai >= 77.5 && $nilai <= 84.9) {
			return "Baik Sekali";
		}
		elseif ($nilai >= 85.0 && $nilai <= 92.4) {
			return "Memuaskan";
		}
		elseif ($nilai >= 92.5 && $nilai <= 100) {
			return "Sangat Memuaskan";
		}						
	}	

	public function yearReportTarget($data){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(pplth_id) FROM (pelaksanaan_plth)  
			WHERE (tahun='.$data.') AND jns_rncana = "Target"
			GROUP BY (tahun='.$data.')
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}

	public function monthReport($year,$data){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(pplth_id) FROM (pelaksanaan_plth)  
			WHERE (Month(tgl_plaksana)='.$data.') 
			AND (YEAR(tgl_plaksana)='.$year.')
			GROUP BY (Month(tgl_plaksana)='.$data.')
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}	

	public function yearReportTambahan($data){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(pplth_id) FROM (pelaksanaan_plth)  
			WHERE (tahun='.$data.') AND jns_rncana = "Tambahan"
			GROUP BY (tahun='.$data.')
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}

	public function status($data){
		if ($data == 0){
			return "Sedang Berlangsung";
		}elseif ($data == 1){
			return "Sedang Berlangsung";
		}elseif ($data == 2){
			return "Dibatalkan";
		}elseif ($data == 3){
			return "Ditunda";
		}
	}

	public function statusIcon($data){
		if ($data == 0 || $data == 1){
			return "<i class='fa fa-spinner'></i>";
		}elseif ($data == 2){
			return "<i class='fa fa-times'></i>";
		}elseif ($data == 3){
			return "<i class='fa fa-clock'></i>";
		}
	}

	public function getStatus($data){
		if ($data == 0){
			return "<div class='label label-warning' data-toggle='modal' data-target='#editStatus'>Sedang Berlangsung <i class='fa fa-edit'></i></div>";
		}elseif ($data == 1){
			return "<div class='label label-success' data-toggle='modal' data-target='#editStatus'>Terlaksana <i class='fa fa-edit'></i></div>";
		}elseif ($data == 2){
			return "<div class='label label-danger' data-toggle='modal' data-target='#editStatus'>Dibatalkan <i class='fa fa-edit'></i></div>";
		}elseif ($data == 3){
			return "<div class='label label-info' data-toggle='modal' data-target='#editStatus'>Ditunda <i class='fa fa-edit'></i></div>";
		}
	}

	public function getWaktu($awal,$akhir){
		$dateAwal = $awal;
		$dateAkhir = $akhir;
		$gantiAwal = date("d M Y", strtotime($dateAwal));
		$gantiAkhir = date("d M Y", strtotime($dateAkhir));

		return $gantiAwal." - ".$gantiAkhir;
	}

	public function getTgl($data){
		return date("d M Y", strtotime($data));
	}
	
	public function getAge($data){
		// tanggal lahir
		$tanggal = new DateTime($data);

		// tanggal hari ini
		$today = new DateTime('today');

		// tahun
		$y = $today->diff($tanggal)->y;

		// bulan
		$m = $today->diff($tanggal)->m;

		// hari
		$d = $today->diff($tanggal)->d;
		return $y . " tahun " . $m . " bulan " . $d . " hari";
	}
}
