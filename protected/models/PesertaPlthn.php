<?php

/**
 * This is the model class for table "peserta_plthn".
 *
 * The followings are the available columns in table 'peserta_plthn':
 * @property integer $psrt_id
 * @property string $no_urut_bidang
 * @property string $bidang
 * @property string $kluster
 * @property string $pelatihan
 * @property string $tanggal_mulai_diklat
 * @property string $tanggal_selesai_diklat
 * @property string $balai_diklat
 * @property integer $nomor
 * @property string $nip
 * @property string $nama_peserta
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $agama
 * @property string $jenis_kelamin
 * @property string $pangkat
 * @property string $golongan
 * @property string $unit_kerja
 * @property string $nama_instansi
 * @property string $alamat_kantor
 * @property string $pusat_daerah
 * @property string $pns_nonpns
 * @property string $jabatan
 * @property string $pendidikan
 * @property string $jurusan
 * @property string $telepon
 * @property string $handphone
 * @property string $email
 * @property integer $pre_test
 * @property integer $pro_test
 * @property integer $nilai_akhir
 * @property string $predikat
 * @property string $status_kelulusan
 * @property string $pembiayaan
 * @property string $sertifikasi
 * @property string $lulus_sertifikasi
 * @property string $kesesuaian_jabatan
 * @property string $kesesuaian_pendidikan
 * @property string $created
 * @property string $updated
 */
class PesertaPlthn extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peserta_plthn';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_urut_bidang, bidang, kluster, nomor, nip, nama_peserta, tempat_lahir, tanggal_lahir, agama, jenis_kelamin, pangkat, golongan, unit_kerja, nama_instansi, alamat_kantor, pusat_daerah, pns_nonpns, jabatan, pendidikan, jurusan, telepon, handphone, email, pembiayaan, sertifikasi, lulus_sertifikasi, kesesuaian_jabatan, kesesuaian_pendidikan', 'required'),
			array('nomor, pre_test, pro_test, nilai_akhir', 'numerical', 'integerOnly'=>true),
			array('no_urut_bidang, bidang, kluster, nip, tempat_lahir', 'length', 'max'=>30),
			array('pelatihan, nama_peserta, alamat_kantor', 'length', 'max'=>255),
			array('tanggal_mulai_diklat, tanggal_selesai_diklat, tanggal_lahir, pangkat, unit_kerja, jabatan, pendidikan, status_kelulusan, pembiayaan, sertifikasi, lulus_sertifikasi', 'length', 'max'=>50),
			array('jenis_kelamin, telepon, handphone', 'length', 'max'=>15),
			array('agama, golongan, pusat_daerah, pns_nonpns, predikat', 'length', 'max'=>20),
			array('nama_instansi, jurusan, email', 'length', 'max'=>100),
			array(' email', 'email'),
			array('kesesuaian_jabatan, kesesuaian_pendidikan', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('psrt_id, no_urut_bidang, bidang, kluster, pelatihan, tanggal_mulai_diklat, tanggal_selesai_diklat, balai_diklat, nomor, nip, nama_peserta, tempat_lahir, tanggal_lahir, agama, jenis_kelamin, pangkat, golongan, unit_kerja, nama_instansi, alamat_kantor, pusat_daerah, pns_nonpns, jabatan, pendidikan, jurusan, telepon, handphone, email, pre_test, pro_test, nilai_akhir, predikat, status_kelulusan, pembiayaan, sertifikasi, lulus_sertifikasi, kesesuaian_jabatan, kesesuaian_pendidikan, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Pelatihan' => array(self::BELONGS_TO, 'PelaksanaanPlth', 'pel_id'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'psrt_id' => 'ID Peserta',
			'no_urut_bidang' => 'No Urut Bidang',
			'bidang' => 'Bidang',
			'kluster' => 'Kluster',
			'pelatihan' => 'Nama Pelatihan',
			'tanggal_mulai_diklat' => 'Tanggal Mulai Pelatihan',
			'tanggal_selesai_diklat' => 'Tanggal Selesai Pelatihan',
			'balai_diklat' => 'Balai Pelatihan',
			'nomor' => 'No',
			'nip' => 'NIP',
			'nama_peserta' => 'Nama Peserta',
			'tempat_lahir' => 'Tempat Lahir',
			'tanggal_lahir' => 'Tanggal Lahir',
			'agama' => 'Agama',
			'jenis_kelamin' => 'Jenis Kelamin',
			'pangkat' => 'Pangkat',
			'golongan' => 'Golongan',
			'unit_kerja' => 'Unit Kerja',
			'nama_instansi' => 'Nama Instansi',
			'alamat_kantor' => 'Alamat Kantor',
			'pusat_daerah' => 'Pusat/Daerah',
			'pns_nonpns' => 'PNS/Non PNS',
			'jabatan' => 'Jabatan',
			'pendidikan' => 'Pendidikan',
			'jurusan' => 'Jurusan',
			'telepon' => 'Telepon',
			'handphone' => 'Handphone',
			'email' => 'Email',
			'pre_test' => 'Pre Test',
			'pro_test' => 'Pro Test',
			'nilai_akhir' => 'Nilai Akhir',
			'predikat' => 'Predikat',
			'status_kelulusan' => 'Status Kelulusan',
			'pembiayaan' => 'Pembiayaan (APBN/Kerjasama)',
			'sertifikasi' => 'Sertifikasi / Tidak ?',
			'lulus_sertifikasi' => 'Lulus Sertifikasi/Tidak ?',
			'kesesuaian_jabatan' => 'Kesesuaian Jabatan',
			'kesesuaian_pendidikan' => 'Kesesuaian Pendidikan',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('psrt_id',$this->psrt_id);
		$criteria->compare('no_urut_bidang',$this->no_urut_bidang,true);
		$criteria->compare('bidang',$this->bidang,true);
		$criteria->compare('kluster',$this->kluster,true);
		$criteria->compare('pelatihan',$this->pelatihan,true);
		$criteria->compare('tanggal_mulai_diklat',$this->tanggal_mulai_diklat,true);
		$criteria->compare('tanggal_selesai_diklat',$this->tanggal_selesai_diklat,true);
		$criteria->compare('balai_diklat',$this->balai_diklat,true);
		$criteria->compare('nomor',$this->nomor);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('nama_peserta',$this->nama_peserta,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('pangkat',$this->pangkat,true);
		$criteria->compare('golongan',$this->golongan,true);
		$criteria->compare('unit_kerja',$this->unit_kerja,true);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_kantor',$this->alamat_kantor,true);
		$criteria->compare('pusat_daerah',$this->pusat_daerah,true);
		$criteria->compare('pns_nonpns',$this->pns_nonpns,true);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('jurusan',$this->jurusan,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('handphone',$this->handphone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('pre_test',$this->pre_test);
		$criteria->compare('pro_test',$this->pro_test);
		$criteria->compare('nilai_akhir',$this->nilai_akhir);
		$criteria->compare('predikat',$this->predikat,true);
		$criteria->compare('status_kelulusan',$this->status_kelulusan,true);
		$criteria->compare('pembiayaan',$this->pembiayaan,true);
		$criteria->compare('sertifikasi',$this->sertifikasi,true);
		$criteria->compare('lulus_sertifikasi',$this->lulus_sertifikasi,true);
		$criteria->compare('kesesuaian_jabatan',$this->kesesuaian_jabatan,true);
		$criteria->compare('kesesuaian_pendidikan',$this->kesesuaian_pendidikan,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getDetail($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		
		$criteria->compare('no_urut_bidang',$this->no_urut_bidang,true);
		$criteria->compare('bidang',$this->bidang,true);
		$criteria->compare('kluster',$this->kluster,true);
		$criteria->compare('pel_id',$id);
		$criteria->compare('pelatihan',$this->pelatihan,true);
		$criteria->compare('tanggal_mulai_diklat',$this->tanggal_mulai_diklat,true);
		$criteria->compare('tanggal_selesai_diklat',$this->tanggal_selesai_diklat,true);
		$criteria->compare('balai_diklat',$this->balai_diklat,true);
		$criteria->compare('nomor',$this->nomor);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('nama_peserta',$this->nama_peserta,true);
		$criteria->compare('tempat_lahir',$this->tempat_lahir,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('agama',$this->agama,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('pangkat',$this->pangkat,true);
		$criteria->compare('golongan',$this->golongan,true);
		$criteria->compare('unit_kerja',$this->unit_kerja,true);
		$criteria->compare('nama_instansi',$this->nama_instansi,true);
		$criteria->compare('alamat_kantor',$this->alamat_kantor,true);
		$criteria->compare('pusat_daerah',$this->pusat_daerah,true);
		$criteria->compare('pns_nonpns',$this->pns_nonpns,true);
		$criteria->compare('jabatan',$this->jabatan,true);
		$criteria->compare('pendidikan',$this->pendidikan,true);
		$criteria->compare('jurusan',$this->jurusan,true);
		$criteria->compare('telepon',$this->telepon,true);
		$criteria->compare('handphone',$this->handphone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('pre_test',$this->pre_test);
		$criteria->compare('pro_test',$this->pro_test);
		$criteria->compare('nilai_akhir',$this->nilai_akhir);
		$criteria->compare('predikat',$this->predikat,true);
		$criteria->compare('status_kelulusan',$this->status_kelulusan,true);
		$criteria->compare('pembiayaan',$this->pembiayaan,true);
		$criteria->compare('sertifikasi',$this->sertifikasi,true);
		$criteria->compare('lulus_sertifikasi',$this->lulus_sertifikasi,true);
		$criteria->compare('kesesuaian_jabatan',$this->kesesuaian_jabatan,true);
		$criteria->compare('kesesuaian_pendidikan',$this->kesesuaian_pendidikan,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PesertaPlthn the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function yearReport($data){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(psrt_id) FROM (peserta_plthn)  
			WHERE (tanggal_mulai_diklat LIKE "%'.$data.'%")
			GROUP BY (tanggal_mulai_diklat LIKE "%'.$data.'%")
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}	

	public function monthReport($year,$data){
		$nilai = Yii::app()->db->createCommand('
			SELECT COUNT(psrt_id) FROM (peserta_plthn)
			JOIN pelaksanaan_plth ON peserta_plthn.pel_id = pelaksanaan_plth.pplth_id 
			WHERE (Month(tgl_plaksana)='.$data.') 
			AND (YEAR(tgl_plaksana)='.$year.')
			GROUP BY (Month(tgl_plaksana)='.$data.')
			')->queryScalar();

		if($nilai==""){
			return "0";
		}else{
			return $nilai;
		}
	}		
}
