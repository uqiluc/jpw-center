<?php

/**
 * This is the model class for table "profil_pelatihan".
 *
 * The followings are the available columns in table 'profil_pelatihan':
 * @property integer $prof_id
 * @property string $nama_pelatihan
 * @property integer $bidang
 * @property string $stdr_komp_lulusan
 * @property string $tahun
 * @property string $sasaran_psrta
 * @property string $kualifikasi
 * @property string $pola
 * @property string $durasi_plthn
 * @property string $jml_jp
 * @property string $mata_plthn
 * @property string $bk_kurikulum
 * @property string $modul
 * @property string $bk_soal
 * @property string $bhn_tayang
 * @property string $pedoman
 * @property string $created
 * @property string $updated
 */
class ProfilPelatihan extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'profil_pelatihan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_pelatihan, bidang, stdr_komp_lulusan, tahun, sasaran_psrta, kualifikasi, pola, durasi_plthn, jml_jp, timPenyusun, mata_plthn','required'),
			array('bidang', 'numerical', 'integerOnly'=>true),
			array('nama_pelatihan, stdr_komp_lulusan, sasaran_psrta, pola, mata_plthn, bk_kurikulum, modul, bk_soal, bhn_tayang, pedoman, timPenyusun', 'length', 'max'=>255),
			array('tahun, durasi_plthn', 'length', 'max'=>4),
			array('jml_jp', 'length', 'max'=>3),
			array('prof_id, nama_pelatihan, bidang, stdr_komp_lulusan, tahun, sasaran_psrta, kualifikasi, pola, durasi_plthn, jml_jp, mata_plthn, bk_kurikulum, modul, bk_soal, bhn_tayang, pedoman, created, updated, timPenyusun, coverImage', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('prof_id, nama_pelatihan, bidang, stdr_komp_lulusan, tahun, sasaran_psrta, kualifikasi, pola, durasi_plthn, jml_jp, mata_plthn, bk_kurikulum, modul, bk_soal, bhn_tayang, pedoman, created, updated, timPenyusun, coverImage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Bidang' => array(self::BELONGS_TO, 'Bidang', 'bidang'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'prof_id' => 'Profil ID',
			'nama_pelatihan' => 'Nama Pelatihan',
			'bidang' => 'Bidang',
			'stdr_komp_lulusan' => 'Standar Kompetensi Lulusan',
			'tahun' => 'Tahun',
			'sasaran_psrta' => 'Sasaran Peserta',
			'kualifikasi' => 'Kualifikasi',
			'pola' => 'Pola',
			'durasi_plthn' => 'Durasi Pelatihan',
			'jml_jp' => 'Jumlah Jp',
			'mata_plthn' => 'Mata Pelatihan',
			'bk_kurikulum' => 'Bk Kurikulum',
			'modul' => 'Modul',
			'bk_soal' => 'Bk Soal',
			'bhn_tayang' => 'Bahan Tayang',
			'pedoman' => 'Pedoman',
			'created' => 'Created',
			'updated' => 'Updated',
			'timPenyusun' => 'Tim Penyusun',
			'coverImage' => 'Gambar Cover',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$type = '';
		if (isset($_GET['type']))
			$type = $_GET['type'];
		
		$criteria=new CDbCriteria;
		$criteria->addCondition('isActive=1');

		$criteria->compare('prof_id',$this->prof_id);
		$criteria->compare('nama_pelatihan',$this->nama_pelatihan,true);
		$criteria->compare('bidang',$type);
		$criteria->compare('stdr_komp_lulusan',$this->stdr_komp_lulusan,true);
		$criteria->compare('tahun',$this->tahun,true);
		$criteria->compare('sasaran_psrta',$this->sasaran_psrta,true);
		$criteria->compare('kualifikasi',$this->kualifikasi,true);
		$criteria->compare('pola',$this->pola,true);
		$criteria->compare('durasi_plthn',$this->durasi_plthn,true);
		$criteria->compare('jml_jp',$this->jml_jp,true);
		$criteria->compare('mata_plthn',$this->mata_plthn,true);
		$criteria->compare('bk_kurikulum',$this->bk_kurikulum,true);
		$criteria->compare('modul',$this->modul,true);
		$criteria->compare('bk_soal',$this->bk_soal,true);
		$criteria->compare('bhn_tayang',$this->bhn_tayang,true);
		$criteria->compare('pedoman',$this->pedoman,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProfilPelatihan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function totalPelaksana($data)
	{
		return Yii::app()->db->createCommand("SELECT COUNT(pel_id) FROM pelaksanaan_plth where pel_id = $data")->queryScalar()." Pelatihan";
	}
}
