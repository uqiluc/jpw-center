<?php

/**
 * This is the model class for table "siswa".
 *
 * The followings are the available columns in table 'siswa':
 * @property integer $id
 * @property string $nama_siswa
 * @property string $nip
 * @property string $jenjang
 * @property string $universitas
 * @property string $prodi
 * @property string $jenis_beasiswa
 * @property integer $status_pegawai
 * @property string $semester
 * @property string $bulan
 * @property string $deskripsi
 */
class Siswa extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'siswa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_siswa, nip, jenjang, universitas, prodi, jenis_beasiswa, status_pegawai, semester, bulan, deskripsi', 'required'),
			array('status_pegawai', 'numerical', 'integerOnly'=>true),
			array('nama_siswa, universitas, prodi', 'length', 'max'=>255),
			array('nip, jenjang, jenis_beasiswa, semester, bulan', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama_siswa, nip, jenjang, universitas, prodi, jenis_beasiswa, status_pegawai, semester, bulan, deskripsi', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama_siswa' => 'Nama Siswa',
			'nip' => 'Nip',
			'jenjang' => 'Jenjang',
			'universitas' => 'Universitas',
			'prodi' => 'Prodi',
			'jenis_beasiswa' => 'Jenis Beasiswa',
			'status_pegawai' => 'Status Pegawai',
			'semester' => 'Semester',
			'bulan' => 'Bulan',
			'deskripsi' => 'Deskripsi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama_siswa',$this->nama_siswa,true);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('jenjang',$this->jenjang,true);
		$criteria->compare('universitas',$this->universitas,true);
		$criteria->compare('prodi',$this->prodi,true);
		$criteria->compare('jenis_beasiswa',$this->jenis_beasiswa,true);
		$criteria->compare('status_pegawai',$this->status_pegawai);
		$criteria->compare('semester',$this->semester,true);
		$criteria->compare('bulan',$this->bulan,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Siswa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getStatus($data)
	{
		switch ($data) {
			case 0:
				return "Kontrak";
				break;
			case 1:
				return "Tetap";
				break;			
		}
	}
}
