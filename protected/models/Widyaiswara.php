<?php

/**
 * This is the model class for table "widyaiswara".
 *
 * The followings are the available columns in table 'widyaiswara':
 * @property integer $wd_id
 * @property string $nama_widyaiswara
 * @property string $nip
 * @property string $ttl
 * @property string $kategori
 * @property integer $pdk_trkhr
 * @property string $pengampu_mt_plth
 * @property string $pengalaman_krj
 * @property string $sertifikat
 * @property string $created
 * @property string $updated
 */
class Widyaiswara extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'widyaiswara';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama_widyaiswara, bidangId, nip, ttl, kategori, pdk_trkhr, pengampu_mt_plth, pengalaman_krj, created, updated', 'required'),
			array('pdk_trkhr', 'numerical', 'integerOnly'=>true),
			array('nama_widyaiswara, kategori, pengampu_mt_plth, sertifikat', 'length', 'max'=>255),
			array('nip', 'length', 'max'=>30),
			array('rate, review, bidangId, jurusan, kategori_detail,wi', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('wd_id, nama_widyaiswara, nip, ttl, kategori, pdk_trkhr, pengampu_mt_plth, pengalaman_krj, sertifikat, created, updated,bidangId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Bidang' => array(self::BELONGS_TO, 'Bidang', 'bidangId'),			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'wd_id' => 'Widyaiswara ID',
			'nama_widyaiswara' => 'Nama Widyaiswara',
			'nip' => 'NIP',
			'ttl' => 'Tanggal Lahir',
			'bidangId' => 'Bidang',
			'kategori' => 'Kategori',
			'pdk_trkhr' => 'Pendidikan Terakhir',
			'pengampu_mt_plth' => 'Pengampu Mat Pelatihan',
			'pengalaman_krj' => 'Pengalaman Kerja',
			'sertifikat' => 'Sertifikat',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($type)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('wd_id',$this->wd_id);
		$criteria->compare('bidangId',$type);
		$criteria->compare('nama_widyaiswara',$this->nama_widyaiswara,true);
		$criteria->compare('nip',$this->nip,true);
		$criteria->compare('ttl',$this->ttl,true);
		$criteria->compare('kategori',$this->kategori,true);
		$criteria->compare('pdk_trkhr',$this->pdk_trkhr);
		$criteria->compare('pengampu_mt_plth',$this->pengampu_mt_plth,true);
		$criteria->compare('pengalaman_krj',$this->pengalaman_krj,true);
		$criteria->compare('sertifikat',$this->sertifikat,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Widyaiswara the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getPdk($data){
		if ($data == 1){
			return "S3";
		}elseif ($data == 2){
			return "S2";
		}elseif ($data == 3){
			return "S1";
		}elseif ($data == 4){
			return "SMA";
		}elseif ($data == 5){
			return "SMP";
		}		
	}	
}
