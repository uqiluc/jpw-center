<?php
/* @var $this BahanTayangController */
/* @var $model BahanTayang */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bahan-tayang-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_file'); ?>
		<?php echo $form->textField($model,'nama_file',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_file'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",
				'style'=>'width:100;','class' => 'form-control')
			);
			?>
		<?php echo $form->error($model,'bidang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tgl_dokumen'); ?>
		<?php echo $form->dateField($model,'tgl_dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'tgl_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'dokumen'); ?>
		<?php echo $form->fileField($model,'dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'dokumen'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->