<?php
/* @var $this BahanTayangController */
/* @var $dataProvider CActiveDataProvider */


$this->menu=array(
	array('label'=>'Tambah Bahan Tayang', 'url'=>array('create')),
	array('label'=>'Kelola Bahan Tayang', 'url'=>array('admin')),
);
?>

<h1>Bahan Tayang</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
