<div class="row">
<div class="col-lg-12">
	<?php 
	echo CHtml::link('<i class="fa fa-plus"></i> Tambah Bahan Tayang',
	array('create'),
	array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-edit"></i> Edit',
	array('update','id'=>$model->bhn_id),
	array('class' => 'btn btn-primary','title'=>'Edit Data'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-file"></i> Dokumen',
	array('gambar','id'=>$model->bhn_id),
	array('class' => 'btn btn-primary','title'=>'Edit Dokumen'));
	?>	
	<?php 
	echo CHtml::link('<i class="fa fa-th"></i> Kelola',
	array('admin'),
	array('class' => 'btn btn-primary','title'=>'Kelola Data'));
	?>
    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default pull-right'/>
	<hr>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions'=>array('class'=>"table"),	
	'attributes'=>array(
		'bhn_id',
		'nama_file',
		array(
			'name'=>'bidang',
			'type'=>'raw',
			'value'=>$model->Bidang->nama_bidang
		),		'tgl_dokumen',
		'dokumen',
		'created',
		'updated',
	),
)); ?>
</div>
</div>