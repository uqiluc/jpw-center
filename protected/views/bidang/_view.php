<?php
/* @var $this BidangController */
/* @var $data Bidang */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('bd_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->bd_id), array('view', 'id'=>$data->bd_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_bidang')); ?>:</b>
	<?php echo CHtml::encode($data->nama_bidang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />


</div>