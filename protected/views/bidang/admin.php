<?php
/* @var $this BidangController */
/* @var $model Bidang */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bidang-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="row">
    <div class="col-lg-12">
	<div class="box box">
    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-book"></i> Bidang</h3>
		<div class="pull-right">
		<?php 
		echo CHtml::link('<i class="fa fa-plus"></i> Create',
		array('create'),
		array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
		array('excel'),
		array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
		?>
		</div>
    </div>
    <div class="box-body">

	<div class="table table-responsive">
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'bidang-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'itemsCssClass'=>'table table-striped table-bordered table-hover',
		'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
				'htmlOptions'=>array('width'=>'10px', 
				'style' => 'text-align: center; background-color: #3c8dbc; color:#ffffff;')
			),
			'nama_bidang',
			'created',
			'updated',
			array(
				'header'=>'Aksi',
				'class'=>'CButtonColumn',
				'htmlOptions'=>array('width'=>'150px', 
				'style' => 'text-align: center;'),
			),
		),
	)); ?>
	</div>

	</div>
	</div>
	</div>
</div>	