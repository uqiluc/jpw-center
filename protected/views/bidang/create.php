<div class="row">
    <div class="col-lg-12">
	<div class="box">
    <div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> Tambah Bidang</h3>
    <div class="pull-right">
        <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>
    </div>    
    </div>
    <div class="box-body">
	<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
	</div>
	</div>
	</div>
</div>	