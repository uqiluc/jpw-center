<?php
/* @var $this BidangController */
/* @var $dataProvider CActiveDataProvider */


$this->menu=array(
	array('label'=>'Tambah Bidang', 'url'=>array('create')),
	array('label'=>'Kelola Bidang', 'url'=>array('admin')),
);
?>

<h1>Bidang</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
