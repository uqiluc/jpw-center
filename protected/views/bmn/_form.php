<?php
/* @var $this BmnController */
/* @var $model Bmn */
/* @var $form CActiveForm */
?>
<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bmn-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),		
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tipe'); ?>
		<?php echo $form->dropDownList($model,'tipe', 
			array(
				'0'=>'Intra',
				'1'=>'Ekstra',
			),
			array("empty"=>"-- Pilih Kategori --",
			'style'=>'width:100;','class' => 'form-control')
		);?>	
		<?php echo $form->error($model,'tipe'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kode_barang'); ?>
		<?php echo $form->textField($model,'kode_barang',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'kode_barang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_barang'); ?>
		<?php echo $form->textField($model,'nama_barang',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_barang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nup'); ?>
		<?php echo $form->numberField($model,'nup',array('class' => 'form-control')); ?>
		<?php echo $form->error($model,'nup'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'merk'); ?>
		<?php echo $form->textField($model,'merk',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'merk'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'satuan'); ?>
		<?php echo $form->textField($model,'satuan',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'satuan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'thn_peroleh'); ?>
		<?php echo $form->textField($model,'thn_peroleh',array('size'=>4,'maxlength'=>4,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'thn_peroleh'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kondisi'); ?>
		<?php echo $form->textField($model,'kondisi',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'kondisi'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'keterangan'); ?>
		<?php echo $form->textArea($model,'keterangan',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'keterangan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'img_url'); ?>
		<?php echo $form->fileField($model,'img_url',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'img_url'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->