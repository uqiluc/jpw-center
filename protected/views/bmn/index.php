<?php
/* @var $this BmnController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bmns',
);

$this->menu=array(
	array('label'=>'Create Bmn', 'url'=>array('create')),
	array('label'=>'Manage Bmn', 'url'=>array('admin')),
);
?>

<h1>Bmns</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
