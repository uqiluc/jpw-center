<?php
    $this->pageTitle="Dokumen";
    $this->breadcrumbs=array(
        'BMN'=>array('admin'),
        'View'
    );
?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_barang;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->kode_barang;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
		<?php 
		echo CHtml::link('<i class="fa fa-edit"></i> Edit',
		array('update','id'=>$model->bmn_id),
		array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-th"></i> Data BMN',
		array('admin'),
		array('class' => 'btn btn-default','title'=>'Kelola Data'));
		?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>
<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-4">
      <div class="card h-100">
        <div class="card-body p-4">
          <img src="<?php echo Yii::app()->baseUrl;?>/Dokumen/bmn/<?php echo $model->img_url;?>" width="100%">        
        </div>
	    </div>
	  </div>
    <div class="col-12 col-xl-8">
      <div class="card h-100">
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Tipe</strong> <br> <?php echo $model->tipe;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Nup</strong> <br> <?php echo $model->nup;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Merk</strong> <br> <?php echo $model->merk;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Satuan</strong> <br> <?php echo $model->satuan;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tahun Peroleh</strong> <br> <?php echo $model->thn_peroleh;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Kondisi</strong> <br> <?php echo $model->kondisi;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Keterangan</strong> <br> <?php echo $model->keterangan;?></li>
          </ul>         
        </div>
      </div>
    </div>
</div>
</div>