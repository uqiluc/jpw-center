<?php
/* @var $this EvaluasiController */
/* @var $model Evaluasi */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'evaluasi-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
	'enableAjaxValidation'=>false,
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList ($model, 'category_id',
			CHtml::listData(KategoriEvaluasi::model()->findAll(),'id', 'name'),
			array("empty"=>"-- Pilih Kategori --",
				'style'=>'width:100;','class' => 'form-control','required'=>true)
			);
		?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type', 
								[
									'1'=>'Laporan Triwulan',
									'2'=>'Laporan Semester',
									'3'=>'Laporan Masukan Terhadap Pengajar',
									'4'=>'Laporan Manajemen Penyelenggaraan',
									'5'=>'Laporan Masukan Pengajar Terhadap Pemateri',
									'6'=>'Laporan FGD',
									'7'=>'Laporan Lokakarya',
									'8'=>'Laporan Akhir',
								]
								,array("empty"=>"-- Pilih Type --",
								'style'=>'width:100;','class' => 'form-control')
							);
		?>	
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'file_url'); ?>
		<?php echo $form->fileField($model,'file_url',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'file_url'); ?>
	</div>


	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->