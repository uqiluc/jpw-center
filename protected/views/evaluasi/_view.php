<?php
/* @var $this EvaluasiController */
/* @var $data Evaluasi */
$baseUrl = Yii::app()->baseurl;
?>
<div class="col-4">
	<div class="card mb-4">
	<div class="card-body px-0 pt-0 pb-2 text-center">
		<div style="padding: 30px;">
			<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
		</div>
		<b><?php echo $data->name;?></b><br>
		<hr class="horizontal dark my-1">
		<br>
		<?php if ($data->name == 'Laporan Pengendalian Mutu') { ?>
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_mutu" >Pilih</button>
		<?php } else if ($data->name == 'Laporan Pasca') { ?>
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_pasca" >Pilih</button>
		<?php } else { ?>
			<a href="<?php echo $baseUrl;?>/evaluasi/admin?Evaluasi[category_id]=<?php echo $data->id;?>" class="btn btn-primary">Pilih</a>
		<?php } ?>
	</div>
	</div>
</div>

<div class="modal fade" id="modal_mutu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Laporan Pengendalian Mutu</h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<div class="row">
				<div class="col-6">
					<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=2&Evaluasi[type]=1';?>" class="btn btn-primary" style="display:block">
						Laporan Triwulan	
					</a>
				</div>
				<div class="col-6">
				<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=2&Evaluasi[type]=2';?>" class="btn btn-primary" style="display:block">
						Laporan Semester	
					</a>
				</div>
				<div class="col-6">
				<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=2&Evaluasi[type]=3';?>" class="btn btn-primary" style="display:block">
						Laporan Masukan Terhadap Pengajar	
					</a>
				</div>
				<div class="col-6">
				<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=2&Evaluasi[type]=4';?>" class="btn btn-primary" style="display:block">
						Laporan Manajemen Penyelenggaraan	
					</a>
				</div>
				<div class="col-6">
				<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=2&Evaluasi[type]=5';?>" class="btn btn-primary" style="display:block">
						Laporan Masukan Pengajar Terhadap Pemateri	
					</a>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_pasca" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4>Laporan Pasca</h4>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
			<div class="row">
				<div class="col-6">
				<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=3&Evaluasi[type]=6';?>" class="btn btn-primary" style="display:block">
						Laporan FGD	
					</a>
				</div>
				<div class="col-6">
				<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=3&Evaluasi[type]=7';?>" class="btn btn-primary" style="display:block">
						Laporan Lokakarya	
					</a>
				</div>
				<div class="col-6">
				<a href="<?php echo $baseUrl.'/evaluasi/admin?Evaluasi[category_id]=3&Evaluasi[type]=8';?>" class="btn btn-primary" style="display:block">
						Laporan Akhir	
					</a>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>