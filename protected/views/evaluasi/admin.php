<?php
/* @var $this EvaluasiController */
/* @var $model Evaluasi */
$this->pageTitle="Evaluasi";
$this->breadcrumbs=array(
	'Evaluasis'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#evaluasi-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
            	<div class="pull-right">
					<?php 
					echo CHtml::link('<i class="fa fa-plus"></i> Create',
					array('create'),
					array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
					array('excel'),
					array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
					?>
	            </div>
	            <h6>Data Evaluasi</h6>
	        </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
	              <div class="dataTable-container">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'evaluasi-grid',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'header'=>'#',
								'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
								'htmlOptions'=>array('width'=>'10px','style'=>'text-align:center')
							),
							array(
								'name'=>'name','type'=>'raw',
								'value'=>'CHtml::link($data->name,array("karyasiswa/view","id"=>$data->id),array("class"=>"badge badge-sm bg-gradient-secondary"))',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'category_id','type'=>'raw',
							  	'value'=>'$data->Category->name',
								'filter'=>CHtml::listData(KategoriEvaluasi::model()->findAll(),'id', 'name'),
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'type','type'=>'raw',
							  	'value'=>'Evaluasi::model()->getType($data->type)',
								'filter'=>[
									'1'=>'Laporan Triwulan',
									'2'=>'Laporan Semester',
									'3'=>'Laporan Masukan Terhadap Pengajar',
									'4'=>'Laporan Manajemen Penyelenggaraan',
									'5'=>'Laporan Masukan Pengajar Terhadap Pemateri',
									'6'=>'Laporan FGD',
									'7'=>'Laporan Lokakarya',
									'8'=>'Laporan Akhir',
								],
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'file_url','type'=>'raw',
								'value'=>'CHtml::link("Download",array("karyasiswa/view","id"=>$data->file_url),array("class"=>"badge badge-sm bg-gradient-secondary"))',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'created_at','type'=>'raw',
							  	'value'=>'$data->created_at',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'header'=>'Aksi',
								'class'=>'CButtonColumn',
								'template'=>'{view} {delete}',
								'htmlOptions'=>array('width'=>'100px', 
								'style' => 'text-align: center;'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
						),
					)); ?>
	              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>