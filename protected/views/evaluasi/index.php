<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Evaluasi";
$this->breadcrumbs=array(
	'Evaluasi'=>array('index')
);

?>
 <div class="container-fluid py-4">
	<?php $this->widget('zii.widgets.CListView', array(
		'dataProvider'=>$dataProvider,
		'itemView'=>'_view',
		'summaryText'=>'',
		'ajaxUpdate'   => false,
		'enablePagination' => false,
		'itemsCssClass'=>'row',
		'pager' => array(
				'header' => '',
			),			
	)); ?>
</div>