<?php
/* @var $this EvaluasiController */
/* @var $model Evaluasi */

$this->breadcrumbs=array(
	'Evaluasis'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Evaluasi', 'url'=>array('index')),
	array('label'=>'Create Evaluasi', 'url'=>array('create')),
	array('label'=>'View Evaluasi', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Evaluasi', 'url'=>array('admin')),
);
?>

<h1>Update Evaluasi <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>