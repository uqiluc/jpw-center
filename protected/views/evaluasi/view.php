<?php
/* @var $this EvaluasiController */
/* @var $model Evaluasi */
$this->pageTitle="Evaluasi";
$this->breadcrumbs=array(
	'Evaluasis'=>array('index'),
	$model->name,
);

?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-5">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->name;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->Category->name;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-7 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
		<?php 
		echo CHtml::link('<i class="fa fa-edit"></i> Edit',
		array('update','id'=>$model->id),
		array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-file"></i> Edit',
		array('updateG','id'=>$model->id),
		array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-th"></i> Program & Evaluasi',
		array('programevaluasi'),
		array('class' => 'btn btn-default','title'=>'Kelola Data'));
		?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>
<div class="container-fluid py-1">
<div class="row">
    <div class="col-12 col-xl-12">
      <div class="card">
        <div class="card-body p-3">
			<?php $this->widget('zii.widgets.CDetailView', array(
				'data'=>$model,
				'htmlOptions'=>array('class'=>"table table-responsive table-flush dataTable-table"),			
				'attributes'=>array(
					array(
						'name'=>'type','type'=>'raw',
						'value'=>Evaluasi::model()->getType($model->type),
						'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
					),
					array(
						'name'=>'file_url','type'=>'raw',
						'value'=>CHtml::link("Download",array("karyasiswa/view","id"=>$model->file_url),array("class"=>"badge badge-sm bg-gradient-secondary")),
						'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
					),
					'created_at',
				),
			)); ?>
        </div>
      </div>
    </div>
</div>
</div>
