<?php
/* @var $this KaryasiswaController */
/* @var $model Karyasiswa */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'karyasiswa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>


	<div class="form-group">
		<?php echo $form->labelEx($model,'prodi'); ?>
		<?php echo $form->textField($model,'prodi',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'prodi'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'universitas'); ?>
		<?php echo $form->textField($model,'universitas',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'universitas'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'thn_masuk'); ?>
		<?php echo $form->numberField($model,'thn_masuk',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'thn_masuk'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'mulai_kuliah'); ?>
		<?php echo $form->dateField($model,'mulai_kuliah',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'mulai_kuliah'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'perkiraan_tahun_lulus'); ?>
		<?php echo $form->textField($model,'perkiraan_tahun_lulus',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'perkiraan_tahun_lulus'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas'); ?>
		<?php echo $form->textField($model,'kelas',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'kelas'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_karyasiswa'); ?>
		<?php echo $form->textField($model,'nama_karyasiswa',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_karyasiswa'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nip'); ?>
		<?php echo $form->numberField($model,'nip',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nip'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'no_hp'); ?>
		<?php echo $form->numberField($model,'no_hp',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'no_hp'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status_kepegawaian'); ?>
		<?php echo $form->dropDownList($model,'status_kepegawaian', 
								array(
									'ASN Pusat'=>'ASN Pusat',
									'ASN Daerah'=>'ASN Daerah',
									'Lain-Lain'=>'Lain-Lain',
								),array("empty"=>"-- Pilih Status Kepegawaian --",
								'style'=>'width:100;','class' => 'form-control','onchange'=>'otherStatus()')
							);
		?>	
		<?php echo $form->textField($model,'other_status_kepegawaian',array('size'=>60,'maxlength'=>255,'class' => 'form-control','style'=>'display:none','placeholder'=>'Isi status kepegawaian lainnya disini')); ?>
		<?php echo $form->error($model,'status_kepegawaian'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'unit_kerja'); ?>
		<?php echo $form->textField($model,'unit_kerja',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'unit_kerja'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ipk'); ?>
		<?php echo $form->textField($model,'ipk',array('size'=>5,'maxlength'=>5,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'ipk'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'sts_pdk'); ?>
		<?php echo $form->dropDownList($model,'sts_pdk', 
								array('Alumni'=>'Alumni',
									'Kuliah'=>'Kuliah',
									'DO'=>'DO',)
								,array("empty"=>"-- Pilih Status --",
								'style'=>'width:100;','class' => 'form-control')
							);
		?>	
		<?php echo $form->error($model,'sts_pdk'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'progress'); ?>
		<?php echo $form->dateField($model,'progress',array('size'=>5,'maxlength'=>5,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'progress'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'catatan_pdk'); ?>
		<?php echo $form->textArea($model,'catatan_pdk',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'catatan_pdk'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'thn_lulus'); ?>
		<?php echo $form->numberField($model,'thn_lulus',array('size'=>5,'maxlength'=>5,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'thn_lulus'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ijazah'); ?>
		<?php echo $form->numberField($model,'ijazah',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'ijazah'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
function otherStatus(){
    var idStatus = document.getElementById("Karyasiswa_status_kepegawaian");
    var otherStatus = document.getElementById("Karyasiswa_other_status_kepegawaian");
	    if (idStatus.value == 'Lain-Lain'){
		    otherStatus.style.display = 'block';
			console.log("ada");
		} else {
		    otherStatus.style.display = 'none';
		    otherStatus.style.value = '';
			console.log("kosong");
		}
}
</script>