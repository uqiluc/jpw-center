<?php
/* @var $this KaryasiswaController */
/* @var $model Karyasiswa */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'kry_id'); ?>
		<?php echo $form->textField($model,'kry_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_karyasiswa'); ?>
		<?php echo $form->textField($model,'nama_karyasiswa',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sts_pdk'); ?>
		<?php echo $form->textField($model,'sts_pdk',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'thn_masuk'); ?>
		<?php echo $form->textField($model,'thn_masuk'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'thn_lulus'); ?>
		<?php echo $form->textField($model,'thn_lulus'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'universitas'); ?>
		<?php echo $form->textField($model,'universitas',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prodi'); ?>
		<?php echo $form->textField($model,'prodi',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ijazah'); ?>
		<?php echo $form->textField($model,'ijazah',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ipk'); ?>
		<?php echo $form->textField($model,'ipk',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'catatan_pdk'); ?>
		<?php echo $form->textArea($model,'catatan_pdk',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->