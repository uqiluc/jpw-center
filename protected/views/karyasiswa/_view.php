<?php
/* @var $this KaryasiswaController */
/* @var $data Karyasiswa */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kry_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kry_id), array('view', 'id'=>$data->kry_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_karyasiswa')); ?>:</b>
	<?php echo CHtml::encode($data->nama_karyasiswa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sts_pdk')); ?>:</b>
	<?php echo CHtml::encode($data->sts_pdk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('thn_masuk')); ?>:</b>
	<?php echo CHtml::encode($data->thn_masuk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('thn_lulus')); ?>:</b>
	<?php echo CHtml::encode($data->thn_lulus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('universitas')); ?>:</b>
	<?php echo CHtml::encode($data->universitas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prodi')); ?>:</b>
	<?php echo CHtml::encode($data->prodi); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('ijazah')); ?>:</b>
	<?php echo CHtml::encode($data->ijazah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ipk')); ?>:</b>
	<?php echo CHtml::encode($data->ipk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catatan_pdk')); ?>:</b>
	<?php echo CHtml::encode($data->catatan_pdk); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>