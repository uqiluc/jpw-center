<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Karyasiswa";
$this->breadcrumbs=array(
	'Karyasiswa'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <?php foreach (Karyasiswa::model()->findAll('universitas != "" GROUP BY universitas') AS $model) { ;?>
        <div class="col-3">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/univ/<?php echo strtoupper($model->universitas);?>.png" width="100%">
            	</div>
            	<b><?php echo strtoupper($model->universitas);?></b><br>
          		<hr class="horizontal dark my-1">
          		<br>
							<!-- Button trigger modal -->
							<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_<?php echo $model->kry_id;?>" >Pilih</button>

							<!-- Modal -->
							<div class="modal fade" id="modal_<?php echo $model->kry_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							  <div class="modal-dialog modal-dialog-centered" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <h5 class="modal-title" id="exampleModalLabel">Pilih Angkatan - <?php echo strtoupper($model->universitas);?></h5>
							        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
							          <span aria-hidden="true">&times;</span>
							        </button>
							      </div>
							      <div class="modal-body">
				      	      <div class="row">
        							<?php foreach (Karyasiswa::model()->findAll('universitas = "'.$model->universitas.'" GROUP BY thn_masuk') AS $univ) { ?>
								        <div class="col-4">
	        								<a href="<?php echo $baseUrl.'/karyasiswa/univ?univ='.$model->universitas.'&thn='.$univ->thn_masuk;?>" class="btn btn-primary">
	        									Angkatan <?php echo $univ->thn_masuk;?>	
	        								</a>
        								</div>
        							<?php } ?>
								      </div>
							      </div>
							    </div>
							  </div>
							</div>
						</div>
				  </div>
				</div>
				<?php } ?>

	  </div>
</div>

<script type="text/javascript">

function getModal(val) {
	// Get the modal
	var modal = document.getElementById("myModal_"+val);
	// Get the button that opens the modal
	var btn = document.getElementById("myBtn_"+val);
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];

	// When the user clicks the button, open the modal 
	btn.onclick = function() {
	  modal.style.display = "block"; 
	}

	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	  modal.style.display = "none";
	}

	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	  if (event.target == modal) {
	    modal.style.display = "none";
	  }
	}	
}	
</script>