<?php
    $this->pageTitle="Karyasiswa";
    $this->breadcrumbs=array(
        'Karyasiswa'=>array('admin'),
        'View'
    );
?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_karyasiswa;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->nip;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
		<?php 
		echo CHtml::link('<i class="fa fa-edit"></i> Edit',
		array('update','id'=>$model->kry_id),
		array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-th"></i> Data Karyasiswa',
		array('index'),
		array('class' => 'btn btn-default','title'=>'Kelola Data'));
		?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>
<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-6">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Profil</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">No HP</strong> <br> <?php echo $model->no_hp;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Status Kepegawaian</strong> <br> <?php echo $model->status_kepegawaian;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Unit Kerja</strong> <br> <?php echo $model->unit_kerja;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">IPK</strong> <br> <?php echo $model->ipk;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Status Kelulusan</strong> <br> <?php echo $model->sts_pdk;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Progress</strong> <br> <?php echo $model->progress;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Keterangan</strong> <br> <?php echo $model->catatan_pdk;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Lulus Tahun</strong> <br> <?php echo $model->thn_lulus;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Ijazah</strong> <br> <?php echo $model->ijazah;?></li>
          </ul>        	
		</div>
	  </div>
	</div>
    <div class="col-12 col-xl-6">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Universitas</h6>
          <hr class="horizontal gray-light my-1">          
        </div>
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Prodi</strong> <br> <?php echo $model->prodi;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Universitas</strong> <br> <?php echo $model->universitas;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Angkatan</strong> <br> <?php echo $model->thn_masuk;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Mulai Kuliah</strong> <br> <?php echo $model->mulai_kuliah;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Perkiraan Tahun Lulus</strong> <br> <?php echo $model->perkiraan_tahun_lulus;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Kelas</strong> <br> <?php echo $model->kelas;?></li>
          </ul>
		</div>
    </div>
  </div>
</div>
</div>