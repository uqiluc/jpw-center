<?php
// Skrip berikut ini adalah skrip yang bertugas untuk meng-export data tadi ke excell
date_default_timezone_set('Asia/Jakarta');

$date = date('Y-m-d H:i:s');

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Kalendar Kegiatan - $date.xls");
?>
<h3>Data Kalendar Kegiatan</h3>
<small>Pengunduhan tanggal : <?php echo $date;?></small>
<table border="1" cellpadding="5">
    <th width="10px">No</th>
    <th><center>Nama Kegiatan</center></th>
    <th><center>Tanggal Kegiatan</center></th>
    <th><center>Keterangan Kegiatan</center></th>
    <th><center>Lokasi Kegiatan</center></th>
<?php
    $no = 0; 
    foreach (Kegiatan::getKegiatan() as $data) {   
    $no++;
?>  
    <tr>
        <td><?php echo $no;?></td>
        <td><?php echo $data['nama_kegiatan'];?></td>
        <td><?php echo Kegiatan::model()->hari_ini($data['tgl_kegiatan']);?></td>
        <td><?php echo $data['ktg_kegiatan'];?></td>
        <td><?php echo $data['lokasi_kegiatan'];?></td>
    </tr>
<?php } ?> 
</table>