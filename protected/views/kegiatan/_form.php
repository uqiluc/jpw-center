<?php
/* @var $this KegiatanController */
/* @var $model Kegiatan */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kegiatan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>


	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_kegiatan'); ?>
		<?php echo $form->textField($model,'nama_kegiatan',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_kegiatan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tgl_kegiatan'); ?>
		<?php echo $form->dateField($model,'tgl_kegiatan',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>		
		<?php echo $form->error($model,'tgl_kegiatan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ktg_kegiatan'); ?>
		<?php echo $form->dropDownList($model,'ktg_kegiatan', 
								array('Rapat'=>'Rapat',
									'Kordinasi'=>'Kordinasi',
									'Kunjungan'=>'Kunjungan',
									'Kegiatan Intern'=>'Kegiatan Intern',
									'Kegiatan Ekstern'=>'Kegiatan Ekstern',
									'Kegiatan Pelatihan'=>'Kegiatan Pelatihan',
								),array("empty"=>"-- Pilih Kegiatan --",
								'style'=>'width:100;','class' => 'form-control')
							);
		?>	
		<?php echo $form->error($model,'ktg_kegiatan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'lokasi_kegiatan'); ?>
		<?php echo $form->textField($model,'lokasi_kegiatan',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'lokasi_kegiatan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'person_in_charge'); ?>
		<?php echo $form->textField($model,'person_in_charge',array('size'=>10,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'person_in_charge'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->