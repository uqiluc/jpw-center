<?php
/* @var $this KegiatanController */
/* @var $data Kegiatan */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kg_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kg_id), array('view', 'id'=>$data->kg_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_kegiatan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_kegiatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_kegiatan')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_kegiatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ktg_kegiatan')); ?>:</b>
	<?php echo CHtml::encode($data->ktg_kegiatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lokasi_kegiatan')); ?>:</b>
	<?php echo CHtml::encode($data->lokasi_kegiatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('person_in_charge')); ?>:</b>
	<?php echo CHtml::encode($data->person_in_charge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>