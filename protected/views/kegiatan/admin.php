<?php
/* @var $this KegiatanController */
/* @var $model Kegiatan */
$this->pageTitle="Kegiatan";
$this->breadcrumbs=array(
	'Kegiatan'=>array('admin')
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#kegiatan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
            	<div class="pull-right">
				<?php 
				echo CHtml::link('<i class="fa fa-plus"></i> Create',
				array('create'),
				array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
				?>
				<?php 
				echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
				array('excel'),
				array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
				?>
				</div>
	            <h6>Data Kegiatan</h6>
	        </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
	              <div class="dataTable-container">
	              	<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'kegiatan-grid',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'header'=>'#',
								'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
								'htmlOptions'=>array('width'=>'10px','style'=>'text-align:center')
							),
							array(
								'name'=>'nama_kegiatan','type'=>'raw',
							  	'value'=>'CHtml::link(substr($data->nama_kegiatan,0,30)."...",array("kegiatan/view","id"=>$data->kg_id),array("class"=>"badge badge-sm bg-gradient-secondary"))',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'tgl_kegiatan','type'=>'raw',
								'value'=>'$data->tgl_kegiatan',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')								
							),
							array(
								'name'=>'person_in_charge','type'=>'raw',
								'value'=>'$data->person_in_charge',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')								
							),
							array(
								'header'=>'Aksi',
								'class'=>'CButtonColumn',
								'template'=>'{update} {delete}',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),

			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')								
							),
						),
					)); ?>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>