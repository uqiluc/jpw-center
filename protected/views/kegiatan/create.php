<?php
    $this->pageTitle="Kegiatan";
    $this->breadcrumbs=array(
        'Kegiatan'=>array('admin'),
        'Create'
    );
?>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
                <div class="pull-right">
                    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>
                </div>
                <h6>Create Kegiatan</h6>
            </div>
            <div class="card-body">
                <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
            </div>
          </div>
        </div>
    </div>  
</div>  