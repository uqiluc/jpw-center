<?php
/* @var $this KegiatanController */
/* @var $dataProvider CActiveDataProvider */
date_default_timezone_set('Asia/Jakarta');

$Url = Yii::app()->baseUrl; 
$now = date('Y-m-d');
$kegiatanNow = Yii::app()->db->createCommand("SELECT nama_kegiatan FROM kegiatan WHERE tgl_kegiatan='$now' ORDER BY kg_id ASC LIMIT 1")->queryScalar();
$lokasi_kegiatan = Yii::app()->db->createCommand("SELECT lokasi_kegiatan FROM kegiatan WHERE tgl_kegiatan='$now' ORDER BY kg_id ASC LIMIT 1")->queryScalar();

$this->pageTitle="Kegiatan";
$this->breadcrumbs=array(
	'Kalendar Kegiatan'=>array('index')
);

?>
 <div class="container-fluid py-4">
      <div class="row">
        <div class="col-8">
          <div class="card mb-4">
            <div class="card-header pb-0 px-3">
	            <h6>Kalender Kegiatan</h6>
	        	</div>
            <div class="card-body px-3 pt-0 pb-2">
						<?php $this->widget('ext.fullcalendar.EFullCalendarHeart', array(
							//'themeCssFile'=>'cupertino/jquery-ui.min.css',
							'options'=>array(
								'header'=>array(
									'left'=>'prev,next,today',
									'center'=>'title',
									'right'=>'month,agendaWeek,agendaDay',
								),
								'events'=>$this->createUrl('kegiatan/calendarEvents'), // URL to get event
								'eventClick'=> 'js:function(calEvent, jsEvent, view) {
							        $("#myModalHeader").html(calEvent.title);
							        $("#myModalBody").load("'.Yii::app()->createUrl("kegiatan/view/id/").'/"+calEvent.id+"?asModal=true");
							        $("#myModal").modal();
							    }',
							)));
						?>
						</div>
					</div>
				</div>
        <div class="col-4">
          <div class="card mb-4">
            <div class="card-header pb-0 px-3">
							Today, <?php echo date('d F Y');?>
	        	</div>
            <div class="card-body px-3 pt-0 pb-2">
						<div class="add-calendar">
							<?php 
							if ($kegiatanNow != ''){
							echo "<b>".$kegiatanNow."</b>";?><br>
							<div style="display: flex;">
								<i class="fa fa-map-marker" style="font-size: 15px;margin-right: 10px"></i> 
								<div style="font-size: 12px;display: inline;">
									<?php echo $lokasi_kegiatan;?>
								</div>
							</div>
							<?php } else {?>
								<b>Tidak ada kegiatan hari ini</b>
							<?php }?>
						</div>
						<hr>
						<?php if (Yii::app()->user->record->level == 1){ ?>
						<a href="<?php echo $Url;?>/kegiatan/create">
							<div class="add-calendar">
								<div class="pull-right"><i class="fa fa-plus"></i></div>
								Add<br><br>
								<div style="display: flex;">
								<i class="fa fa-calendar" style="font-size: 30px;margin-right: 10px"></i> 
								<div style="font-size: 12px;display: inline;">Set up your calendars to see where<br>You need to be</div>
								</div>
							</div>
						</a>
						<hr class="horizontal dark mt-3">
						<a href="<?php echo $Url;?>/kegiatan/admin">
							<div class="add-calendar">
								<div class="pull-right"><i class="fa fa-cog"></i></div>
								Manage<br><br>
								<div style="display: flex;">
								<i class="fa fa-th" style="font-size: 30px;margin-right: 10px"></i> 
								<div style="font-size: 12px;display: inline;">Manage kegiatan</div>
								</div>
							</div>
						</a>
						<hr class="horizontal dark mt-3">
						<a href="<?php echo $Url;?>/kegiatan/excel">
							<div class="add-calendar">
								<div class="pull-right"><i class="fa fa-print"></i></div>
								Print<br><br>
								<div style="display: flex;">
								<i class="fa fa-file-excel-o" style="font-size: 30px;margin-right: 10px"></i> 
								<div style="font-size: 12px;display: inline;">Export Kalendar Kegiatan kedalam file excel</div>
								</div>
							</div>
						</a>
						<hr class="horizontal dark mt-3">
						<?php } else { } ?>
						</div>
					</div>
				</div>
			</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalHeader" style="font-weight: 900">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p id="myModalBody"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>