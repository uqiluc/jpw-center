<?php
    $this->pageTitle="Kegiatan";
    $this->breadcrumbs=array(
        'Kegiatan'=>array('admin'),
        'View'
    );
?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_kegiatan;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            PIC : <?php echo $model->person_in_charge;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
			<?php if (Yii::app()->user->record->level == 1){
			echo CHtml::link('<i class="fa fa-edit"></i> Edit',
			array('update','id'=>$model->kg_id),
			array('class' => 'btn btn-default','title'=>'Edit'));
			?>
			<?php 
			echo CHtml::link('<i class="fa fa-times"></i> Delete',
			array('hapus','id'=>$model->kg_id),
			array('class' => 'btn btn-default','title'=>'Hapus Data'));
			} else { }
			?>
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>
<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-12">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Detail</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
			<?php $this->widget('zii.widgets.CDetailView', array(
				'data'=>$model,
			    'htmlOptions'=>array('class'=>"table"),	
				'attributes'=>array(
					'nama_kegiatan',
					array(
						'name'=>'tgl_kegiatan',
						'type'=>'raw',
						'value'=>Kegiatan::model()->hari_ini($model->tgl_kegiatan)
					),
					'ktg_kegiatan',
					'lokasi_kegiatan',
					// 'person_in_charge',
				),
			)); ?>
		</div>
	  </div>
	</div>
</div>
</div>