<?php
/* @var $this KurmodController */
/* @var $model Kurmod */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kurmod-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data','autocomplete'=>'off'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_dokumen'); ?>
		<?php echo $form->textField($model,'nama_dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenis_dokumen'); ?>
		<?php echo $form->dropDownList ($model, 'jenis_dokumen',
			array(
				'Modul'=>'Modul',
				'Buku Kurikulum'=>'Buku Kurikulum',
				'Bahan Tayang'=>'Bahan Tayang',
				'Pedoman Pelatihan'=>'Pedoman Pelatihan',
				'Buku Soal'=>'Buku Soal',
			),
			array("empty"=>"-- Jenis Dokuen --",'style'=>'width:100;','class'=>'form-control')
			);
			?>
		<?php echo $form->error($model,'bidang'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary btn-flat')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->