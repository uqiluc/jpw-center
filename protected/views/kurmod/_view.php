<?php
/* @var $this KurmodController */
/* @var $data Kurmod */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('km_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->km_id), array('view', 'id'=>$data->km_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bidang')); ?>:</b>
	<?php echo CHtml::encode($data->bidang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_pelatihan')); ?>:</b>
	<?php echo CHtml::encode($data->nm_pelatihan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

</div>