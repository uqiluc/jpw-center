<?php
/* @var $this KurmodController */
/* @var $model Kurmod */

?>

<div class="row">
    <div class="col-lg-12">
	<div class="box">
    <div class="box-header with-border">
    <div class="pull-right">
        <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-primary btn-flat'/>
    </div>    
    </div>
    <div class="box-body">
		<?php $this->renderPartial('_form', array('model'=>$model,'model2'=>$model2)); ?>	</div>
	</div>
	</div>
</div>	