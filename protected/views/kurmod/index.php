<?php
/* @var $this KurmodController */
/* @var $dataProvider CActiveDataProvider */


$this->menu=array(
	array('label'=>'Tambah Kurmod', 'url'=>array('create')),
	array('label'=>'Kelola Kurmod', 'url'=>array('admin')),
);
?>

<h1>Kurmod</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
