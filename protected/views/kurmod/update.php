<?php
/* @var $this KurmodController */
/* @var $model Kurmod */


$this->menu=array(
	array('label'=>'Daftar Kurmod', 'url'=>array('index')),
	array('label'=>'Tambah Kurmod', 'url'=>array('create')),
	array('label'=>'Lihat Kurmod', 'url'=>array('view', 'id'=>$model->km_id)),
	array('label'=>'Kelola Kurmod', 'url'=>array('admin')),
);
?>

<h1>Perbaharui Kurmod <?php echo $model->km_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>