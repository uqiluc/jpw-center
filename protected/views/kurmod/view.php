<?php
/* @var $this KurmodController */
/* @var $model Kurmod */


$this->menu=array(
	array('label'=>'Daftar Kurmod', 'url'=>array('index')),
	array('label'=>'Tambah Kurmod', 'url'=>array('create')),
	array('label'=>'Perbaharui Kurmod', 'url'=>array('update', 'id'=>$model->km_id)),
	array('label'=>'Hapus Kurmod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->km_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Kurmod', 'url'=>array('admin')),
);
?>

<h1>Lihat Kurmod #<?php echo $model->km_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'km_id',
		'bidang',
		'nm_pelatihan',
		'created',
		'updated',
	),
)); ?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$kurmoddetail,
	'columns'=>array(
		'kmd_id',
		'nama_dokumen',
		'jns_dokumen',
		'tanggal',
		'tim_penyusun',
		'cover_dokumen',
		'km_id',
		array(
            'name'=>'KM ID',
            'value'=>'KurmodDetail::model()->findByPk($data->kmd_id)->nama_dokumen',
        ),
	),
)); ?>