<?php
/* @var $this KurmodDetailController */
/* @var $model KurmodDetail */
/* @var $form CActiveForm */
$profId = $_GET['prof_id'];
$Pelatihan = Yii::app()->db->createCommand("SELECT nama_pelatihan FROM profil_pelatihan WHERE prof_id=$profId")->queryScalar();
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kurmod-detail-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>

	<div class="col-lg-12 form-group">
		<?php echo $form->labelEx($model,'Profile Pelatihan'); ?>
		<input required type="" name="" value="<?php echo $Pelatihan;?>" readonly="true" class="form-control">
	</div>

<div id="multipleform">
	<div class="col-lg-3 form-group">
	<label for="KurmodDetail_nama_dokumen">Nama Dokumen</label>		
	<input required size="60" maxlength="255" class="form-control" name="KurmodDetail[nama_dokumen][]" id="KurmodDetail_nama_dokumen" type="text"></div>

	<div class="col-lg-3 form-group">
	<label for="KurmodDetail_jns_dokumen">Jenis Dokumen</label>		
	<select style="width:100;" class="form-control" name="KurmodDetail[jns_dokumen][]" id="KurmodDetail_jns_dokumen">
		<option value="">-- Jenis Dokumen --</option>
		<option value="Modul">Modul</option>
		<option value="Buku Kurikulum">Buku Kurikulum</option>
		<option value="Bahan Tayang">Bahan Tayang</option>
		<option value="Pedoman Pelatihan">Pedoman Pelatihan</option>
		<option value="Buku Soal">Buku Soal</option>
	</select>	
	</div>

	<div class="col-lg-2 form-group">
	<label for="KurmodDetail_tim_penyusun">Tim Penyusun</label>		
	<input required size="60" maxlength="255" class="form-control" name="KurmodDetail[tim_penyusun][]" id="KurmodDetail_tim_penyusun" type="text">	</div>

	<div class="col-lg-2 form-group">
	<label for="KurmodDetail_cover_dokumen">Cover Dokumen</label>		
	<input required id="ytKurmodDetail_cover_dokumen" type="hidden" value="" name="KurmodDetail[cover_dokumen][]">
	<input required size="60" maxlength="255" class="form-control" name="KurmodDetail[cover_dokumen][]" id="KurmodDetail_cover_dokumen" type="file">			
	</div>

	<div class="col-lg-2 form-group">
	<label for="KurmodDetail_file_dokumen">File Dokumen</label>		
	<input required id="ytKurmodDetail_file_dokumen" type="hidden" value="" name="KurmodDetail[file_dokumen][]">
	<input required size="60" maxlength="255" class="form-control" name="KurmodDetail[file_dokumen][]" id="KurmodDetail_file_dokumen" type="file">			
	</div>

</div>

	<div id="MultiForm">
	</div>

	<div class="col-lg-12 form-group">
		<div class="pull-right">
	     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary','onClick'=>'uploadFile()')); ?>
		</div>
		    <input required onClick="tambah()" value="+" type="button" class="btn btn-primary">
			<input required onClick="hapus()" value="x" type="button" class="btn btn-danger" id="btnHapus" style="display: none">
	</div>
<div class="col-lg-12 form-group" id="progres" style="
    position: fixed;
    font-size: 8px;
    bottom: 0px;
    right: 15px;
    text-align: right;
    background-color: #19314c;
    padding: 15px;
    border-radius: 10px;
    color: #fff;
    width: 50%;
    display: none
">
	<progress id="progressBar" value="0" max="100" style="width:100%;height: 20px"></progress>
    <h3 id="status" style="margin-top: 0px;"></h3>
	<p id="total"></p>
</div>

<?php $this->endWidget(); ?>

<script language="JavaScript" type="text/JavaScript">
    function hps(x)
    {
     	z = x ;
    	document.getElementById("multipleform"+z).innerHTML = "";
    }
</script>

<script language="JavaScript" type="text/JavaScript">
function uploadFile() {
    // membaca data file yg akan diupload, dari komponen 'fileku'
    var file = document.getElementById("KurmodDetail_file_dokumen").files[0];
    var formdata = new FormData();
    formdata.append("KurmodDetail[file_dokumen][]", file);
     
    // proses upload via AJAX disubmit ke 'upload.php'
    // selama proses upload, akan menjalankan progressHandler()
    var ajax = new XMLHttpRequest();
    document.getElementById("progres").style.display='block';
    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.open("POST", "upload.php", true);
    ajax.send(formdata);
}
 
function progressHandler(event){
    // hitung prosentase
    var percent = (event.loaded / event.total) * 100;
    // menampilkan prosentase ke komponen id 'progressBar'
    document.getElementById("progressBar").value = Math.round(percent);
    // menampilkan prosentase ke komponen id 'status'
    document.getElementById("status").innerHTML = Math.round(percent)+"% telah terupload, tunggu sebentar";
    // menampilkan file size yg tlh terupload dan totalnya ke komponen id 'total'
    document.getElementById("total").innerHTML = "Telah terupload "+event.loaded+" bytes dari "+event.total;
}

counter = 1;
function tambah()
{
	counterNext = counter + 1;
	counterId = counter + 1;
    var para = document.createElement('div');
    para.innerHTML = '<div id="addForm'+counter+'"><div class="col-lg-3 form-group"><label for="KurmodDetail_nama_dokumen">Nama Dokumen</label><input required size="60" maxlength="255" class="form-control" name="KurmodDetail[nama_dokumen][]" id="KurmodDetail_nama_dokumen" type="text"></div><div class="col-lg-3 form-group"><label for="KurmodDetail_jns_dokumen">Jenis Dokumen</label><select style="width:100;" class="form-control" name="KurmodDetail[jns_dokumen][]" id="KurmodDetail_jns_dokumen"><option value="">-- Jenis Dokumen --</option><option value="Modul">Modul</option><option value="Buku Kurikulum">Buku Kurikulum</option><option value="Bahan Tayang">Bahan Tayang</option><option value="Pedoman Pelatihan">Pedoman Pelatihan</option><option value="Buku Soal">Buku Soal</option><select></div><div class="col-lg-2 form-group"><label for="KurmodDetail_tim_penyusun">Tim Penyusun</label><input required size="60" maxlength="255" class="form-control" name="KurmodDetail[tim_penyusun][]" id="KurmodDetail_tim_penyusun" type="text">	</div><div class="col-lg-2 form-group"><label for="KurmodDetail_cover_dokumen">Cover Dokumen</label><input required id="ytKurmodDetail_cover_dokumen" type="hidden" value="" name="KurmodDetail[cover_dokumen][]"><input required size="60" maxlength="255" class="form-control" name="KurmodDetail[cover_dokumen][]" id="KurmodDetail_cover_dokumen" type="file"></div><div class="col-lg-2 form-group"><label for="KurmodDetail_file_dokumen">File Dokumen</label><input required id="ytKurmodDetail_file_dokumen" type="hidden" value="" name="KurmodDetail[file_dokumen][]"><input required size="60" maxlength="255" class="form-control" name="KurmodDetail[file_dokumen][]" id="KurmodDetail_file_dokumen" type="file"></div></div>';
	document.getElementById('MultiForm').appendChild(para);
	document.getElementById('btnHapus').style.display = "";
    counter++;
}

function hapus(){
    var para = document.getElementById('MultiForm');
	para.removeChild(para.lastChild);
}
</script>