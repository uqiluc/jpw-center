<div class="col-lg-12">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'profil-pelatihan-form',
    'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data','autocomplete'=>'off'),
)); ?>

<div class="col-lg-8">
    <div class="form-group">
        <?php echo $form->labelEx($model,'file_dokumen'); ?>
        <?php echo $form->fileField($model,'file_dokumen',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
        <?php echo $form->error($model,'file_dokumen',array('class'=>'label label-warning')); ?>
    </div>
    
    <div class="form-group">    
        <?php echo CHtml::submitButton('Simpan', array('class' => 'btn btn-primary')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>

</div><!-- form -->