<?php
/* @var $this KurmodController */
/* @var $model Kurmod */
/* @var $form CActiveForm */
if ($model->prof_id != '') {
	$bidang = $model->ProfilPelatihan->bidang;
} else {
	$bidang = $_GET['bidang'];
}
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kurmod-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data','autocomplete'=>'off'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Profile Pelatihan'); ?>
		<?php echo $form->dropDownList ($model, 'prof_id',
			CHtml::listData(ProfilPelatihan::model()->findAll('bidang='.$bidang),'prof_id', 'nama_pelatihan'),
			array("empty"=>"-- Pilih Pelatihan --",
				'style'=>'width:100;','class' => 'form-control','required'=>true)
			);
			?>
		<?php echo $form->error($model,'prof_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_dokumen'); ?>
		<?php echo $form->textField($model,'nama_dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jns_dokumen'); ?>
		<?php echo $form->dropDownList ($model, 'jns_dokumen',
			array(
				'Modul'=>'Modul',
				'Buku Kurikulum'=>'Buku Kurikulum',
				'Bahan Tayang'=>'Bahan Tayang',
				'Pedoman Pelatihan'=>'Pedoman Pelatihan',
				'Buku Soal'=>'Buku Soal',
			),
			array("empty"=>"-- Jenis Dokumen --",'style'=>'width:100;','class'=>'form-control')
			);
			?>
		<?php echo $form->error($model,'jns_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tim_penyusun'); ?>
		<?php echo $form->textField($model,'tim_penyusun',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'tim_penyusun'); ?>
	</div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'cover_dokumen'); ?>
        <?php echo $form->fileField($model,'cover_dokumen',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
        <?php echo $form->error($model,'cover_dokumen',array('class'=>'label label-warning')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'file_dokumen'); ?>
        <?php echo $form->fileField($model,'file_dokumen',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
        <?php echo $form->error($model,'file_dokumen',array('class'=>'label label-warning')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'Link Url'); ?>
        <?php echo $form->textField($model,'gdriveUrl',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
        <?php echo $form->error($model,'gdriveUrl',array('class'=>'label label-warning')); ?>
    </div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->