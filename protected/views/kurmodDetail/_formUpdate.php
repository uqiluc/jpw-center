<?php
/* @var $this KurmodController */
/* @var $model Kurmod */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'kurmod-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data','autocomplete'=>'off'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_dokumen'); ?>
		<?php echo $form->textField($model,'nama_dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jns_dokumen'); ?>
		<?php echo $form->dropDownList ($model, 'jns_dokumen',
			array(
				'Modul'=>'Modul',
				'Buku Kurikulum'=>'Buku Kurikulum',
				'Bahan Tayang'=>'Bahan Tayang',
				'Pedoman Pelatihan'=>'Pedoman Pelatihan',
				'Buku Soal'=>'Buku Soal',
			),
			array("empty"=>"-- Jenis Dokuen --",'style'=>'width:100;','class'=>'form-control')
			);
			?>
		<?php echo $form->error($model,'jns_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tim_penyusun'); ?>
		<?php echo $form->textField($model,'tim_penyusun',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'tim_penyusun'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->