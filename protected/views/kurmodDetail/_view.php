<?php
/* @var $this KurmodDetailController */
/* @var $data KurmodDetail */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('kmd_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->kmd_id), array('view', 'id'=>$data->kmd_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->nama_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jns_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->jns_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tim_penyusun')); ?>:</b>
	<?php echo CHtml::encode($data->tim_penyusun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cover_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->cover_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('km_id')); ?>:</b>
	<?php echo CHtml::encode($data->km_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>