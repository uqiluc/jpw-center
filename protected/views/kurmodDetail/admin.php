<?php
/* @var $this KurmodDetailController */
/* @var $model KurmodDetail */
$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Kurikulum & Modul'=>array('admin')
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#kurmod-detail-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

 <div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
            	<div class="pull-right">
					<?php 
					echo CHtml::link('<i class="fa fa-plus"></i> Create',
					array('createKurmod?bidang='.$_GET['type']),
					array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
					array('excel'),
					array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
					?>
	            </div>
	            <h6>Data Kurikulum & Modul</h6>
	        </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
	              <div class="dataTable-container">
			   		<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'kurmod-detail-grid-jembatan',
						'dataProvider'=>$model->getKurmodTable($_GET['type']),
						'filter'=>$model,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'header'=>'#',
								'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							array(
								'name'=>'nama_dokumen','type'=>'raw',
								'value'=>'CHtml::link($data->ProfilPelatihan->nama_pelatihan,array("profilPelatihan/view?id=".$data->prof_id."&type=profile"),array("class"=>"badge badge-sm bg-gradient-secondary"));',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							array(
								'name'=>'tanggal',
								'value'=>'substr($data->tanggal,0,4)',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							array(
								'name'=>'updated',
								'value'=>'$data->updated',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							// array(
							// 	'name'=>'prof_id', 'type'=>'raw',
							// 	'value'=>'CHtml::link($data->ProfilPelatihan->nama_pelatihan,array("profilPelatihan/view?id=".$data->prof_id."&type=profile"),array("class"=>"badge badge-sm bg-gradient-secondary"));',
							// 	'filter'=>CHtml::listData(ProfilPelatihan::model()->findAll('bidang=7'),'prof_id', 'nama_pelatihan'),
							// 	'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  	// 			'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							array(
								'name'=>'isActive',
								'value'=>'KurmodDetail::model()->getStatusReady($data->isActive)',
								'filter'=>array('0'=>'No','1'=>'Ready'),
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							array(
								'name'=>'description',
								'value'=>'substr($data->description,0,20)."..."',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							array(
								'header'=>'Aksi',
								'class'=>'CButtonColumn',
								'template'=>'{update}{delete}',						
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
						),
					)); 
					?>
	   				</div>
	  			</div>
			</div>
		</div>
	  </div>
	</div>
</div>