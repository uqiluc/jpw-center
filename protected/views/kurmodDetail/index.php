<?php
/* @var $this KurmodDetailController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Tambah Kurmod Detail', 'url'=>array('create')),
	array('label'=>'Kelola Kurmod Detail', 'url'=>array('admin')),
);
?>

<h1>Kurmod Detail</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
