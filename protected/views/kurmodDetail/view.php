<?php
/* @var $this ProfilPelatihanController */
/* @var $model ProfilPelatihan */
$Url = Yii::app()->baseUrl; 
$theme = Yii::app()->theme->baseUrl; 

if ($model->cover_dokumen == '' || (substr($model->cover_dokumen,-3) == "pdf" || substr($model->cover_dokumen,-3) == "doc")){
	$imgprofil = $Url."/Dokumen/noImage.jpg";
}else{
	$imgprofil = $Url."/Dokumen/CoverDokumen/".$model->cover_dokumen;
}

$checkHistory = Yii::app()->db->createCommand("SELECT COUNT(KurmodId) FROM log_kurmod_pelatihan WHERE KurmodId = $model->kmd_id")->queryScalar(); 

?>
<div class="row">
<div class="col-lg-12">
<?php if (Yii::app()->user->record->level == 1){
	echo CHtml::link('<i class="fa fa-eye"></i> Lihat Dokumen',
	array('viewDokumen','id'=>$model->kmd_id),
	array('class' => 'btn btn-primary','title'=>'Edit Kurmod','target'=>'_blank'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-image"></i> Update Cover',
	array('cover','id'=>$model->kmd_id),
	array('class' => 'btn btn-primary','title'=>'Update Cover'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-file"></i> Update File',
	array('upload','id'=>$model->kmd_id),
	array('class' => 'btn btn-primary','title'=>'Update Cover'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-edit"></i> Edit',
	array('update','id'=>$model->kmd_id),
	array('class' => 'btn btn-primary','title'=>'Edit Kurmod'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-times"></i> Hapus',
	array('hapus','id'=>$model->kmd_id),
	array('class' => 'btn btn-danger','title'=>'Hapus'));
	?>
	<?php if (Yii::app()->user->record->level==1 && $checkHistory) {
	echo CHtml::link('<i class="fa fa-history"></i> View History',
	array('viewLog','id'=>$model->kmd_id),
	array('class' => 'btn btn-warning','title'=>'Lihat riwayat perubahan'));		
	}
} else {}
	?>	
	<div class="pull-right">
        <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>		
	</div>
	<hr>
</div>
<div class="col-lg-12">
<div class="col-lg-3">
<img src="<?php echo $imgprofil;?>" width="100%" style="border-radius: 10px;border: 1px solid #ddd;">
</div>
<div class="col-lg-9">
<p>
	Profil Pelatihan<br>
	<b><?php echo $model->ProfilPelatihan->nama_pelatihan;?></b>
</p>
<p>
	<?php echo $model->jns_dokumen;?><br>
	<b><?php echo $model->nama_dokumen;?></b>
</p>
<p>
	Nama Dokumen<br>
	<b><?php echo $model->file_dokumen;?></b>
</p>
<p>
	Tim Penyusun<br>
	<b><?php echo $model->tim_penyusun;?></b>
</p>
<p>
	Tanggal Upload<br>
	<b><?php echo $model->created;?></b>
</p>
<p>
	Tanggal Update<br>
	<b><?php echo $model->updated;?></b>
</p>
</div>
</div>
</div>