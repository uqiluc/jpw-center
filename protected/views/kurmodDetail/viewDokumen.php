<?php

$file = $_SERVER['DOCUMENT_ROOT']."/dmspu/Dokumen/CoverDokumen/".$model->file_dokumen;
$filename = $model->file_dokumen;
if (!file_exists($file)) {
    echo "The file $file does not exist<br>";
    echo $_SERVER['DOCUMENT_ROOT'];
    die();
}

header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="' . $filename . '"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file));
header('Accept-Ranges: bytes');

@readfile($file);

?>