<div class="row">
<div class="col-lg-12">
<div class="pull-right">
    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>		
</div>
<hr>
<?php 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
    'htmlOptions'=>array('class'=>"table"),	
	'attributes'=>array(
		array('label'=>'Profil Pelatihan','type'=>'raw',
		  	  'value'=>$model->ProfilPelatihan->nama_pelatihan,
		),
		array('label'=>'Nama Dokumen Saat Ini','type'=>'raw',
		  	  'value'=>$model->nama_dokumen,
		),
		array('label'=>'File Dokumen Saat Ini','type'=>'raw',
		  	  'value'=>$model->file_dokumen,
		),
		'tim_penyusun',
		'created',
		'updated'
	),
)); 	
?>
<hr>
<center><h4><b>Data Riwayat Perubahan data</b></h4></center>
	<?php $ttlpesrt = Yii::app()->db->createCommand("SELECT COUNT(id) FROM log_kurmod_pelatihan WHERE kurmodId = $model->kmd_id")->queryScalar(); ?>
	<b>Total :</b> <?php echo $ttlpesrt;?> Perubahan
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'log-profile-grid',
		'dataProvider'=>$log->getKurmod($model->kmd_id),
		'filter'=>$log,
		'itemsCssClass'=>'table table-striped table-bordered table-hover',	
		'columns'=>array(
			array(
				'header'=>'#',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
				'htmlOptions'=>array('width'=>'10px', 
				'style' => 'text-align: center; background-color: #3c8dbc; color:#ffffff;')
			),
			'initialName',
			'nameChange',
			'initialFile',
			'fileChange',
			'createTime',
			array(
				'name'=>'createdBy','type'=>'raw',
				'value'=>'$data->Users->nama_lengkap'
			)
		),
	)); ?>
<br>
</div>