<?php
/* @var $this LaporanKgtController */
/* @var $model LaporanKgt */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'laporan-kgt-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>


	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_laporan'); ?>
		<?php echo $form->textField($model,'nama_laporan',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_laporan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jns_dokumen'); ?>
		<?php echo $form->dropDownList($model,'jns_dokumen', 
								array('Laporan Kegiatan'=>'Laporan Kegiatan',
									'Laporan Akhir'=>'Laporan Akhir',)
									,array("empty"=>"-- Pilih Jenis Dokumen --",
									'style'=>'width:100;','class' => 'form-control')
								);
		?>	
		<?php echo $form->error($model,'jns_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",
				'style'=>'width:100;','class' => 'form-control')
			);
			?>
		<?php echo $form->error($model,'bidang'); ?>
	</div>
	
	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->