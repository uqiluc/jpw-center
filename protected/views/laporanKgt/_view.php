<?php
/* @var $this LaporanKgtController */
/* @var $data LaporanKgt */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('lpr_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->lpr_id), array('view', 'id'=>$data->lpr_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_laporan')); ?>:</b>
	<?php echo CHtml::encode($data->nama_laporan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jns_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->jns_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bidang')); ?>:</b>
	<?php echo CHtml::encode($data->bidang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>