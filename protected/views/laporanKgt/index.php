<?php
/* @var $this LaporanKgtController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Tambah Laporan Kegiatan', 'url'=>array('create')),
	array('label'=>'Kelola Laporan Kegitan', 'url'=>array('admin')),
);
?>

<h1>Laporan Kegiatan</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
