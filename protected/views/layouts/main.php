<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Beranda', 'url'=>array('/site/index'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
			),
		)); ?>
	</div><!-- mainmenu -->

	<?php if (Yii::app()->user->isAdministrator()){ ?> 
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Beranda', 'url'=>array('/site/index')),
				array('label'=>'Bahan Tayang', 'url'=>array('/bahanTayang/admin')),
				array('label'=>'Bidang', 'url'=>array('/bidang/admin')),
				array('label'=>'BMN', 'url'=>array('/bmn/admin')),
				array('label'=>'Karyasiswa', 'url'=>array('/karyasiswa/admin')),
				array('label'=>'Kegiatan', 'url'=>array('/kegiatan/admin')),
				array('label'=>'Kurmod', 'url'=>array('/kurmod/admin')),
				array('label'=>'Laporan Kegiatan', 'url'=>array('/laporanKgt/admin')),
				array('label'=>'Log Aktivitas', 'url'=>array('/logAktivitas/admin')),
				array('label'=>'Media', 'url'=>array('/media/admin')),
				array('label'=>'Naskah Dinas', 'url'=>array('/naskahDinas/admin')),
				array('label'=>'Pegawai', 'url'=>array('/pegawai/admin')),
				array('label'=>'Pelaksanaan Pelatihan', 'url'=>array('/pelaksanaanPlth/admin')),
				array('label'=>'Peserta Pelatihan', 'url'=>array('/pesertaPlthn/admin')),
				array('label'=>'Profil Pelatihan', 'url'=>array('/profilPelatihan/admin')),
				array('label'=>'Renstra', 'url'=>array('/renstra/admin')),
				array('label'=>'Renstra Dok', 'url'=>array('/renstraDok/admin')),
				array('label'=>'SOP', 'url'=>array('/sop/admin')),
				array('label'=>'User', 'url'=>array('/user/admin')),
				array('label'=>'Widyaiswara', 'url'=>array('/widyaiswara/admin')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		</div><!-- mainmenu -->
	<?php } ?>

	<?php if (Yii::app()->user->isAdminbidang()){ ?> 
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Beranda', 'url'=>array('/site/index')),
				array('label'=>'Bahan Tayang', 'url'=>array('/bahanTayang/admin')),
				array('label'=>'Bidang', 'url'=>array('/bidang/admin')),
				array('label'=>'BMN', 'url'=>array('/bmn/admin')),
				array('label'=>'Karyasiswa', 'url'=>array('/karyasiswa/admin')),
				array('label'=>'Kegiatan', 'url'=>array('/kegiatan/admin')),
				array('label'=>'Kurmod', 'url'=>array('/kurmod/admin')),
				array('label'=>'Log Aktivitas', 'url'=>array('/logAktivitas/admin')),
				array('label'=>'Media', 'url'=>array('/media/admin')),
				array('label'=>'Naskah Dinas', 'url'=>array('/naskahDinas/admin')),
				array('label'=>'Pegawai', 'url'=>array('/pegawai/admin')),
				array('label'=>'Renstra', 'url'=>array('/renstra/admin')),
				array('label'=>'Renstra Dok', 'url'=>array('/renstraDok/admin')),
				array('label'=>'SOP', 'url'=>array('/sop/admin')),
				array('label'=>'Widyaiswara', 'url'=>array('/widyaiswara/admin')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		</div><!-- mainmenu -->
	<?php } ?>

	<?php if (Yii::app()->user->isVisitor()){ ?> 
	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Beranda', 'url'=>array('/site/index')),
				array('label'=>'Bahan Tayang', 'url'=>array('/bahanTayang/admin')),
				array('label'=>'Naskah Dinas', 'url'=>array('/naskahDinas/admin')),
				array('label'=>'Pegawai', 'url'=>array('/pegawai/admin')),
				array('label'=>'Renstra', 'url'=>array('/renstra/admin')),
				array('label'=>'Renstra Dok', 'url'=>array('/renstraDok/admin')),
				array('label'=>'SOP', 'url'=>array('/sop/admin')),
				array('label'=>'Widyaiswara', 'url'=>array('/widyaiswara/admin')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
		</div><!-- mainmenu -->
	<?php } ?>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Kiprama Studio.<br/>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
