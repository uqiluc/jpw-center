<?php
/* @var $this LogAktivitasController */
/* @var $model LogAktivitas */
$this->menu=array(
	array('label'=>'Daftar Log Aktivitas', 'url'=>array('index')),
	array('label'=>'Kelola Log Aktivitas', 'url'=>array('admin')),
);
?>

<h1>Tambah Log Aktivitas</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>