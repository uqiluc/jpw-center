<?php
/* @var $this LogAktivitasController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Tambah Log Aktivitas', 'url'=>array('create')),
	array('label'=>'Kelola Log Aktivitas', 'url'=>array('admin')),
);
?>

<h1>Log Aktivitas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
