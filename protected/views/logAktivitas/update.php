<?php
/* @var $this LogAktivitasController */
/* @var $model LogAktivitas */

$this->menu=array(
	array('label'=>'Daftar Log Aktivitas', 'url'=>array('index')),
	array('label'=>'Tambah Log Aktivitas', 'url'=>array('create')),
	array('label'=>'Lihat Log Aktivitas', 'url'=>array('view', 'id'=>$model->log_id)),
	array('label'=>'Kelola Log Aktivitas', 'url'=>array('admin')),
);
?>

<h1>Perbaharui Log Aktivitas <?php echo $model->log_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>