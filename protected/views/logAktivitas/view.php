<?php
/* @var $this LogAktivitasController */
/* @var $model LogAktivitas */


$this->menu=array(
	array('label'=>'List Log Aktivitas', 'url'=>array('index')),
	array('label'=>'Tambah Log Aktivitas', 'url'=>array('create')),
	array('label'=>'Perbaharui Log Aktivitas', 'url'=>array('update', 'id'=>$model->log_id)),
	array('label'=>'Hapus Log Aktivitas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->log_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Log Aktivitas', 'url'=>array('admin')),
);
?>

<h1>View LogAktivitas #<?php echo $model->log_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'log_id',
		'created',
		'updated',
	),
)); ?>
