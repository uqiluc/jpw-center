<?php
	$bidang = Bidang::model()->findByPk($_GET['bidang']);
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'media-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->hiddenField($model,'bidang',array('value'=>$bidang->bd_id)); ?>
		<input type="text" class="form-control" value="<?php echo $bidang->nama_bidang;?>" readonly=true>
		<?php echo $form->error($model,'bidang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jns_file'); ?>
		<?php echo $form->textField($model,'jns_file',array('size'=>60,'maxlength'=>255,'class' => 'form-control','value'=>$_GET["type"],'readonly'=>true)); ?>
		<?php echo $form->error($model,'jns_file'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_file'); ?>
		<?php echo $form->textField($model,'nama_file',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_file'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tgl_dokumen'); ?>
		<?php echo $form->dateField($model,'tgl_dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'tgl_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'thumbnail'); ?>
		<?php echo $form->fileField($model,'thumbnail',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'thumbnail'); ?>
	</div>

	<div class="form-group">
		<?php 
			if ($_GET['type'] == 'video') {
				$type = "Video Url *";
			} else if ($_GET['type'] == 'ebook') {
				$type = "E-Book Url *";
			} else {
				$type = "Lainnya *";
			}
			echo $form->labelEx($model,$type); 
		?>
		<?php echo $form->textField($model,'dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'dokumen'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->