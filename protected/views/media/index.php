<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Media";
$this->breadcrumbs=array(
	'Media'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/Bidang/jalanjembatan.png" width="100%">
            	</div>
            	<b>Bidang Jalan & Jembatan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_jalanjembatan" >Pilih</button>
            	<!-- <a href="<?php //echo $baseUrl;?>/media/jalanjembatan" class="btn btn-primary">Pilih</a> -->
			</div>
		  </div>
		</div>
        
        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/Bidang/Perumahan.png" width="100%">
            	</div>
            	<b>Bidang Perumahan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_perumahan" >Pilih</button>
            	<!-- <a href="<?php //echo $baseUrl;?>/media/perumahan" class="btn btn-primary">Pilih</a> -->
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/Bidang/piw.png" width="100%">
            	</div>
            	<b>Bidang PIW</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal_piw" >Pilih</button>
            	<!-- <a href="<?php //echo $baseUrl;?>/media/piw" class="btn btn-primary">Pilih</a> -->
			</div>
		  </div>
		</div>
	  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_piw" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Pilih Tahun Upload - Media PIW</h5>
		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
	<div class="row">
		<?php foreach (Media::model()->findAll('bidang = 8 group by year(created)') AS $piw) { ?>
			<div class="col-4">
			<a href="<?php echo $baseUrl;?>/media/piw?Media[created]=<?php echo substr($piw['created'],0,4);?>" class="btn btn-primary">Diupload <?php echo substr($piw['created'],0,4);?>
			</a>
			</div>
		<?php } ?>
			</div>
		</div>
	</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal_jalanjembatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Pilih Tahun Upload - Media Jalan Jembatan</h5>
		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
	<div class="row">
		<?php foreach (Media::model()->findAll('bidang = 7 group by year(created)') AS $piw) { ?>
			<div class="col-4">
            	<a href="<?php echo $baseUrl;?>/media/jalanjembatan?Media[created]=<?php echo substr($piw['created'],0,4);?>" class="btn btn-primary">Diupload <?php echo substr($piw['created'],0,4);?>
			</a>
			</div>
		<?php } ?>
			</div>
		</div>
	</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_perumahan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Pilih Tahun Upload - Media Perumahan</h5>
		<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		</div>
		<div class="modal-body">
	<div class="row">
		<?php foreach (Media::model()->findAll('bidang = 6 group by year(created)') AS $piw) { ?>
			<div class="col-4">
            	<a href="<?php echo $baseUrl;?>/media/jalanjembatan?Media[created]=<?php echo substr($piw['created'],0,4);?>" class="btn btn-primary">Diupload <?php echo substr($piw['created'],0,4);?>
			</a>
			</div>
		<?php } ?>
			</div>
		</div>
	</div>
	</div>
</div>