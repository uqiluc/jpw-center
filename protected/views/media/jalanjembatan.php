<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Media";
$this->breadcrumbs=array(
	'Media'=>array('index'),
	'Jalan'
);

?>
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            	<div class="pull-right">
							<?php 
							echo CHtml::link('<i class="fa fa-video"></i> Tambah Video',
							array('create?type=video&bidang=6'),
							array('class' => 'btn btn-default','title'=>'Tambah Video'));
							?>
							<?php 
							echo CHtml::link('<i class="fa fa-book"></i> Tambah E-Book',
							array('create?type=ebook&bidang=6'),
							array('class' => 'btn btn-default','title'=>'Tambah E-Book'));
							?>							
							<?php 
							echo CHtml::link('<i class="fa fa-th"></i> Tambah Lainnya',
							array('create?type=other&bidang=6'),
							array('class' => 'btn btn-default','title'=>'Tambah Lainnya'));
							?>							
	            </div>
	            <h6>Data Media</h6>
	        </div>
	        </div>
	      </div>
	    </div>
</div>

<div class="container-fluid py-2">
    <div class="row">
    <div class="col-12">
        <div class="card mb-4">
        <div class="card-body px-0 pt-0 pb-2">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'media-grid',
			'dataProvider'=>$model->bidang(6),
			'filter'=>$model,
			'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
			'itemsCssClass'=>'table table-flush dataTable-table',
			'columns'=>array(
				array(
					'header'=>'#',
					'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
					'htmlOptions'=>array('width'=>'10px','style'=>'text-align:center')
				),
				array(
					'name'=>'nama_file',
					'type'=>'raw',
					'value'=>'$data->nama_file',
					'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
					'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
				),
				array(
					'name'=>'jns_file',
					'type'=>'raw',
					'value'=>'$data->jns_file',
					'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
					'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
				),
				array(
					'name'=>'bidang',
					'type'=>'raw',
					'value'=>'$data->Bidang->nama_bidang',
					'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
					'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
				),
				array(
					'header'=>'Aksi',
					'class'=>'CButtonColumn',
					'template'=>'{view}{delete}',				
					'htmlOptions'=>array('width'=>'150px', 
					'style' => 'text-align: center;'),
				),
	
			),
		)); ?>
		</div>
		</div>
	</div>
</div>