<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Media";
$this->breadcrumbs=array(
	'Media'=>array('index'),
	'Jalan'
);

?>
<script src="//static.fliphtml5.com/web/js/plugin/LightBox/js/fliphtml5-light-box-api-min.js"></script>
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            	<div class="pull-right">
							<?php 
							echo CHtml::link('<i class="fa fa-video"></i> Tambah Video',
							array('create?type=video&bidang=7'),
							array('class' => 'btn btn-default','title'=>'Tambah Video'));
							?>
							<?php 
							echo CHtml::link('<i class="fa fa-book"></i> Tambah E-Book',
							array('create?type=ebook&bidang=7'),
							array('class' => 'btn btn-default','title'=>'Tambah E-Book'));
							?>							
							<?php 
							echo CHtml::link('<i class="fa fa-th"></i> Tambah Lainnya',
							array('create?type=other&bidang=7'),
							array('class' => 'btn btn-default','title'=>'Tambah Lainnya'));
							?>							
	            </div>
	            <h6>Data Media</h6>
	        </div>
	        </div>
	      </div>
	    </div>
</div>

<div class="container-fluid py-2">
    <div class="row">
    <?php foreach (Media::model()->findAll('bidang=6 AND jns_file="Video"') AS $data) { ;?>
    <div class="col-3">
        <div class="card mb-4">
         <div class="card-body px-0 pt-0 pb-2">
          	<div style="padding: 15px;">
          		<a href="<?php echo $baseUrl;?>/media/piw" class="link-media">
	            		<img src="<?php echo $baseUrl; ?>/img/play.png" width="60%" class="link-play">
	            		<img src="<?php echo $baseUrl; ?>/Dokumen/Media/<?php echo $data->thumbnail;?>" width="100%" class="link-thumb">
  						</a>
          	</div>
          	<div style="padding: 0px 15px;">
	          	<b><?php echo $data->nama_file;?></b>
	          	<i class="fa fa-video pull-right"></i>
	          </div>
				</div>
		  </div>
		</div>
		<?php } ?>
	  <hr>
    <div class="col-12">
    <?php foreach (Media::model()->findAll('bidang=6 AND jns_file="ebook"') AS $data) { ;?>
    <div class="col-3">
        <div class="card mb-4">
         <div class="card-body px-0 pt-0 pb-2">
          	<div style="padding: 15px;">
            		<img src="<?php echo $baseUrl; ?>/Dokumen/Media/<?php echo $data->thumbnail;?>" width="100%" class="link-thumb" data-rel="fh5-light-box-demo" data-href="<?php echo $data->dokumen;?>" data-width="700" data-height="400" data-title="<?php echo $data->nama_file;?>">
          	</div>
          	<div style="padding: 0px 15px;">
	          	<b><?php echo $data->nama_file;?></b>
	          	<i class="fa fa-book pull-right"></i>
	          </div>
				</div>
		  </div>
		</div>
		<?php } ?>
	  </div>

	  </div>

</div>