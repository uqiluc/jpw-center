<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Media";
$this->breadcrumbs=array(
	'Media'=>array('index'),
	'Jembatan dan Jalan'
);

?>
<script src="//static.fliphtml5.com/web/js/plugin/LightBox/js/fliphtml5-light-box-api-min.js"></script>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_file;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->jns_file." - ".$model->Bidang->nama_bidang;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
		<?php 
		// echo CHtml::link('<i class="fa fa-edit"></i> Edit',
		// array('update','id'=>$model->psrt_id),
		// array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		// echo CHtml::link('<i class="fa fa-th"></i> Data Peserta',
		// array('admin'),
		// array('class' => 'btn btn-default','title'=>'Kelola Data'));
		?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>

<div class="container-fluid py-1">
<div class="row">
    <div class="col-12 col-xl-6">
      <div class="card">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Peserta</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
        	<?php $this->widget('zii.widgets.CDetailView', array(
				'data'=>$model,
			    'htmlOptions'=>array('class'=>"table table-responsive table-flush dataTable-table"),			
				'attributes'=>array(
					'tgl_dokumen',
					'dokumen',
					'created',
					'updated',
				),
			)); ?>
        </div>
      </div>
    </div>
	<div class="col-12 col-xl-6">
      <div class="card">
        <div class="card-body p-3">
		<?php if ($model->jns_file == "video") { ?>
			<iframe width="100%" height="450px"
				src="<?php echo str_replace("watch?v=","embed/",$model->dokumen);?>">
			</iframe>
		<?php } else if ($model->jns_file == "ebook") { ?>
			<img src="<?php echo $baseUrl; ?>/Dokumen/Media/<?php echo $model->thumbnail;?>" width="100%" class="link-thumb" data-rel="fh5-light-box-demo" data-href="<?php echo $model->dokumen;?>" data-width="700" data-height="400" data-title="<?php echo $model->nama_file;?>">
		<?php } else { ?>
			
		<?php } ?>
		</div>
	  </div>
	</div>
</div>
</div>