<?php
/* @var $this NaskahDinasController */
/* @var $model NaskahDinas */
/* @var $form CActiveForm */
$type = $_GET['type'];

if ($type == 'administrasi') {
	$list = array('Dokumen LSP'=>'Dokumen LSP','Persuratan'=>'Persuratan','Nota Dinas'=>'Nota Dinas');
} else if ($type == 'skema') {
	$list = array('Skema Sertifikasi Dokumen'=>'Skema Sertifikasi Dokumen','Skema Sertifikasi SKKNI'=>'Skema Sertifikasi SKKNI','Urensi Skema'=>'Urensi Skema','Surat Verifikasi Skema'=>'Surat Verifikasi Skema');
} else if ($type == 'sertifikasi') {
	$list = array(
		'Pelatihan Asseor'=>array(
			'Asseor Persuratan'=>'Persuratan',
			'Asseor Jadwal'=>'Jadwal',
			'Form Assesmen'=>'Form Assesmen',
			'Form Pelatihan Asseor'=>'Form Pelatihan Asseor',
			'Asseor Nota Dinas / Laporan'=>'Nota Dinas / Laporan',
		),
		'Uji Kompetensi'=>array(
			'Kompetensi Persuratan'=>'Persuratan',
			'Kompetensi Jadwal'=>'Jadwal',
			'Dokumen Persyaratan'=>'Dokumen Persyaratan',
			'Dokumen Uji Kompetensi'=>'Dokumen Uji Kompetensi',
			'Kompetensi Nota Dinas / Laporan'=>'Nota Dinas / Laporan',
		));
} else if ($type == 'mutu') {
	$list = array('Surat'=>'Surat','Laporan Evaluasi'=>'Laporan Evaluasi','SOP'=>'SOP');
} else if ($type == 'regulasi') {
	$list = array('Regulasi'=>'Regulasi');
} else if ($type == 'dokumentasi') {
	$list = array('Dokumentasi'=>'Dokumentasi');
} else if ($type == 'pendidikan') {
	$list = array('Monitoring Pendidikan'=>'Monitoring Pendidikan');
} else if ($type == 'dokumen') {
	$list = array('PKS'=>'PKS','Kontrak'=>'Kontrak','MOU'=>'MOU','Lain-lain'=>'Lain-lain');
} else if ($type == 'laporan') {
	$list = array('Laporan Kegiatan'=>'Laporan Kegiatan');
} else if ($type == 'renstra') {
	$list = array('Renstra'=>'Renstra');
} else if ($type == 'sop') {
	$list = array('SOP'=>'SOP');
} else if ($type == 'bmn') {
	$list = array('BMN'=>'BMN');
}
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'naskah-dinas-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_file'); ?>
		<?php echo $form->textField($model,'nama_file',array('size'=>60,'maxlength'=>255,'class' => 'form-control','placeholder'=>'Nama file')); ?>
		<?php echo $form->error($model,'nama_file'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jns_dokumen'); ?>
		<?php echo $form->dropDownList ($model, 'jns_dokumen',
			$list,
			array("empty"=>"-- Pilih Jenis Dokumen --",
				'style'=>'width:100;','class' => 'form-control')
			);
			?>
		<?php echo $form->error($model,'jns_dokumen'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",
				'style'=>'width:100;','class' => 'form-control')
			);
			?>
		<?php echo $form->error($model,'bidang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tgl_dokumen'); ?>
		<?php echo $form->dateField($model,'tgl_dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'tgl_dokumen'); ?>
	</div>
	<div class="form-group">
		<?php echo $form->labelEx($model,'dokumen'); ?>
		<?php echo $form->fileField($model,'dokumen',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'dokumen'); ?>
	</div>
	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->