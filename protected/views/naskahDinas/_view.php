<?php
/* @var $this NaskahDinasController */
/* @var $data NaskahDinas */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('nsk_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->nsk_id), array('view', 'id'=>$data->nsk_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_file')); ?>:</b>
	<?php echo CHtml::encode($data->nama_file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jns_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->jns_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bidang')); ?>:</b>
	<?php echo CHtml::encode($data->bidang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>