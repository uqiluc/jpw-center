<?php
/* @var $this NaskahDinasController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Tambah Naskah Dinas', 'url'=>array('create')),
	array('label'=>'Kelola Naskah Dinas', 'url'=>array('admin')),
);
?>

<h1>Naskah Dinas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
