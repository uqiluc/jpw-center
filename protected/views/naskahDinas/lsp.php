<?php
    if ($_GET['type'] == 'pendidikan' || $_GET['type'] == 'dokumen'){
        $ctg = "Kerjasama";
    } else if ($_GET['type'] == 'renstra' || $_GET['type'] == 'sop' || $_GET['type'] == 'perundangan' || $_GET['type'] == 'bmn' || $_GET['type'] == 'laporan'){
        $ctg = "Dokumen";
    } else { 
        $ctg = "LSP";
    }

    $this->pageTitle=$ctg;
    $this->breadcrumbs=array(
        $ctg=>array('index'),
        'Create'
    );
?>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
                <div class="pull-right">
                    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>
                </div>
                <h6>Create <?php echo $ctg;?></h6>
            </div>
            <div class="card-body">
            	<?php echo $this->renderPartial('_formLsp', array('model'=>$model)); ?>
            </div>
          </div>
        </div>
    </div>	
</div>  