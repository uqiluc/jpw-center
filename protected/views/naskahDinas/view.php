<div class="row">
<div class="col-lg-12">
	<?php 
	echo CHtml::link('<i class="fa fa-plus"></i> Tambah Naskah Dinas',
	array('create'),
	array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-edit"></i> Edit',
	array('update','id'=>$model->nsk_id),
	array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-photo"></i> Dokumen',
	array('dokumen','id'=>$model->nsk_id),
	array('class' => 'btn btn-primary','title'=>'Edit Dokumen'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-th"></i> Kelola',
	array('admin'),
	array('class' => 'btn btn-primary','title'=>'Kelola Data'));
	?>	
	<div class="pull-right">
    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>		
	</div>
	<hr>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
    'htmlOptions'=>array('class'=>"table"),		
	'attributes'=>array(
		'nama_file',
		'jns_dokumen',
		'tgl_dokumen',
		array(
			'name'=>'bidang',
			'type'=>'raw',
			'value'=>$model->Bidang->nama_bidang
		),
	),
)); ?>
</div>
</div>