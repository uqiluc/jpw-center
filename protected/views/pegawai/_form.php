<?php
/* @var $this PegawaiController */
/* @var $model Pegawai */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pegawai-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",
				'style'=>'width:100;','class' => 'form-control','required'=>true)
			);
			?>
		<?php echo $form->error($model,'bidang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenisPegawai'); ?>
		<?php echo $form->dropDownList ($model, 'jenisPegawai',
			array('PNS'=>'PNS','PH'=>'PH','KI'=>'KI'),
			array("empty"=>"-- Pilih Jenis Pegawai --",
				'style'=>'width:100;','class' => 'form-control','required'=>true)
			);
			?>
		<?php echo $form->error($model,'bidang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_pegawai'); ?>
		<?php echo $form->textField($model,'nama_pegawai',array('size'=>60,'maxlength'=>255,'class' => 'form-control','required'=>true)); ?>
		<?php echo $form->error($model,'nama_pegawai'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nip'); ?>
		<?php echo $form->numberField($model,'nip',array('size'=>30,'maxlength'=>30,'class' => 'form-control','required'=>true)); ?>
		<?php echo $form->error($model,'nip'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->emailField($model,'email',array('size'=>50,'maxlength'=>255,'class' => 'form-control','required'=>true)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pangkat'); ?>
		<?php echo $form->textField($model,'pangkat',array('size'=>60,'maxlength'=>255,'class' => 'form-control','required'=>true)); ?>
		<?php echo $form->error($model,'pangkat'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'golongan'); ?>
		<?php echo $form->textField($model,'golongan',array('size'=>60,'maxlength'=>255,'class' => 'form-control','required'=>true)); ?>
		<?php echo $form->error($model,'golongan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jabatan'); ?>
		<?php echo $form->textField($model,'jabatan',array('size'=>60,'maxlength'=>255,'class' => 'form-control','required'=>true)); ?>
		<?php echo $form->error($model,'jabatan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kelas_jabatan'); ?>
		<?php echo $form->textField($model,'kelas_jabatan',array('size'=>60,'maxlength'=>255,'class' => 'form-control','required'=>true)); ?>
		<?php echo $form->error($model,'kelas_jabatan'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->