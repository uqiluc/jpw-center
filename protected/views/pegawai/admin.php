<?php
/* @var $this PegawaiController */
/* @var $model Pegawai */
$this->pageTitle="Pegawai";
$this->breadcrumbs=array(
	'Pegawai'=>array('admin')
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pegawai-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

 <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
            	<div class="pull-right">
					<?php 
					echo CHtml::link('<i class="fa fa-plus"></i> Create',
					array('create'),
					array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
					array('excel'),
					array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
					?>
	            </div>
	            <h6>Data Pegawai</h6>
	        </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
	              <div class="dataTable-container">
	              	<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'pegawai-grid',
						'dataProvider'=>$model->search(),
						'filter'=>$model,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'name'=>'nama_pegawai','type'=>'raw',
							  	'value'=>'Pegawai::model()->getPegawai($data->pg_id)',
				  				'htmlOptions'=>array('width'=>'300px'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'nip','type'=>'raw',
								'value'=>'$data->nip',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'golongan','type'=>'raw',
								'value'=>'$data->golongan',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0 text-center'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'bidang','type'=>'raw','value'=>'$data->bidang',
								'filter'=>CHtml::listData(Pegawai::model()->findAll(),'bidang', 'bidang'),
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'header'=>'Aksi',
								'class'=>'CButtonColumn',
								'template'=>'{update} {delete}',
								'htmlOptions'=>array('width'=>'100px', 
								'style' => 'text-align: center;'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
						),
					)); ?>
	              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
</div>