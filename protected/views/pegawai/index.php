<?php
/* @var $this PegawaiController */
/* @var $dataProvider CActiveDataProvider */


$this->menu=array(
	array('label'=>'Tambah Pegawai', 'url'=>array('create')),
	array('label'=>'Kelola Pegawai', 'url'=>array('admin')),
);
?>

<h1>Pegawai</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
