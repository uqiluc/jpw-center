<?php
$this->pageTitle="Pegawai";
$this->breadcrumbs=array(
    'Pegawai'=>array('admin'),
    'View'
);

$Url = Yii::app()->baseUrl; 
$theme = Yii::app()->theme->baseUrl; 

if ($model->foto == ''){
	$imgprofil = $Url."/Dokumen/noImage.jpg";
}else{
	$imgprofil = $Url."/Dokumen/Foto/".$model->foto;
}

?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-8">
  <div class="card card-body overflow-hidden">
    <div class="card-header pb-0 pt-0">
        <div class="pull-right">
    		<?php 
    		echo CHtml::link('<i class="fa fa-image"></i> Edit Foto',
    		array('updateG','id'=>$model->pg_id),
    		array('class' => 'btn btn-default','title'=>'Edit Data'));
    		?>
    		<?php 
    		echo CHtml::link('<i class="fa fa-edit"></i> Edit',
    		array('update','id'=>$model->pg_id),
    		array('class' => 'btn btn-default','title'=>'Edit Data'));
    		?>
    		<?php 
    		echo CHtml::link('<i class="fa fa-th"></i> Data Pegawai',
    		array('admin'),
    		array('class' => 'btn btn-default','title'=>'Data Pegawai'));
    		?>	
        </div>
    </div>  	
    <div class="row gx-4">
      <div class="col-auto">
        <div class="avatar avatar-xl position-relative">
          <img src="<?php echo $imgprofil;?>" alt="profile_image" class="w-100 border-radius-lg shadow-sm">
        </div>
      </div>
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_pegawai;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->jabatan;?>
          </p>
        </div>
      </div>
    </div>
  </div>

  <div class="card mt-2">
    <div class="card-header pb-0 p-3">
      <div class="pull-right">
      <?php 
        echo CHtml::link('<i class="fa fa-plus"></i> Add Sosmed',
        array('sosmed/create','id'=>$model->pg_id),
        array('class' => 'btn btn-default','title'=>'Add Sosmed'));
      ?>
      </div>      
      <h6 class="mb-0">Sosial Media</h6>
    </div>
    <div class="card-body p-3">
      <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'sosmed-grid',
            'dataProvider'=>$Sosmed->search($model->pg_id),
            'filter'=>$Sosmed,
            'summaryText'=>'',
            'itemsCssClass'=>'table table-flush dataTable-table',
            'columns'=>array(
              array(
                'name'=>'label','type'=>'raw','value'=>'$data->label',
                'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
                  'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
              ),
              array(
                'name'=>'url','type'=>'raw','value'=>'$data->url',
                'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
                  'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
              ),
              array(
                'header'=>'Aksi',
                'class'=>'CButtonColumn',
                'template'=>'{update} {delete}',
                'htmlOptions'=>array('width'=>'100px', 
                'style' => 'text-align: center;'),
                'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
              ),
            ),
        )); ?>
    </div>
  </div>  
  
  </div>

  <div class="col-12 col-xl-4">
  <div class="card">
    <div class="card-header pb-0 p-3">
      <h6 class="mb-0">Profil Pegawai</h6>
    </div>
    <div class="card-body p-3">
    	<ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Tanggal Lahir:</strong> &nbsp; <?php echo Pegawai::model()->date(substr($model->nip,0,4).'-'.substr($model->nip,5,2).'-'.substr($model->nip,6,2));?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">NIP:</strong> &nbsp; <?php echo $model->nip;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Pangkat:</strong> &nbsp; <?php echo $model->pangkat;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Golongan:</strong> &nbsp; <?php echo $model->golongan;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Bagian:</strong> &nbsp; <?php echo $model->Bidang->nama_bidang;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Kelas Jabatan:</strong> &nbsp; <?php echo $model->kelas_jabatan;?></li>
        </ul>
    </div>
  </div>
  </div>
</div>
</div>