<?php
/* @var $this PelaksanaanPlthController */
/* @var $model PelaksanaanPlth */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pelaksanaan-plth-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>


	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
		CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
		array("empty"=>"-- Pilih Bidang --",
			   'style'=>'width:100;','class' => 'form-control',
			   'ajax' => array(
               'type'=>'POST',
               'url'=>CController::createUrl('PelaksanaanPlth/selectProfile'),
               'update'=>'#'.CHtml::activeId($model,'pel_id'),
               'beforeSend'=>'function() {
                 $("#PelaksanaanPlth_pel_id").find("option").remove();
                 }',
             )
           )
        ); ?>		
		<?php echo $form->error($model,'bidang'); ?>
	</div>


	<div class="form-group">
		<?php echo $form->labelEx($model,'pel_id'); ?>
		<?php echo $form->dropDownList ($model, 'pel_id',
			CHtml::listData(ProfilPelatihan::model()->findAll('prof_id != 0 order by nama_pelatihan ASC'),'prof_id', 'nama_pelatihan'),
			array("empty"=>"-- Pilih Pelatihan --", 'onchange'=>'updatePelatihan(this)',
				'style'=>'width:100;','class' => 'form-control')
			);
			?>
		<?php echo $form->error($model,'pel_id'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nm_pelatihan'); ?>
		<?php echo $form->textField($model,'nm_pelatihan',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nm_pelatihan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tahun'); ?>
		<?php echo $form->numberField($model,'tahun',array('size'=>60,'maxlength'=>255,'class' => 'form-control','placeholder'=>'2021')); ?>
		<?php echo $form->error($model,'tahun'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'Tanggal Pelatihan'); ?> *
			<div class="input-group">
				<?php echo $form->dateField($model,'tgl_plaksana',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
				<?php echo $form->error($model,'tgl_plaksana'); ?>
		        <span class="input-group-addon" id="basic-addon1">s/d</span>
				<?php echo $form->dateField($model,'tgl_akhir',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
				<?php echo $form->error($model,'tgl_akhir'); ?>
			</div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'lokasi_plthn'); ?>
		<?php echo $form->textField($model,'lokasi_plthn',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'lokasi_plthn'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ktg_plthn'); ?>
		<?php echo $form->textField($model,'ktg_plthn',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'ktg_plthn'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jns_rncana'); ?>
		<?php echo $form->dropDownList($model,'jns_rncana', 
								array('Target'=>'Target',
									'Tambahan'=>'Tambahan',)
								,array("empty"=>"-- Pilih Jenis Rencana --",
								'style'=>'width:100;','class' => 'form-control')
							);
		?>
		<?php echo $form->error($model,'jns_rncana'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
	function updatePelatihan(sel){
		document.getElementById('PelaksanaanPlth_nm_pelatihan').value = sel.options[sel.selectedIndex].text;
	}
</script>