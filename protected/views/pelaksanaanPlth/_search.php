<?php
/* @var $this PelaksanaanPlthController */
/* @var $model PelaksanaanPlth */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'pplth_id'); ?>
		<?php echo $form->textField($model,'pplth_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nm_pelatihan'); ?>
		<?php echo $form->textField($model,'nm_pelatihan',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tahun'); ?>
		<?php echo $form->textField($model,'tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_plaksana'); ?>
		<?php echo $form->textField($model,'tgl_plaksana'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'lokasi_plthn'); ?>
		<?php echo $form->textField($model,'lokasi_plthn',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bidang'); ?>
		<?php echo $form->textField($model,'bidang'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ktg_plthn'); ?>
		<?php echo $form->textField($model,'ktg_plthn',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jml_peserta'); ?>
		<?php echo $form->textField($model,'jml_peserta',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jns_rncana'); ?>
		<?php echo $form->textField($model,'jns_rncana',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'brt_acara'); ?>
		<?php echo $form->textField($model,'brt_acara',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dtl_psrta'); ?>
		<?php echo $form->textField($model,'dtl_psrta',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sts_platihan'); ?>
		<?php echo $form->textField($model,'sts_platihan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->