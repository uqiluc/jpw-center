<?php
/* @var $this PelaksanaanPlthController */
/* @var $data PelaksanaanPlth */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pplth_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pplth_id), array('view', 'id'=>$data->pplth_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nm_pelatihan')); ?>:</b>
	<?php echo CHtml::encode($data->nm_pelatihan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tgl_plaksana')); ?>:</b>
	<?php echo CHtml::encode($data->tgl_plaksana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lokasi_plthn')); ?>:</b>
	<?php echo CHtml::encode($data->lokasi_plthn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bidang')); ?>:</b>
	<?php echo CHtml::encode($data->bidang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ktg_plthn')); ?>:</b>
	<?php echo CHtml::encode($data->ktg_plthn); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('jml_peserta')); ?>:</b>
	<?php echo CHtml::encode($data->jml_peserta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jns_rncana')); ?>:</b>
	<?php echo CHtml::encode($data->jns_rncana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brt_acara')); ?>:</b>
	<?php echo CHtml::encode($data->brt_acara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dtl_psrta')); ?>:</b>
	<?php echo CHtml::encode($data->dtl_psrta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sts_platihan')); ?>:</b>
	<?php echo CHtml::encode($data->sts_platihan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>