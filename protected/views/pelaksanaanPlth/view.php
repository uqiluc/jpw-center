<?php
/* @var $this KegiatanController */
/* @var $model Kegiatan */
?>

<div class="row">
<div class="col-lg-12">
<div class="pull-right">
<?php if (Yii::app()->user->record->level == 1){
echo CHtml::link('<i class="fa fa-edit"></i> Edit',
array('update','id'=>$model->pplth_id),
array('class' => 'label label-warning','title'=>'Edit'));
?>
<?php 
echo CHtml::link('<i class="fa fa-times"></i> Delete',
array('hapus','id'=>$model->pplth_id),
array('class' => 'label label-danger','title'=>'Hapus Data'));
} else {}
?>
</div>	
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
    'htmlOptions'=>array('class'=>"table"),	
	'attributes'=>array(
		array('name'=>'nm_pelatihan','type'=>'raw',
		  	  'value'=>CHtml::link($model->nm_pelatihan." <i class='fa fa-eye'></i>",array("pelaksanaanPlth/viewDetail","id"=>$model->pplth_id)),
		),
		array(
			'name'=>'tgl_plaksana',
			'type'=>'raw',
			'value'=>Kegiatan::model()->hari_ini($model->tgl_plaksana)
		),
		array(
			'name'=>'tgl_akhir',
			'type'=>'raw',
			'value'=>Kegiatan::model()->hari_ini($model->tgl_akhir)
		),		
		'ktg_plthn',
		'lokasi_plthn',
	),
)); ?>
</div>
</div>