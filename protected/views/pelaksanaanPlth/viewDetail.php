<?php
$this->pageTitle="Jadwal Pelatihan";
$this->breadcrumbs=array(
    'Pelaksanaan Pelatihan'=>array('admin'),
    'View'
);
?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1" style="font-size:16px">
            <?php echo CHtml::link($model->nm_pelatihan." (".$model->tahun.") <i class='fa fa-exclamation-circle'></i>",array("profilPelatihan/view","id"=>$model->pel_id));?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo Kegiatan::model()->hari_ini($model->tgl_plaksana)." s/d ".Kegiatan::model()->hari_ini($model->tgl_akhir);?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
				<?php if (Yii::app()->user->record->level == 1){
					echo CHtml::link('<i class="fa fa-edit"></i> Edit',
					array('update','id'=>$model->pplth_id),
					array('class' => 'btn btn-default','title'=>'Edit data'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-plus"></i> Tambah Peserta',
					array('pesertaPlthn/create','prof_id'=>$model->pplth_id),
					array('class' => 'btn btn-default','title'=>'Tambah Data Baru'));
				} ?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>

<div class="container-fluid py-1">
<div class="row">
    <div class="col-12 col-xl-6">
      <div class="card h-100">
        <div class="card-body p-3">
			    <ul class="list-group">
				    <li class="list-group-item border-0 ps-0 pt-0 text-sm">
				    	<strong class="text-dark">Bidang</strong><br>
				    	<?php echo $model->Bidang->nama_bidang;?>
				    </li>
				    <li class="list-group-item border-0 ps-0 pt-0 text-sm">
				    	<strong class="text-dark">Lokasi Pelatihan</strong><br>
				    	<?php echo $model->lokasi_plthn;?>
				    </li>	    
				    <li class="list-group-item border-0 ps-0 pt-0 text-sm">
				    	<strong class="text-dark">Keterangan Pelatihan</strong><br>
				    	<?php echo $model->ktg_plthn;?>
				    </li>	    
			  	</ul>
			  </div>
			</div>
  	</div>
    <div class="col-12 col-xl-6">
      <div class="card h-100">
        <div class="card-body p-3">
			    <ul class="list-group">
				    <li class="list-group-item border-0 ps-0 pt-0 text-sm">
				    	<strong class="text-dark">Jenis Rencana</strong><br>
				    	<?php echo $model->jns_rncana;?>
				    </li>
				    <li class="list-group-item border-0 ps-0 pt-0 text-sm">
				    	<strong class="text-dark">Berita Acara</strong><br>
				    	<?php echo $model->brt_acara == '' ? '-' : $model->brt_acara;?>
				    </li>
				    <li class="list-group-item border-0 ps-0 pt-0 text-sm">
				    	<strong class="text-dark">Status Pelatihan</strong><br>
				    	<?php echo PelaksanaanPlth::model()->getStatus($model->sts_platihan);?>
				    </li>	    
			  	</ul>
  			</div>  
  		</div>  
  	</div>
</div>
</div>

<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-12">
      <div class="card h-100">
        <div class="card-body p-3">
				<center><h5><b>Daftar Peserta Pelatihan</b></h5></center>
					<?php $ttlpesrt = Yii::app()->db->createCommand("SELECT COUNT(psrt_id) FROM peserta_plthn WHERE pel_id = $model->pplth_id")->queryScalar(); ?>
					<b>Total Peserta :</b> <?php echo $ttlpesrt;?> Orang
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'peserta-plthn-grid',
						'dataProvider'=>$Peserta->getDetail($model->pplth_id),
						'filter'=>$Peserta,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'name'=>'nama_peserta','type'=>'raw',
							  'value'=>'CHtml::link($data->nama_peserta,array("pesertaPlthn/view","id"=>$data->psrt_id))',
  				  		'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),			
							array(
								'name'=>'nip','type'=>'raw',
							  'value'=>'$data->nip',
  				  		'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),			
							array(
								'name'=>'nama_instansi','type'=>'raw',
							  'value'=>'substr($data->nama_instansi,0,25)."..."',
  				  		'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array('name'=>'nilai_akhir','value'=>'$data->nilai_akhir','htmlOptions'=>array('width'=>'10px'),'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')),
							array(
								'header'=>'Predikat',
								'type'=>'raw',
								'value'=>'PelaksanaanPlth::model()->prd($data->nilai_akhir)',
  				  		'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')								
							)			
						),
					)); ?>
				<br>
				</div>
			</div>
		</div>
</div>
</div>

<div id="editStatus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
			<h3 id="srv">Edit Status Pelatihan</h3>
      </div>
      <div class="modal-body">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'PelaksanaanPlth-form',
			'htmlOptions' => array('enctype' => 'multipart/form-data','autocomplete'=>'off'),		
			'enableAjaxValidation'=>false,
		)); ?>

		<div class="form-groupp">
			<?php echo $form->labelEx($modelPlth,'sts_platihan'); ?>
			<?php echo $form->dropDownList($model,'sts_platihan', array(
				'0'=>'Sedang Berlangsung','1'=>'Terlaksana','2'=>'Dibatalkan','3'=>'Ditunda'),
				array('class' => 'form-control'));?>
			<?php echo $form->error($modelPlth,'sts_platihan'); ?>
		</div>

      </div>
      <div class="modal-footer">
		<?php echo CHtml::submitButton($modelPlth->isNewRecord ? 'Simpan' : 'Simpan',array('class' => 'mch-sr-btn-ok')); ?>
		<?php $this->endWidget(); ?>
        <button class="mch-sr-btn-btl" data-dismiss="modal">Kembali</button>
      </div>
    </div>

  </div>
</div>