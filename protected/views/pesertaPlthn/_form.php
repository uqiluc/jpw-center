<?php
/* @var $this PesertaPlthnController */
/* @var $model PesertaPlthn */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'peserta-plthn-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>


<div style="display: block;" id="step1">
	<div class="form-group">
		<?php echo $form->labelEx($model,'no_urut_bidang'); ?>
		<?php echo $form->textField($model,'no_urut_bidang',array('size'=>30,'maxlength'=>30,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'no_urut_bidang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",'style'=>'width:100;','class'=>'form-control', 'required'=>true)
			);
			?>
		<?php echo $form->error($model,'bidang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kluster'); ?>
		<?php echo $form->textField($model,'kluster',array('size'=>30,'maxlength'=>30,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'kluster'); ?>
	</div>
	<div class="form-group">
		<button type="button" class="btn btn-primary" onclick="openstep2()">Next</button>
	</div>
</div>
<div style="display: none;" id="step2">
	<div class="form-group">
		<?php echo $form->labelEx($model,'nomor'); ?>
		<?php echo $form->textField($model,'nomor',array('size'=>5,'maxlength'=>10, 'onkeyup'=>'validAngka(this)','class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'nomor'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nip'); ?>
		<?php echo $form->textField($model,'nip',array('size'=>40,'maxlength'=>30, 'onkeyup'=>'validAngka(this)','class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'nip'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_peserta'); ?>
		<?php echo $form->textField($model,'nama_peserta',array('size'=>60,'maxlength'=>255,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'nama_peserta'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tempat_lahir'); ?>
		<?php echo $form->textField($model,'tempat_lahir',array('size'=>30,'maxlength'=>30,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'tempat_lahir'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tanggal_lahir'); ?>
		<?php echo $form->dateField($model,'tanggal_lahir',array('size'=>30,'maxlength'=>30,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'tanggal_lahir'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'agama'); ?>
		<?php echo $form->dropDownList($model,'agama', 
								array('Islam'=>'Islam',
									'Kristen'=>'Kristen',
									'Khatolik'=>'Khatolik',
									'Protestan'=>'Protestan',
									'Hindu'=>'Hindu',
									'Budha'=>'Budha',
									'Kong Hu Chu'=>'Kong Hu Chu',)
								,array("empty"=>"-- Pilih Agama --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>	
		<?php echo $form->error($model,'agama'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenis_kelamin'); ?>
		<?php echo $form->dropDownList($model,'jenis_kelamin', 
								array('Pria'=>'Pria',		
									'Wanita'=>'Wanita',)
								,array("empty"=>"-- Pilih Jenis Kelamin --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>	
		<?php echo $form->error($model,'jenis_kelamin'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pangkat'); ?>
		<?php echo $form->textField($model,'pangkat',array('size'=>50,'maxlength'=>50,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'pangkat'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'golongan'); ?>
		<?php echo $form->textField($model,'golongan',array('size'=>20,'maxlength'=>20,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'golongan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'unit_kerja'); ?>
		<?php echo $form->textField($model,'unit_kerja',array('size'=>50,'maxlength'=>50,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'unit_kerja'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_instansi'); ?>
		<?php echo $form->textField($model,'nama_instansi',array('size'=>60,'maxlength'=>100,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'nama_instansi'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'alamat_kantor'); ?>
		<?php echo $form->textArea($model,'alamat_kantor',array('size'=>60,'maxlength'=>255,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'alamat_kantor'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pusat_daerah'); ?>
		<?php echo $form->dropDownList($model,'pusat_daerah', 
								array('Pusat'=>'Pusat',		
									'Daerah'=>'Daerah',)
								,array("empty"=>"-- Pilih Pilihan --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>	
		<?php echo $form->error($model,'pusat_daerah'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pns_nonpns'); ?>
		<?php echo $form->dropDownList($model,'pns_nonpns', 
								array('PNS'=>'PNS',		
									'Non PNS'=>'Non PNS',)
								,array("empty"=>"-- Pilih Pilihan --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>	
		<?php echo $form->error($model,'pns_nonpns'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jabatan'); ?>
		<?php echo $form->textField($model,'jabatan',array('size'=>50,'maxlength'=>50,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'jabatan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pendidikan'); ?>
		<?php echo $form->textField($model,'pendidikan',array('size'=>50,'maxlength'=>50,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'pendidikan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jurusan'); ?>
		<?php echo $form->textField($model,'jurusan',array('size'=>60,'maxlength'=>100,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'jurusan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'telepon'); ?>
		<?php echo $form->textField($model,'telepon',array('size'=>40,'maxlength'=>30, 'onkeyup'=>'validAngka(this)','class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'telepon'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'handphone'); ?>
		<?php echo $form->textField($model,'handphone',array('size'=>40,'maxlength'=>30, 'onkeyup'=>'validAngka(this)','class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'handphone'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->emailField($model,'email',array('size'=>60,'maxlength'=>100,'class' => 'form-control', 'required'=>true)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="form-group">
		<button type="button" class="btn btn-default" onclick="openstep1()">Prev</button>
		<button type="button" class="btn btn-primary" onclick="openstep3()">Next</button>
	</div>	
</div>
<div style="display: none;" id="step3">
	<div class="form-group">
		<?php echo $form->labelEx($model,'pembiayaan'); ?>
		<?php echo $form->dropDownList($model,'pembiayaan', 
								array('APBN'=>'APBN',
										'Kerjasama'=>'Kerjasama',)
								,array("empty"=>"-- Pilih Pembiayaan --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>
		<?php echo $form->error($model,'pembiayaan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'sertifikasi'); ?>
		<?php echo $form->dropDownList($model,'sertifikasi', 
								array('Sertifikasi'=>'Sertifikasi',
										'Tidak'=>'Tidak',)
								,array("empty"=>"-- Pilih Sertifikasi --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>
		<?php echo $form->error($model,'sertifikasi'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'lulus_sertifikasi'); ?>
		<?php echo $form->dropDownList($model,'lulus_sertifikasi', 
								array('Lulus Sertifikasi'=>'Lulus Sertifikasi',
										'Not Available'=>'Not Available',)
								,array("empty"=>"-- Pilih Status --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>
		<?php echo $form->error($model,'lulus_sertifikasi'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kesesuaian_jabatan'); ?>
		<?php echo $form->dropDownList($model,'kesesuaian_jabatan', 
								array('Sesuai'=>'Sesuai',
										'Tidak'=>'Tidak',)
								,array("empty"=>"-- Pilih Kesesuaian Jabatan --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>
		<?php echo $form->error($model,'kesesuaian_jabatan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kesesuaian_pendidikan'); ?>
		<?php echo $form->dropDownList($model,'kesesuaian_pendidikan', 
								array('Sesuai'=>'Sesuai',
										'Tidak'=>'Tidak',)
								,array("empty"=>"-- Pilih Kesesuaian Pendidikan --",
								'style'=>'width:100;','class' => 'form-control', 'required'=>true)
							);
		?>
		<?php echo $form->error($model,'kesesuaian_pendidikan'); ?>
	</div>

	<div class="form-group">
		<button type="button" class="btn btn-default" onclick="openstep2()">Prev</button>		
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<script language='javascript'>
	function openstep1(){
		document.getElementById('step1').style.display = 'block';
		document.getElementById('step2').style.display = 'none';
		document.getElementById('step3').style.display = 'none';
	}
	function openstep2(){
		document.getElementById('step1').style.display = 'none';
		document.getElementById('step2').style.display = 'block';
		document.getElementById('step3').style.display = 'none';
	}
	function openstep3(){
		document.getElementById('step1').style.display = 'none';
		document.getElementById('step2').style.display = 'none';
		document.getElementById('step3').style.display = 'block';
	}

	function validAngka(a)
	{
		if(!/^[0-9.]+$/.test(a.value))
		{
		a.value = a.value.substring(0,a.value.length-1000);
		}
	}
</script>