<?php
/* @var $this PesertaPlthnController */
/* @var $data PesertaPlthn */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('psrt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->psrt_id), array('view', 'id'=>$data->psrt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('no_urut_bidang')); ?>:</b>
	<?php echo CHtml::encode($data->no_urut_bidang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bidang')); ?>:</b>
	<?php echo CHtml::encode($data->bidang); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kluster')); ?>:</b>
	<?php echo CHtml::encode($data->kluster); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pelatihan')); ?>:</b>
	<?php echo CHtml::encode($data->pelatihan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_mulai_diklat')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_mulai_diklat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_selesai_diklat')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_selesai_diklat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('balai_diklat')); ?>:</b>
	<?php echo CHtml::encode($data->balai_diklat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomor')); ?>:</b>
	<?php echo CHtml::encode($data->nomor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip')); ?>:</b>
	<?php echo CHtml::encode($data->nip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_peserta')); ?>:</b>
	<?php echo CHtml::encode($data->nama_peserta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tempat_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tempat_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tanggal_lahir')); ?>:</b>
	<?php echo CHtml::encode($data->tanggal_lahir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('agama')); ?>:</b>
	<?php echo CHtml::encode($data->agama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenis_kelamin')); ?>:</b>
	<?php echo CHtml::encode($data->jenis_kelamin); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pangkat')); ?>:</b>
	<?php echo CHtml::encode($data->pangkat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('golongan')); ?>:</b>
	<?php echo CHtml::encode($data->golongan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unit_kerja')); ?>:</b>
	<?php echo CHtml::encode($data->unit_kerja); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_instansi')); ?>:</b>
	<?php echo CHtml::encode($data->nama_instansi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alamat_kantor')); ?>:</b>
	<?php echo CHtml::encode($data->alamat_kantor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pusat_daerah')); ?>:</b>
	<?php echo CHtml::encode($data->pusat_daerah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pns_nonpns')); ?>:</b>
	<?php echo CHtml::encode($data->pns_nonpns); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jabatan')); ?>:</b>
	<?php echo CHtml::encode($data->jabatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pendidikan')); ?>:</b>
	<?php echo CHtml::encode($data->pendidikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jurusan')); ?>:</b>
	<?php echo CHtml::encode($data->jurusan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telepon')); ?>:</b>
	<?php echo CHtml::encode($data->telepon); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('handphone')); ?>:</b>
	<?php echo CHtml::encode($data->handphone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pre_test')); ?>:</b>
	<?php echo CHtml::encode($data->pre_test); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pro_test')); ?>:</b>
	<?php echo CHtml::encode($data->pro_test); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilai_akhir')); ?>:</b>
	<?php echo CHtml::encode($data->nilai_akhir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('predikat')); ?>:</b>
	<?php echo CHtml::encode($data->predikat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status_kelulusan')); ?>:</b>
	<?php echo CHtml::encode($data->status_kelulusan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pembiayaan')); ?>:</b>
	<?php echo CHtml::encode($data->pembiayaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sertifikasi')); ?>:</b>
	<?php echo CHtml::encode($data->sertifikasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lulus_sertifikasi')); ?>:</b>
	<?php echo CHtml::encode($data->lulus_sertifikasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kesesuaian_jabatan')); ?>:</b>
	<?php echo CHtml::encode($data->kesesuaian_jabatan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kesesuaian_pendidikan')); ?>:</b>
	<?php echo CHtml::encode($data->kesesuaian_pendidikan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>