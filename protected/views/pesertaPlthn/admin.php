<?php
/* @var $this PesertaPlthnController */
/* @var $model PesertaPlthn */
$this->pageTitle="Program dan Evaluasi";
$this->breadcrumbs=array(
	'Program dan Evaluasi'=>array('admin')
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#peserta-plthn-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

 <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
            	<div class="pull-right">
					<?php 
					echo CHtml::link('<i class="fa fa-plus"></i> Create',
					array('create'),
					array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
					array('excel'),
					array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
					?>
	            </div>
	            <h6>Data Peserta Pelatihan</h6>
	        </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
	              <div class="dataTable-container">
								<?php $this->widget('zii.widgets.grid.CGridView', array(
									'id'=>'peserta-plthn-grid',
									'dataProvider'=>$model->search(),
									'filter'=>$model,
									'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
									'itemsCssClass'=>'table table-flush dataTable-table',
									'columns'=>array(
										array(
											'header'=>'#',
											'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
											'htmlOptions'=>array('width'=>'10px','style'=>'text-align:center')
										),
										array(
											'name'=>'nama_peserta','type'=>'raw',
										  	'value'=>'CHtml::link($data->nama_peserta,array("PesertaPlthn/view","id"=>$data->psrt_id),array("class"=>"badge badge-sm bg-gradient-secondary"))."<br><b class=`mb-0 text-xxs`>".$data->nip."</b>"',
											'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
							  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
										),
										array(
											'name'=>'pelatihan','type'=>'raw',
									  	'value'=>'CHtml::link(substr($data->Pelatihan->nm_pelatihan,0,25)."...",array("pelaksanaanPlth/viewDetail","id"=>$data->pel_id),array("class"=>"badge badge-sm bg-gradient-secondary"))',
											'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
							  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
										),
										array(
											'name'=>'unit_kerja','type'=>'raw',
									  	'value'=>'substr($data->unit_kerja,0,25)."..."',
											'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
							  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
										),										
										array(
											'name'=>'jabatan','type'=>'raw',
									  	'value'=>'substr($data->jabatan,0,25)."..."',
											'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
							  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
										),										
										array(
											'name'=>'pusat_daerah','type'=>'raw',
									  	'value'=>'$data->pusat_daerah',
											'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
							  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
										),
										array(
											'name'=>'nilai_akhir','type'=>'raw',
									  	'value'=>'$data->nilai_akhir',
											'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
							  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
										),
										array(
											'name'=>'predikat','type'=>'raw',
									  	'value'=>'$data->predikat',
											'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
							  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
										),
										array(
											'header'=>'Aksi',
											'class'=>'CButtonColumn',
											'template'=>'{update} {delete}',				
											'htmlOptions'=>array('width'=>'150px', 
											'style' => 'text-align: center;'),
										),
									),
								)); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
</div>