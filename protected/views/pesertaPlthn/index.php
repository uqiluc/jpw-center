<?php
/* @var $this PesertaPlthnController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Peserta Plthns',
);

$this->menu=array(
	array('label'=>'Create PesertaPlthn', 'url'=>array('create')),
	array('label'=>'Manage PesertaPlthn', 'url'=>array('admin')),
);
?>

<h1>Peserta Plthns</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
