<?php
/* @var $this PesertaPlthnController */
/* @var $model PesertaPlthn */
$this->pageTitle="Program dan Evaluasi";
$this->breadcrumbs=array(
	'Program dan Evaluasi'=>array('index'),
	$model->nama_peserta,
);

?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_peserta;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->nip;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
		<?php 
		echo CHtml::link('<i class="fa fa-edit"></i> Edit',
		array('update','id'=>$model->psrt_id),
		array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-th"></i> Data Peserta',
		array('admin'),
		array('class' => 'btn btn-default','title'=>'Kelola Data'));
		?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>

<div class="container-fluid py-1">
<div class="row">
    <div class="col-12 col-xl-6">
      <div class="card">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Peserta</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
        	<?php $this->widget('zii.widgets.CDetailView', array(
				'data'=>$model,
			    'htmlOptions'=>array('class'=>"table table-flush dataTable-table"),			
				'attributes'=>array(
					'nomor',
					'nip',
					'nama_peserta',
					'tempat_lahir',
					'tanggal_lahir',
					'agama',
					'jenis_kelamin',
					'pendidikan',
					'jurusan',
					'telepon',
					'handphone',
					'email',
				),
			)); ?>
        </div>
      </div>
    </div>
    <div class="col-12 col-xl-6">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Diklat</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
        	<?php $this->widget('zii.widgets.CDetailView', array(
				'data'=>$model,
			    'htmlOptions'=>array('class'=>"table table-flush dataTable-table"),			
				'attributes'=>array(
					'no_urut_bidang',
					'bidang',
					'kluster',
					array('name'=>'pelatihan','type'=>'raw',
					  	  'value'=>CHtml::link($model->pelatihan." <i class='fa fa-eye'></i>",array("pelaksanaanPlth/viewDetail","id"=>$model->pel_id)),
					),
					'tanggal_mulai_diklat',
					'tanggal_selesai_diklat',
					'balai_diklat',
				),
			)); ?>
        </div>
      </div>
    </div>
    <div class="col-12 col-xl-6 my-2">
      <div class="card">
        <div class="card-header  pb-0 p-3">
          <h6 class="mb-0">Instansi</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
          <div class="table-responsive p-0">
              <div class="dataTable-container">
	        	<?php $this->widget('zii.widgets.CDetailView', array(
					'data'=>$model,
				    'htmlOptions'=>array('class'=>"table table-flush dataTable-table"),
					'attributes'=>array(
						'pangkat',
						'golongan',
						'unit_kerja',
						'nama_instansi',
						'alamat_kantor',
						'pusat_daerah',
						'pns_nonpns',
						'jabatan',
					),
				)); ?>
			  </div>
		  </div>
        </div>
      </div>
    </div>
    <div class="col-12 col-xl-6 my-2">
      <div class="card">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Nilai</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
          <div class="table-responsive p-0">
              <div class="dataTable-container">
	        	<?php $this->widget('zii.widgets.CDetailView', array(
					'data'=>$model,
				    'htmlOptions'=>array('class'=>"table table-flush dataTable-table"),			
					'attributes'=>array(
						'pre_test',
						'pro_test',
						'nilai_akhir',
						'predikat',
						'status_kelulusan',
						'pembiayaan',
						'sertifikasi',
						'lulus_sertifikasi',
						'kesesuaian_jabatan',
						'kesesuaian_pendidikan',
					),
				)); ?>
			  </div>
		  </div>
        </div>
      </div>
    </div>
</div>
</div>