<?php
/* @var $this ProfilPelatihanController */
/* @var $model ProfilPelatihan */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profil-pelatihan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_pelatihan'); ?>
		<?php echo $form->textField($model,'nama_pelatihan',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_pelatihan', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",
				'style'=>'width:100;','class' => 'form-control')
			);
			?>
		<?php echo $form->error($model,'bidang', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'stdr_komp_lulusan'); ?>
		<?php echo $form->textArea($model,'stdr_komp_lulusan',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'stdr_komp_lulusan', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'tahun'); ?>
		<?php echo $form->numberField($model,'tahun',array('size'=>4,'maxlength'=>4, 'onkeyup'=>'validAngka(this)','class' => 'form-control')); ?>
		<?php echo $form->error($model,'tahun', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'timPenyusun'); ?>
		<?php echo $form->textArea($model,'timPenyusun',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'timPenyusun', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'sasaran_psrta'); ?>
		<?php echo $form->textArea($model,'sasaran_psrta',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'sasaran_psrta', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kualifikasi'); ?>
		<?php echo $form->textArea($model,'kualifikasi',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'kualifikasi', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'mata_plthn'); ?>
		<?php echo $form->textArea($model,'mata_plthn',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'mata_plthn', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pola'); ?>
		<?php echo $form->dropDownList($model,'pola', 
			array('-'=>'-',
				  'Klasikal'=>'Klasikal',
				  'Blended Learning'=>'Blended Learning',
				  'Code Intra'=>'Code Intra',
				  'Full E-Learning'=>'Full E-Learning',)
			,array("empty"=>"-- Pilih Pola --",'style'=>'width:100;','class' => 'form-control'));
		?>	
		<?php echo $form->error($model,'pola', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'durasi_plthn'); ?>
		<div class="input-group">
		<?php echo $form->numberField($model,'durasi_plthn',array('size'=>4,'maxlength'=>4, 'onkeyup'=>'validAngka(this)','class' => 'form-control')); ?>
            <span class="input-group-text" id="basic-addon2">Hari</span>
        </div>		
		<?php echo $form->error($model,'durasi_plthn', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jml_jp'); ?>
		<div class="input-group">
		<?php echo $form->numberField($model,'jml_jp',array('size'=>3,'maxlength'=>3, 'onkeyup'=>'validAngka(this)','class' => 'form-control')); ?>
            <span class="input-group-text" id="basic-addon2">JP</span>
        </div>		
		<?php echo $form->error($model,'jml_jp', array('class'=>'label label-danger')); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script language='javascript'>
	function validAngka(a)
	{
		if(!/^[0-9.]+$/.test(a.value))
		{
		a.value = a.value.substring(0,a.value.length-1000);
		}
	}
</script>