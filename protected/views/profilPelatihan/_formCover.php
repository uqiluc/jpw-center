<?php
/* @var $this ProfilPelatihanController */
/* @var $model ProfilPelatihan */
/* @var $form CActiveForm */
$url = Yii::app()->baseUrl;

if ($model->coverImage == ''){
	$imgprofil = $url."/Dokumen/noImage.jpg";
}else{
	$imgprofil = $url."/Dokumen/CoverDokumen/".$model->coverImage;
}
?>

<div class="col-lg-12">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'profil-pelatihan-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data','autocomplete'=>'off'),
)); ?>

<div class="col-lg-4">
	<img src="<?php echo $imgprofil;?>" id="cekimg" width="100%">
</div>
<div class="col-lg-8">
	<div class="form-group">
		<?php echo $form->labelEx($model,'coverImage'); ?>
		<?php echo $form->fileField($model,'coverImage',array('size'=>60,'maxlength'=>255,'class'=>'form-control')); ?>
		<?php echo $form->error($model,'coverImage',array('class'=>'label label-warning')); ?>
	</div>
	
	<div class="form-group">	
     	<?php echo CHtml::submitButton('Simpan', array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<script type="text/javascript">
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#cekimg').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

$("#ProfilPelatihan_coverImage").change(function() {
  readURL(this);
});
</script>