<?php
/* @var $this ProfilPelatihanController */
/* @var $model ProfilPelatihan */
/* @var $form CActiveForm */
?>

<div class="wide form">
<h6>Cari Pelatihan</h6>
<p style="font-size:12px">Gunakan opsi ini untuk mencari Pelatihan yang ingin Anda cari.</p>
<hr class="horizontal gray-light my-1">
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('profilPelatihan/index'),
	'method'=>'get',
)); ?>

	<div class="form-group">
		<?php echo $form->label($model,'bidang'); ?>
		<?php echo $form->dropDownList ($model, 'bidang',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",
				'style'=>'width:100;','class' => 'form-control')
			);
			?>
	</div>

	<div class="form-group">
		<?php echo $form->label($model,'nama_pelatihan'); ?>
		<?php echo $form->searchField($model,'nama_pelatihan',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Cari', array('class' => 'btn btn-primary pull-right','style'=>'width:100%')); ?>
      	<?php 
			echo CHtml::link('Tambah Kurmod',
			array('kurmodDetail/createKurmod?bidang='.$model->bidang),
			array('class' => 'btn btn-info','title'=>'Tambah Data Baru','style'=>'width:100%'));
		?>     	
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->