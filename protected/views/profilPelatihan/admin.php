<?php
/* @var $this ProfilPelatihanController */
/* @var $model ProfilPelatihan */
$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Profil Pelatihan'=>array('admin')
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#profil-pelatihan-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

 <div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
            	<div class="pull-right">
					<?php 
					echo CHtml::link('<i class="fa fa-plus"></i> Create',
					array('create'),
					array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
					array('excel'),
					array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
					?>
	            </div>
	            <h6>Data Profil Pelatihan</h6>
	        </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
	              <div class="dataTable-container">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'profil-pelatihan-grid',
						'dataProvider'=>$model->search($_GET['type']),
						'filter'=>$model,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'header'=>'#',
								'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')							),
							array('name'=>'nama_pelatihan','type'=>'raw',
							  	  'value'=>'CHtml::link($data->nama_pelatihan,array("profilPelatihan/view?id=".$data->prof_id."&type=profile"),array("class"=>"badge badge-sm bg-gradient-secondary"));',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'tahun',
								'value'=>'$data->tahun',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')							),
							array(
								'name'=>'jml_jp',
								'value'=>'$data->jml_jp',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'header'=>'Total Pelaksana','type'=>'raw',
								'value'=>'ProfilPelatihan::model()->totalPelaksana($data->prof_id)',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'header'=>'Aksi',
								'class'=>'CButtonColumn',
								'template'=>'{update} {delete}',
								'htmlOptions'=>array('width'=>'100px', 
								'style' => 'text-align: center;'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
						),
					)); ?>
					</div>

					</div>
				<div>
			</div>
		</div>	
	</div>
</div>