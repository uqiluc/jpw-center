<?php
$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Pelatihan'=>array('/pelatihan')
);

$Url = Yii::app()->baseUrl; 

?>

<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-3">
      <div class="card">
        <div class="card-body p-3">
        	<?php echo $this->renderPartial('_search', array('model'=>$model)); ?>
		    </div>
	     </div>
	  </div>
    <div class="col-12 col-xl-9">
      <div class="card h-100">
        <div class="card-body p-3">
					<div class="row">
					<?php $this->widget('zii.widgets.CListView', array(
						'dataProvider'=>$model->search(),
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemView'=>'_view',
					)); ?>
		  		</div>
        </div> 
	  </div>
	</div>
    
</div>
</div>