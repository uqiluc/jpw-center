<?php
$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Pelatihan'=>array('/pelatihan')
);

$Url = Yii::app()->baseUrl; 

?>

<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-3">
      <div class="card h-100">
        <div class="card-body p-3">
		    </div>
	     </div>
	  </div>
    <div class="col-12 col-xl-9">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0"><?php echo $model->nama_pelatihan;?></h6>
          <hr class="horizontal gray-light my-1">          
        </div>
        <div class="card-body p-3">
        	<div class="nav-wrapper position-relative end-0">
					   <ul class="nav nav-pills nav-fill p-1" role="tablist">
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1 active"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=profile';?>" >
					         Profile
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=kurikulum';?>" >
					         Kurikulum
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=modul';?>" >
					         Modul
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=bahan_tayang';?>" >
					         Bahan Tayang
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=other';?>" >
					         Lain-Lain
					         </a>
					      </li>					      
					    </ul>
					</div>
					<hr>
					<ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Nama Pelatihan</strong> <br> <?php echo $model->nama_pelatihan;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Bidang</strong> <br> <?php echo $model->Bidang->nama_bidang;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Standar Kompetensi Lulusan</strong> <br> <?php echo $model->stdr_komp_lulusan;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tim Penyusun</strong> <br> <?php echo $model->timPenyusun;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tahun</strong> <br> <?php echo $model->tahun;?></li>
          </ul> 
		    </div>
	    </div>
    
	  </div>
</div>
</div>