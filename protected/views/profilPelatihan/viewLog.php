<div class="row">
<div class="col-lg-12">
<div class="pull-right">
    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>		
</div>
<hr>
<?php 
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
    'htmlOptions'=>array('class'=>"table"),	
	'attributes'=>array(
		array('label'=>'Nama Pelatihan Saat Ini','type'=>'raw',
		  	  'value'=>$model->nama_pelatihan,
		),
		array('name'=>'bidang','type'=>'raw',
		  	  'value'=>$model->Bidang->nama_bidang,
		),
		'stdr_komp_lulusan',
		'timPenyusun',
		'tahun'
	),
)); 	
?>
<hr>
<center><h4><b>Data Riwayat Perubahan data</b></h4></center>
	<?php $ttlpesrt = Yii::app()->db->createCommand("SELECT COUNT(id) FROM log_profile_pelatihan WHERE profileId = $model->prof_id")->queryScalar(); ?>
	<b>Total :</b> <?php echo $ttlpesrt;?> Perubahan
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'log-profile-grid',
		'dataProvider'=>$log->getProfile($model->prof_id),
		'filter'=>$log,
		'itemsCssClass'=>'table table-striped table-bordered table-hover',	
		'columns'=>array(
			array(
				'header'=>'#',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
				'htmlOptions'=>array('width'=>'10px', 
				'style' => 'text-align: center; background-color: #3c8dbc; color:#ffffff;')
			),
			'initialName',
			'nameChange',
			'createTime',
			array(
				'name'=>'createdBy','type'=>'raw',
				'value'=>'$data->Users->nama_lengkap'
			)
		),
	)); ?>
<br>
</div>