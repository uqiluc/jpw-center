<?php
/* @var $this KurmodDetailController */
/* @var $model KurmodDetail */

$baseUrl = Yii::app()->baseurl;
$bidang = $_GET['type'];
$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Pelatihan'=>array('index'),
	'Jalan'
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#kurmod-detail-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script src="//static.fliphtml5.com/web/js/plugin/LightBox/js/fliphtml5-light-box-api-min.js"></script>
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            	<div class="pull-right">
					<?php 
					echo CHtml::link('<i class="fa fa-file"></i> Penyusunan Kurmod',
					array('kurmodDetail/createKurmod?bidang='.$bidang),
					array('class' => 'btn btn-default','title'=>'Penyusunan Kurmod'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-file"></i> Data WI',
					array('widyaiswara/admin?type='.$bidang),
					array('class' => 'btn btn-default','title'=>'Data WI'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-archive"></i> Arsip',
					array('kurmodDetail/admin?bidang='.$bidang),
					array('class' => 'btn btn-default','title'=>'Data Arsip'));
					?>	
	            </div>
	            <h6>Data Pelatihan - Subkor <?php echo Bidang::model()->findByPk($bidang)->nama_bidang;?></h6>
	        </div>
	        </div>
	      </div>
	    </div>
</div>

<div class="container-fluid py-2">
    <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
            	<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'kurmod-detail-grid-jalan',
					'dataProvider'=>$model->getPelatihan($bidang),
					'filter'=>$model,
					'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
					'itemsCssClass'=>'table table-flush dataTable-table',
					'columns'=>array(
						array(
							'name'=>'prof_id', 'type'=>'raw',
							'value'=>'CHtml::link($data->ProfilPelatihan->nama_pelatihan,array("profilPelatihan/view","id"=>$data->prof_id),array("class"=>"badge badge-sm bg-gradient-secondary"))',
							'filter'=>CHtml::listData(ProfilPelatihan::model()->findAll('bidang=8'),'prof_id', 'nama_pelatihan'),
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'name'=>'jns_dokumen',
							'value'=>'$data->jns_dokumen',
							'filter'=>array('Modul'=>'Modul','Buku Kurikulum'=>'Buku Kurikulum','Bahan Tayang'=>'Bahan Tayang','Pedoman Pelatihan'=>'Pedoman Pelatihan','Buku Soal'=>'Buku Soal'),
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'name'=>'nama_dokumen','type'=>'raw',
							'value'=>'CHtml::link($data->nama_dokumen,array("KurmodDetail/view","id"=>$data->kmd_id),array("class"=>"badge badge-sm bg-gradient-secondary"))',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'name'=>'tim_penyusun',
							'value'=>'$data->tim_penyusun',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'header'=>'Aksi',
							'class'=>'CButtonColumn',
							'template'=>'{update}{delete}',
							'htmlOptions'=>array('width'=>'150px', 
							'style' => 'text-align: center;'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
					),
				)); 
				?>
			</div>
		</div>
	</div>

</div>