<?php
$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Pelatihan'=>array('/pelatihan')
);

$Url = Yii::app()->baseUrl; 

?>
<script src="//static.fliphtml5.com/web/js/plugin/LightBox/js/fliphtml5-light-box-api-min.js"></script>

<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-3">
      <div class="card">
        <div class="card-body p-3">
        	<?php echo $this->renderPartial('_search', array('model'=>$model)); ?>
		    </div>
	     </div>
	  </div>
    <div class="col-12 col-xl-9">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0"><?php echo $model->nama_pelatihan;?></h6>
          <hr class="horizontal gray-light my-1">          
        </div>
        <div class="card-body p-3">
        	<div class="nav-wrapper position-relative end-0">
					   <ul class="nav nav-pills nav-fill p-1" role="tablist">
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=profile';?>" >
					         Profile
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=kurikulum&filter=Name';?>">
					         Kurikulum
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1 "  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=modul&filter=Name';?>" >
					         Modul
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=bahan_tayang&filter=Name';?>" >
					         Bahan Tayang
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1 selected"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=other&filter=Name';?>" >
					         Lain-Lain
					         </a>
					      </li>					      
					    </ul>
					</div>
					<hr>
					<div class="row">
  					<div class="col-lg-12 filter-sort">
  						<div class="pull-right">
	  						Sort by : 
	  						<select class="sort-select" id="filter-sort" onchange="getSort()">
	  							<option <?php if ($_GET['filter'] == 'Name') {$filter = 'nama_dokumen'; echo "selected";}?>>Name</option>	
	  							<option <?php if ($_GET['filter'] == 'Update') {$filter = 'updated'; echo "selected";}?>>Update</option>	
	  						</select>
	  					</div>
  					</div>
					<hr>
  					<?php foreach (KurmodDetail::model()->findAll('prof_id='.$model->prof_id.' AND (jns_dokumen != "Buku Kurikulum" AND jns_dokumen != "bahan_tayang" AND jns_dokumen != "Modul") ORDER BY '.$filter.' ASC') as $Kurmod) {
  						?>
					    <div class="list-modul">
						    <div class="col-lg-2">
							    <img src="<?php echo $Kurmod->cover_dokumen;?>" width="100px"><br>
						    </div>
						    <div class="col-lg-6">
		  						<div style="display:block;margin-left: 10px;width: 100%;">
		  							<b style="font-size: 18px"><?php echo $Kurmod->nama_dokumen;?></b>
		  							<p class="description-modul"><?php echo $Kurmod->description;?></p>
		  						</div>
							  </div>
						    <div class="col-lg-4" style="margin: auto;">
		  						<div style="font-size: 14px;display:flex;" class="pull-right">
		  							<a href="<?php echo $Kurmod->file_dokumen;?>">Unduh</a>
		  							<a href="<?php echo $Kurmod->gdriveUrl;?>" class="link-media-modul" data-rel="fh5-light-box-demo" data-href="<?php echo $Kurmod->gdriveUrl;?>" data-width="700" data-height="400" data-title="<?php echo $Kurmod->nama_dokumen;?>">Lanjut Baca</a>
		  							<!-- <a href="<?php echo $Kurmod->gdriveUrl;?>" class="link-media-modul" ><i class="fa fa-edit"></i></a>
		  							<a href="<?php echo $Kurmod->gdriveUrl;?>" class="link-media-modul" ><i class="fa fa-times"></i></a> -->
		  						</div>
							  </div>
						  </div>
					  <?php } ?>
          </div>
        </div> 
		    </div>
	    </div>
    
	  </div>
</div>
</div>

<script type="text/javascript">
	function getSort() {
		var sortValue = document.getElementById('filter-sort').value;
    window.location.href="http://localhost/jpwnew/profilPelatihan/1?type=modul&filter="+sortValue;
	}
</script>