<?php
$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Pelatihan'=>array('/pelatihan')
);

$Url = Yii::app()->baseUrl; 

?>
<script src="//static.fliphtml5.com/web/js/plugin/LightBox/js/fliphtml5-light-box-api-min.js"></script>

<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-3">
      <div class="card">
        <div class="card-body p-3">
        	<?php echo $this->renderPartial('_search', array('model'=>$model)); ?>
		    </div>
	     </div>
	  </div>
    <div class="col-12 col-xl-9">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <div class="pull-right">
          	<?php 
							echo CHtml::link('<i class="fa fa-edit"></i>',
							array('update','id'=>$model->prof_id),
							array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
						?>
          </div>
          <h6 class="mb-0"><?php echo $model->nama_pelatihan;?></h6>
          <hr class="horizontal gray-light my-1">          
        </div>
        <div class="card-body p-3">
        	<div class="nav-wrapper position-relative end-0">
					   <ul class="nav nav-pills nav-fill p-1" role="tablist">
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1 selected"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=profile';?>" >
					         Profile
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=kurikulum&filter=Name';?>">
					         Kurikulum
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1 "  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=modul&filter=Name';?>" >
					         Modul
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=bahan_tayang&filter=Name';?>" >
					         Bahan Tayang
					         </a>
					      </li>
					      <li class="nav-item">
					         <a class="nav-link mb-0 px-0 py-1"  href="<?php echo $Url.'/profilPelatihan/'.$model->prof_id.'?type=other&filter=Name';?>" >
					         Lain-Lain
					         </a>
					      </li>					      
					    </ul>
					</div>
					<hr>
					<div class="row">
						<div class="col-lg-6">
							<ul class="list-group">
		            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Nama Pelatihan</strong> <br> <?php echo $model->nama_pelatihan;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Bidang</strong> <br> <?php echo $model->Bidang->nama_bidang;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Standar Kompetensi Lulusan</strong> <br> <?php echo $model->stdr_komp_lulusan;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tim Penyusun</strong> <br> <?php echo $model->timPenyusun;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tahun</strong> <br> <?php echo $model->tahun;?></li>
		          </ul> 							
						</div>
						<div class="col-lg-6">
							<ul class="list-group">
		            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Sasaran Peserta</strong> <br> <?php echo $model->sasaran_psrta;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Kualifikasi</strong> <br> <?php echo $model->kualifikasi;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Pola</strong> <br> <?php echo $model->pola;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Durasi Pelatihan</strong> <br> <?php echo $model->durasi_plthn;?></li>
		            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jumlah JP</strong> <br> <?php echo $model->jml_jp;?></li>
		          </ul> 							
						</div>
        </div> 
		    </div>
	    </div>
    
	  </div>
</div>
</div>

<script type="text/javascript">
	function getSort() {
		var sortValue = document.getElementById('filter-sort').value;
    window.location.href="http://localhost/jpwnew/profilPelatihan/1?type=modul&filter="+sortValue;
	}
</script>