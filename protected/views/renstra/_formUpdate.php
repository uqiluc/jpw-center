<?php
/* @var $this RenstraController */
/* @var $model Renstra */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'renstra-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>

	<div class="form-group">	
		<?php echo $form->labelEx($model,'target_jml_plth'); ?>
		<?php echo $form->textField($model,'target_jml_plth',array('size'=>10,'maxlength'=>10, 'onkeyup'=>'validAngka(this)','class' => 'form-control')); ?>
		<?php echo $form->error($model,'target_jml_plth'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'target_jml_psrt'); ?>
		<?php echo $form->textField($model,'target_jml_psrt',array('size'=>10,'maxlength'=>10, 'onkeyup'=>'validAngka(this)','class' => 'form-control')); ?>
		<?php echo $form->error($model,'target_jml_psrt'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script language='javascript'>
	function validAngka(a)
	{
		if(!/^[0-9.]+$/.test(a.value))
		{
		a.value = a.value.substring(0,a.value.length-1000);
		}
	}
</script>