<?php
/* @var $this RenstraController */
/* @var $model Renstra */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'rs_id'); ?>
		<?php echo $form->textField($model,'rs_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'target_jml_plth'); ?>
		<?php echo $form->textField($model,'target_jml_plth',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'target_jml_psrt'); ?>
		<?php echo $form->textField($model,'target_jml_psrt',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dokumen_renstra'); ?>
		<?php echo $form->textField($model,'dokumen_renstra',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->