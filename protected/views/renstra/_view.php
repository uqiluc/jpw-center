<?php
/* @var $this RenstraController */
/* @var $data Renstra */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('rs_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->rs_id), array('view', 'id'=>$data->rs_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('target_jml_plth')); ?>:</b>
	<?php echo CHtml::encode($data->target_jml_plth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('target_jml_psrt')); ?>:</b>
	<?php echo CHtml::encode($data->target_jml_psrt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dokumen_renstra')); ?>:</b>
	<?php echo CHtml::encode($data->dokumen_renstra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />


</div>