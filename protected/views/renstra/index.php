<?php
/* @var $this RenstraController */
/* @var $dataProvider CActiveDataProvider */


$this->menu=array(
	array('label'=>'Tambah Renstra', 'url'=>array('create')),
	array('label'=>'Kelola Renstra', 'url'=>array('admin')),
);
?>

<h1>Renstra</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
