<div class="row">
<div class="col-lg-12">
	<?php 
	echo CHtml::link('<i class="fa fa-plus"></i> Tambah Renstra',
	array('create'),
	array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-edit"></i> Edit',
	array('update','id'=>$model->rs_id),
	array('class' => 'btn btn-primary','title'=>'Edit Data'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-file"></i> Dokumen',
	array('dokumen','id'=>$model->rs_id),
	array('class' => 'btn btn-primary','title'=>'Edit Dokumen'));
	?>
	<?php 
	echo CHtml::link('<i class="fa fa-th"></i> Kelola',
	array('admin'),
	array('class' => 'btn btn-primary','title'=>'Kelola Data'));
	?>	
	<div class="pull-right">
    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>		
	</div>
	<hr>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions'=>array('class'=>"table"),	
	'attributes'=>array(
		'rs_id',
		'target_jml_plth',
		'target_jml_psrt',
		'dokumen_renstra',
		'created',
		'updated',
	),
)); ?>
</div>
</div>