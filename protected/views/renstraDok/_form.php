<?php
/* @var $this RenstraDokController */
/* @var $model RenstraDok */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'renstra-dok-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nama_renstra'); ?>
		<?php echo $form->textField($model,'nama_renstra',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'nama_renstra'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tahun'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker',
			array(
				'model'=>$model,
				'language'=>'id',
				'attribute'=>'tahun',
				'value' => $model->tahun,
				'options' => array(
					'dateFormat'=>'yy-mm-dd',
					'showOn'=>'button',
					'changeMonth' => 'true',
					'changeYear' => 'true',
					'constrainInput' => 'false', 
					'duration'=>'fast', 
					'showAnim' =>'slide',
					
				),
				'htmlOptions'=>array('size'=>20,'aria-describedby'=>'sizing-addon1'),
			)
		); ?>
		<?php echo $form->error($model,'tahun'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dokumen'); ?>
		<?php echo $form->fileField($model,'dokumen',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'dokumen'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->