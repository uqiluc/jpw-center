<?php
/* @var $this RenstraDokController */
/* @var $data RenstraDok */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ren_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ren_id), array('view', 'id'=>$data->ren_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_renstra')); ?>:</b>
	<?php echo CHtml::encode($data->nama_renstra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tahun')); ?>:</b>
	<?php echo CHtml::encode($data->tahun); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dokumen')); ?>:</b>
	<?php echo CHtml::encode($data->dokumen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />


</div>