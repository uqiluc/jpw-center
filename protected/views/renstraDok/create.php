<?php
/* @var $this RenstraDokController */
/* @var $model RenstraDok */

$this->menu=array(
	array('label'=>'Daftar Renstra Dok', 'url'=>array('index')),
	array('label'=>'Kelola Renstra Dok', 'url'=>array('admin')),
);
?>

<h1>Tambah Renstra Dok</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>