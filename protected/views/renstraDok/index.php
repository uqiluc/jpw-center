<?php
/* @var $this RenstraDokController */
/* @var $dataProvider CActiveDataProvider */

$this->menu=array(
	array('label'=>'Tambah Renstra Dok', 'url'=>array('create')),
	array('label'=>'Kelola Renstra Dok', 'url'=>array('admin')),
);
?>

<h1>Renstra Dok</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
