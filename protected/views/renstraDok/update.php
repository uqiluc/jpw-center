<?php
/* @var $this RenstraDokController */
/* @var $model RenstraDok */

$this->menu=array(
	array('label'=>'Daftar Renstra Dok', 'url'=>array('index')),
	array('label'=>'Tambah Renstra Dok', 'url'=>array('create')),
	array('label'=>'Lihat Renstra Dok', 'url'=>array('view', 'id'=>$model->ren_id)),
	array('label'=>'Kelola Renstra Dok', 'url'=>array('admin')),
);
?>

<h1>Perbaharui Renstra Dok <?php echo $model->ren_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>