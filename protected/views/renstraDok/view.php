<?php
/* @var $this RenstraDokController */
/* @var $model RenstraDok */


$this->menu=array(
	array('label'=>'Daftar Renstra Dok', 'url'=>array('index')),
	array('label'=>'Tambah Renstra Dok', 'url'=>array('create')),
	array('label'=>'Perbaharui Renstra Dok', 'url'=>array('update', 'id'=>$model->ren_id)),
	array('label'=>'Hapus Renstra Dok', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ren_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola Renstra Dok', 'url'=>array('admin')),
);
?>

<h1>Lihat Renstra Dok #<?php echo $model->ren_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'ren_id',
		'nama_renstra',
		'tahun',
		'dokumen',
		'created',
		'updated',
	),
)); ?>
