<?php
    $this->pageTitle="Kerjasama";
    $this->breadcrumbs=array(
        'Siswa'=>array('admin'),
        'Create Non Achievment Akademis'
    );
?>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
                <div class="pull-right">
                    <input type="button" value="Go Back" onclick="history.back(-1)" class='btn btn-default'/>
                </div>
                <h6>Create Non Achievment Akademis</h6>
            </div>
            <div class="card-body">

                <div class="col-lg-12">

                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'achievment_non_akademis-form',
                    'enableAjaxValidation'=>false,
                )); ?>

                    <div class="form-group">
                        <?php echo $form->labelEx($model,'deskripsi'); ?>
                        <?php echo $form->textArea($model,'deskripsi',array('rows'=>6, 'cols'=>50,'class' => 'form-control','placeholder'=>'Isi dengan deskripsi')); ?>
                        <?php echo $form->error($model,'deskripsi'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
                    </div>

                <?php $this->endWidget(); ?>

                </div><!-- form -->
            </div>
          </div>
        </div>
    </div>  
</div>  