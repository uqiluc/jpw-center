<?php
/* @var $this SiswaController */
/* @var $model Siswa */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'siswa-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_siswa'); ?>
		<?php echo $form->textField($model,'nama_siswa',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_siswa'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nip'); ?>
		<?php echo $form->textField($model,'nip',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nip'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenjang'); ?>
		<?php echo $form->textField($model,'jenjang',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'jenjang'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'universitas'); ?>
		<?php echo $form->textField($model,'universitas',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'universitas'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'prodi'); ?>
		<?php echo $form->textField($model,'prodi',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'prodi'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'jenis_beasiswa'); ?>
		<?php echo $form->textField($model,'jenis_beasiswa',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'jenis_beasiswa'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'status_pegawai'); ?>
		<?php echo $form->dropDownList($model,'status_pegawai', 
				array(
					'0'=>'Kontrak',
					'1'=>'Tetap')
				,array("empty"=>"-- Pilih Status --",
				'style'=>'width:100;','class' => 'form-control')
			);
		?>	
		<?php echo $form->error($model,'status_pegawai'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'semester'); ?>
		<?php echo $form->textField($model,'semester',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'semester'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bulan'); ?>
		<?php echo $form->textField($model,'bulan',array('size'=>50,'maxlength'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'bulan'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'deskripsi'); ?>
		<?php echo $form->textArea($model,'deskripsi',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'deskripsi'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->