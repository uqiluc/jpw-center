<?php
    $this->pageTitle="Siswa";
    $this->breadcrumbs=array(
        'Kerjasama'=>array('admin'),
        'View'
    );
?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_siswa;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->nip;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
		<?php 
		echo CHtml::link('<i class="fa fa-edit"></i> Edit',
		array('update','id'=>$model->id),
		array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-th"></i> Data Siswa',
		array('siswa/admin'),
		array('class' => 'btn btn-default','title'=>'Kelola Data'));
		?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>

<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-4">
      <div class="card">
        <div class="card-header pb-0 p-3">
          <h6 class="mb-0">Detail Siswa</h6>
          <hr class="horizontal gray-light my-1">
        </div>
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Jenjang</strong> <br> <?php echo $model->jenjang;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Universitas</strong> <br> <?php echo $model->universitas;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Prodi</strong> <br> <?php echo $model->prodi;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Jenis Beasiswa</strong> <br> <?php echo $model->jenis_beasiswa;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Status Pegawai</strong> <br> <?php echo $model->status_pegawai;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Semester</strong> <br> <?php echo $model->semester;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Bulan</strong> <br> <?php echo $model->bulan;?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Deskripsi</strong> <br> <?php echo $model->deskripsi;?></li>
          </ul>        	
		</div>
	  </div>
	</div>
    <div class="col-12 col-xl-8">
      <div class="card">
        <div class="card-header pb-0 p-3">
		  <?php 
			echo CHtml::link('<i class="fa fa-plus"></i> Tambah',
			array('siswa/createAchievmentAkademis?id='.$model->id),
			array('class' => 'btn btn-default pull-right','title'=>'Kelola Data'));
		  ?>        	
          <h6 class="mb-0">Achievment Akademis</h6>
          <hr class="horizontal gray-light my-1">
          <div class="table-responsive p-0">
              <div class="dataTable-container">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'karyasiswa-grid',
					'dataProvider'=>$AchievmentAkademis->search($model->id),
					'filter'=>$AchievmentAkademis,
					'summaryText'=>'',
					'itemsCssClass'=>'table table-flush dataTable-table',
					'columns'=>array(
						array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'name'=>'deskripsi','type'=>'raw',
						  	'value'=>'$data->deskripsi',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'header'=>'Aksi',
							'class'=>'CButtonColumn',
							'template'=>'{update} {delete}',
							'htmlOptions'=>array('width'=>'100px', 
							'style' => 'text-align: center;'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
					),
				)); ?>
              </div>
          </div>
		</div>
	  </div>

      <div class="card my-2">
        <div class="card-header pb-0 p-3">
		  <?php 
			echo CHtml::link('<i class="fa fa-plus"></i> Tambah',
			array('siswa/createAchievmentNonAkademis?id='.$model->id),
			array('class' => 'btn btn-default pull-right','title'=>'Kelola Data'));
		  ?>        	
          <h6 class="mb-0">Achievment Non Akademis</h6>
          <hr class="horizontal gray-light my-1">
          <div class="table-responsive p-0">
              <div class="dataTable-container">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'karyasiswa-grid',
					'dataProvider'=>$AchievmentNonAkademis->search($model->id),
					'filter'=>$AchievmentNonAkademis,
					'summaryText'=>'',
					'itemsCssClass'=>'table table-flush dataTable-table',
					'columns'=>array(
						array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'name'=>'deskripsi','type'=>'raw',
						  	'value'=>'$data->deskripsi',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'header'=>'Aksi',
							'class'=>'CButtonColumn',
							'template'=>'{update} {delete}',
							'htmlOptions'=>array('width'=>'100px', 
							'style' => 'text-align: center;'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
					),
				)); ?>
              </div>        	
		</div>
	  </div>
	</div>

      <div class="card my-2">
        <div class="card-header pb-0 p-3">
		  <?php 
			echo CHtml::link('<i class="fa fa-plus"></i> Tambah',
			array('siswa/createKendalaAkademis?id='.$model->id),
			array('class' => 'btn btn-default pull-right','title'=>'Kelola Data'));
		  ?>        	
          <h6 class="mb-0">Kendala Akademis</h6>
          <hr class="horizontal gray-light my-1">
          <div class="table-responsive p-0">
              <div class="dataTable-container">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'karyasiswa-grid',
					'dataProvider'=>$KendalaAkademis->search($model->id),
					'filter'=>$KendalaAkademis,
					'summaryText'=>'',
					'itemsCssClass'=>'table table-flush dataTable-table',
					'columns'=>array(
						array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'name'=>'deskripsi','type'=>'raw',
						  	'value'=>'$data->deskripsi',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'header'=>'Aksi',
							'class'=>'CButtonColumn',
							'template'=>'{update} {delete}',
							'htmlOptions'=>array('width'=>'100px', 
							'style' => 'text-align: center;'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
					),
				)); ?>
              </div>        	
		</div>
	  </div>
	</div>

      <div class="card my-2">
        <div class="card-header pb-0 p-3">
		  <?php 
			echo CHtml::link('<i class="fa fa-plus"></i> Tambah',
			array('siswa/createKendalaNonAkademis?id='.$model->id),
			array('class' => 'btn btn-default pull-right','title'=>'Kelola Data'));
		  ?>        	
          <h6 class="mb-0">Kendala Non Akademis</h6>
          <hr class="horizontal gray-light my-1">
          <div class="table-responsive p-0">
              <div class="dataTable-container">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
					'id'=>'karyasiswa-grid',
					'dataProvider'=>$KendalaNonAkademis->search($model->id),
					'filter'=>$KendalaNonAkademis,
					'summaryText'=>'',
					'itemsCssClass'=>'table table-flush dataTable-table',
					'columns'=>array(
						array(
							'header'=>'#',
							'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'name'=>'deskripsi','type'=>'raw',
						  	'value'=>'$data->deskripsi',
							'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),							  	
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
						array(
							'header'=>'Aksi',
							'class'=>'CButtonColumn',
							'template'=>'{update} {delete}',
							'htmlOptions'=>array('width'=>'100px', 
							'style' => 'text-align: center;'),
			  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
						),
					),
				)); ?>
              </div>        	
		</div>
	  </div>
	</div>

	</div>
</div>
</div>