<?php
/* @var $this SopController */
/* @var $model Sop */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'sop_id'); ?>
		<?php echo $form->textField($model,'sop_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_file'); ?>
		<?php echo $form->textField($model,'nama_file',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bidang'); ?>
		<?php echo $form->textField($model,'bidang'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tgl_dokumen'); ?>
		<?php echo $form->textField($model,'tgl_dokumen'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dokumen'); ?>
		<?php echo $form->textField($model,'dokumen',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->