<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */


$this->menu=array(
	array('label'=>'Tambah User', 'url'=>array('create')),
	array('label'=>'Kelola User', 'url'=>array('admin')),
);
?>

<h1>User</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
