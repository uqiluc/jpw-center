<?php
/* @var $this UserController */
/* @var $model User */

$this->menu=array(
	array('label'=>'Daftar User', 'url'=>array('index')),
	array('label'=>'Tambah User', 'url'=>array('create')),
	array('label'=>'Lihat User', 'url'=>array('view', 'id'=>$model->user_id)),
	array('label'=>'Kelola User', 'url'=>array('admin')),
);
?>

<h1>Perbaharui User <?php echo $model->user_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>