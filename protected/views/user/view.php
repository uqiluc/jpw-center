<?php
/* @var $this UserController */
/* @var $model User */

$this->menu=array(
	array('label'=>'Daftar User', 'url'=>array('index')),
	array('label'=>'Tambah User', 'url'=>array('create')),
	array('label'=>'Perbaharui User', 'url'=>array('update', 'id'=>$model->user_id)),
	array('label'=>'Hapus User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Kelola User', 'url'=>array('admin')),
);
?>

<h1>Lihat User #<?php echo $model->user_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'nama_lengkap',
		'bidang',
		'level',
		'username',
		'password',
		'created',
		'updated',
		'status',
	),
)); ?>
