<?php
/* @var $this WidyaiswaraController */
/* @var $model Widyaiswara */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'widyaiswara-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nama_widyaiswara'); ?>
		<?php echo $form->textField($model,'nama_widyaiswara',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nama_widyaiswara'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'nip'); ?>
		<?php echo $form->numberField($model,'nip',array('size'=>30,'maxlength'=>30,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'nip'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'bidangId'); ?>
		<?php echo $form->dropDownList ($model, 'bidangId',
			CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
			array("empty"=>"-- Pilih Bidang --",
				'style'=>'width:100;','class' => 'form-control','required'=>true)
			);
			?>
		<?php echo $form->error($model,'bidangId'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'ttl'); ?>
		<?php echo $form->dateField($model,'ttl',array('size'=>30,'maxlength'=>30,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'ttl'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'kategori'); ?>
		<?php echo $form->dropDownList($model,'kategori', 
								array(
									'Widyaiswara'=>'Widyaiswara',
									'Akademisi'=>'Akademisi',
									'Jafung'=>'Jafung',
									'Praktisi'=>'Praktisi',
								)
								,array("empty"=>"-- Pilih Kategori --",
								'style'=>'width:100;','class' => 'form-control','onchange'=>'kategoriFull()')
							);
		?>
		<?php echo $form->textField($model,'kategori_detail',array('size'=>60,'maxlength'=>255,'class' => 'form-control','style'=>'display:none','placeholder'=>'Isi detail','required'=>true)); ?>
		<?php echo $form->error($model,'kategori_detail'); ?>
		<?php echo $form->dropDownList($model,'wi', 
								array(
									'Utama'=>'WI Utama',
									'Madya'=>'WI Madya',
									'Muda'=>'WI Muda',
								)
								,array("empty"=>"-- Pilih Widyaiswara --",
								'style'=>'width:100;display:none','class' => 'form-control')
							);
		?>
		<?php echo $form->error($model,'wi'); ?>
		<?php echo $form->error($model,'kategori'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pdk_trkhr'); ?>
		<?php echo $form->dropDownList($model,'pdk_trkhr', 
								array(1=>'S3',
									2=>'S2',
									3=>'S1',
									4=>'SMA',
									5=>'SMP',)
								,array(
								// "empty"=>"-- Pilih Pendidikan Terakhir --",
								'style'=>'width:100;','class' => 'form-control','onchange'=>'jurusan()')
							);
		?>
		<?php echo $form->textField($model,'jurusan',array('size'=>60,'maxlength'=>255,'class' => 'form-control','style'=>'display:none','placeholder'=>'Isi jurusan','required'=>true)); ?>
		<?php echo $form->error($model,'jurusan'); ?>
		<?php echo $form->error($model,'pdk_trkhr'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pengampu_mt_plth'); ?>
		<?php echo $form->textArea($model,'pengampu_mt_plth',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'pengampu_mt_plth'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'pengalaman_krj'); ?>
		<?php echo $form->textArea($model,'pengalaman_krj',array('rows'=>6, 'cols'=>50,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'pengalaman_krj'); ?>
	</div>
	
	<div class="form-group">
		<?php echo $form->labelEx($model,'sertifikat'); ?>
		<?php echo $form->fileField($model,'sertifikat',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'sertifikat'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
$(document).ready(function() {
    kategoriFull();
	jurusan();
});

function kategoriFull(){
    var kategori = document.getElementById("Widyaiswara_kategori");
    var kategoriDetail = document.getElementById("Widyaiswara_kategori_detail");
    var wi = document.getElementById("Widyaiswara_wi");
	    if (kategori.value == 'Akademisi' || kategori.value == 'Jafung'){
		    kategoriDetail.style.display = 'block';
		    wi.style.display = 'none';
		    wi.value = '';
			console.log("ada");
		} else if (kategori.value == 'Widyaiswara'){
		    wi.style.display = 'block';
		    kategoriDetail.style.display = 'none';
			kategoriDetail.value = '-';
			console.log("ada");
		} else {
		    kategoriDetail.style.display = 'none';
		    wi.style.display = 'none';
		    wi.value = '';
			kategoriDetail.value = '-';
			console.log("kosong");
		}
}

function jurusan(){
    var pdkTerakhir = document.getElementById("Widyaiswara_pdk_trkhr");
    var jurusan = document.getElementById("Widyaiswara_jurusan");
	    if (pdkTerakhir.value != '5'){
		    jurusan.style.display = 'block';
			console.log("ada");
		} else {
		    jurusan.style.display = 'none';
			jurusan.value = '-';
			console.log("kosong");
		}
}
</script>