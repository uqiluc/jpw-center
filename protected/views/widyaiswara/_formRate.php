<?php
/* @var $this WidyaiswaraController */
/* @var $model Widyaiswara */
/* @var $form CActiveForm */
?>

<div class="col-lg-12">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'widyaiswara-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),	
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'rate'); ?>
		<?php echo $form->dropDownList($model,'rate', 
								array(1=>'1',
									2=>'2',
									3=>'3',
									4=>'4',
									5=>'5',)
								,array("empty"=>"-- Pilih Penilaian --",
								'style'=>'width:100;','class' => 'form-control')
							);
		?>
		<?php echo $form->error($model,'rate'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'review'); ?>
		<?php echo $form->textField($model,'review',array('size'=>60,'maxlength'=>255,'class' => 'form-control')); ?>
		<?php echo $form->error($model,'review'); ?>
	</div>

	<div class="form-group">
     	<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary pull-right')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->