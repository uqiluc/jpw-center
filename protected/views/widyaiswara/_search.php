<?php
/* @var $this WidyaiswaraController */
/* @var $model Widyaiswara */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'wd_id'); ?>
		<?php echo $form->textField($model,'wd_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nama_widyaiswara'); ?>
		<?php echo $form->textField($model,'nama_widyaiswara',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nip'); ?>
		<?php echo $form->textField($model,'nip',array('size'=>30,'maxlength'=>30)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ttl'); ?>
		<?php echo $form->textField($model,'ttl'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kategori'); ?>
		<?php echo $form->textField($model,'kategori',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pdk_trkhr'); ?>
		<?php echo $form->textField($model,'pdk_trkhr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pengampu_mt_plth'); ?>
		<?php echo $form->textField($model,'pengampu_mt_plth',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'pengalaman_krj'); ?>
		<?php echo $form->textArea($model,'pengalaman_krj',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sertifikat'); ?>
		<?php echo $form->textField($model,'sertifikat',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created'); ?>
		<?php echo $form->textField($model,'created'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated'); ?>
		<?php echo $form->textField($model,'updated'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->