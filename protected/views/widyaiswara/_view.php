<?php
/* @var $this WidyaiswaraController */
/* @var $data Widyaiswara */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('wd_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->wd_id), array('view', 'id'=>$data->wd_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nama_widyaiswara')); ?>:</b>
	<?php echo CHtml::encode($data->nama_widyaiswara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nip')); ?>:</b>
	<?php echo CHtml::encode($data->nip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ttl')); ?>:</b>
	<?php echo CHtml::encode($data->ttl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kategori')); ?>:</b>
	<?php echo CHtml::encode($data->kategori); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pdk_trkhr')); ?>:</b>
	<?php echo CHtml::encode($data->pdk_trkhr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pengampu_mt_plth')); ?>:</b>
	<?php echo CHtml::encode($data->pengampu_mt_plth); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pengalaman_krj')); ?>:</b>
	<?php echo CHtml::encode($data->pengalaman_krj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sertifikat')); ?>:</b>
	<?php echo CHtml::encode($data->sertifikat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated')); ?>:</b>
	<?php echo CHtml::encode($data->updated); ?>
	<br />

	*/ ?>

</div>