<?php
/* @var $this WidyaiswaraController */
/* @var $model Widyaiswara */
$this->pageTitle="Widyaiswara";
$this->breadcrumbs=array(
	'Widyaiswara'=>array('admin')
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#widyaiswara-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="container-fluid py-4">
  	<div class="row">
    	<div class="col-12">
          	<div class="card mb-4">
            <div class="card-header pb-0">
            	<div class="pull-right">
					<?php 
					echo CHtml::link('<i class="fa fa-plus"></i> Create',
					array('create'),
					array('class' => 'btn btn-primary','title'=>'Tambah Data Baru'));
					?>
					<?php 
					echo CHtml::link('<i class="fa fa-file-excel-o"></i>',
					array('excel'),
					array('class' => 'btn btn-success','title'=>'Export Ke Excel'));
					?>
	            </div>
	            <h6>Data Widyaiswara</h6>
	        </div>
            <div class="card-body px-0 pt-0 pb-2">
              	<div class="table-responsive p-0">
	              	<div class="dataTable-container">
					<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'widyaiswara-grid',
						'dataProvider'=>$model->search($_GET['type']),
						'filter'=>$model,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'name'=>'nama_widyaiswara','type'=>'raw',
								'value'=>'CHtml::link($data->nama_widyaiswara,array("widyaiswara/view","id"=>$data->wd_id),array("class"=>"badge badge-sm bg-gradient-secondary"))',
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),			
							array(
								'name'=>'bidangId','type'=>'raw','value'=>'$data->Bidang->nama_bidang',
								'filter'=>CHtml::listData(Bidang::model()->findAll('bd_id='.$_GET['type']),'bd_id', 'nama_bidang'),
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),								
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),			
							array(
								'name'=>'nip','value'=>'$data->nip',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),								
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'ttl','value'=>'PelaksanaanPlth::model()->getTgl($data->ttl)',
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),								
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'kategori','value'=>'$data->kategori',
								'filter'=>array(
									'WI'=>'WI',
									'Akademisi'=>'Akademisi',
									'Widyaiswara Utama'=>'Widyaiswara',
									'Jafung'=>'Jafung',
									'Praktisi'=>'Praktisi'
								),
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),								
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'pdk_trkhr','value'=>'Widyaiswara::model()->getPdk($data->pdk_trkhr)',
								'filter'=>array(1=>'S3',2=>'S2',3=>'S1',4=>'SMA',5=>'SMP'),
								'htmlOptions'=>array('class'=>'text-sm font-weight-bold mb-0'),								
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'header'=>'Aksi',
								'template'=>'{update}{delete}',
								'class'=>'CButtonColumn',
								'htmlOptions'=>array('width'=>'150px', 
								'style' => 'text-align: center;'),
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
						),
					)); ?>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>