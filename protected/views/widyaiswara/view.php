<?php
    $this->pageTitle="Widyaiswara";
    $this->breadcrumbs=array(
        'Widyaiswara'=>array('index'),
        'View'
    );
?>

<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-12">
  <div class="card card-body overflow-hidden">
    <div class="row gx-4">
      <div class="col-auto my-auto">
        <div class="h-100">
          <h5 class="mb-1">
            <?php echo $model->nama_widyaiswara;?>
          </h5>
          <p class="mb-0 font-weight-bold text-sm">
            <?php echo $model->nip;?>
          </p>
        </div>
      </div>
	  <div class="col-lg-4 col-md-6 my-sm-auto ms-sm-auto me-sm-0 mx-auto mt-3">
        <div class="pull-right">
		<?php 
		echo CHtml::link('<i class="fa fa-edit"></i> Edit',
		array('update','id'=>$model->wd_id),
		array('class' => 'btn btn-default','title'=>'Edit Data'));
		?>
		<?php 
		echo CHtml::link('<i class="fa fa-th"></i> Data Widyaiswara',
		array('admin'),
		array('class' => 'btn btn-default','title'=>'Kelola Data'));
		?>	
        </div>	  
	  </div>
    </div>
  </div>
  </div>
</div>
</div>
<div class="container-fluid py-2">
<div class="row">
    <div class="col-12 col-xl-6">
      <div class="card h-100">
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Tanggal Lahir</strong> <br> 
            	<?php echo PelaksanaanPlth::model()->getTgl($model->ttl);?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Umur</strong> <br> <?php echo PelaksanaanPlth::model()->getAge($model->ttl);?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Kategori</strong> <br>
            <?php 
              echo $model->kategori;
              if ($model->kategori == 'Widyaiswara') {
                echo " ".$model->wi;
              } else if ($model->kategori == 'Jafung' || $model->kategori == 'Akademisi') {
                echo " ".$model->kategori_detail;
              } else {
                echo "";
              }
            ?>
            </li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Pendidikan Terakhir</strong> <br> 
              <?php 
                echo Widyaiswara::model()->getPdk($model->pdk_trkhr);
                if ($model->pdk_trkhr != 5) {
                  echo " Jurusan ".$model->jurusan;
                }
              ?>
            </li>
          </ul>         
        </div>
	  </div>
	</div>
    <div class="col-12 col-xl-6">
      <div class="card h-100">
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 ps-0 pt-0 text-sm"><strong class="text-dark">Pengampu Mat Pelatihan</strong> <br> <?php echo nl2br($model->pengampu_mt_plth);?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Pengalaman Kerja</strong> <br> <?php echo nl2br($model->pengalaman_krj);?></li>
            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Sertifikat</strong> <br> <?php echo $model->sertifikat;?></li>
          </ul>         
        </div>
      </div>
    </div>
</div>
</div>