<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Display the pdf file using php</title>
</head>

<body>
<?php
$file = $_SERVER['DOCUMENT_ROOT']."/dmspu/showfile/pdf/dummy.pdf";
$filename = 'dummy.pdf';
if (!file_exists($file)) {
    echo "The file $file does not exist<br>";
    echo $_SERVER['DOCUMENT_ROOT'];
    die();
}
header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="' . $filename . '"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file));
header('Accept-Ranges: bytes');

@readfile($file);

?>
<a href="http://www.sanwebtutorials.blogspot.in/">San web corner</a>
</body>
</html>