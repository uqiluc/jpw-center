$('#MUser_level_3').on('change', function () 
	{
		$('#level').show(); 
	}
);

$('#MUser_level_0').on('change', function () 
	{
		$('#level').hide(); 
	}
);

$('#MUser_level_1').on('change', function () 
	{
		$('#level').hide(); 
	}
);

$('#MUser_level_2').on('change', function () 
	{
		$('#level').hide(); 
	}
);

$(document).ready(function() {

  var countKurmod = document.getElementById('countKurmod').innerHTML;
  if (countKurmod > 5)
    countKurmod = 5;
    console.log(countKurmod);

  var countTayang = document.getElementById('countTayang').innerHTML;
  if (countTayang > 3)
    countTayang = 3;

  var countBuku = document.getElementById('countBuku').innerHTML;
  if (countBuku > 3)
    countBuku = 3;

  // var countLatihan = document.getElementById('countLatihan').innerHTML;
  // if (countLatihan > 3)
  //   countLatihan = 3;

  var countSoal = document.getElementById('countSoal').innerHTML;
  if (countSoal > 5)
    countSoal = 5;

  var countPedoman = document.getElementById('countPedoman').innerHTML;
  if (countPedoman > 5)
    countPedoman = 5;
    
  $(".kurmod").slick({
    dots: true,
    autoplay: false,
    // autoplaySpeed: 3000,
    infinite: false,
    slidesToShow: countKurmod,
    // centerMode: true,
    slidesToScroll: 1,
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
    nextArrow: '<button class="slide-arrow next-arrow"></button>'
  });

  $(".bahanTayang").slick({
    dots: true,
    autoplay: false,
    // autoplaySpeed: 3000,
    infinite: false,
    slidesToShow: countTayang,
    // centerMode: true,
    slidesToScroll: 1,
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
    nextArrow: '<button class="slide-arrow next-arrow"></button>'
  });

  $(".buku").slick({
    dots: true,
    autoplay: false,
    // autoplaySpeed: 3000,
    infinite: false,
    slidesToShow: countBuku,
    // centerMode: true,
    slidesToScroll: 1,
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
    nextArrow: '<button class="slide-arrow next-arrow"></button>'
  });

  // $(".latihan").slick({
  //   dots: true,
  //   autoplay: false,
  //   // autoplaySpeed: 3000,
  //   infinite: false,
  //   slidesToShow: countLatihan,
  //   // centerMode: true,
  //   slidesToScroll: 1,
  //   prevArrow: '<button class="slide-arrow prev-arrow"></button>',
  //   nextArrow: '<button class="slide-arrow next-arrow"></button>'
  // });

  $(".soal").slick({
    dots: true,
    autoplay: false,
    // autoplaySpeed: 3000,
    infinite: false,
    slidesToShow: countSoal,
    // centerMode: true,
    slidesToScroll: 1,
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
    nextArrow: '<button class="slide-arrow next-arrow"></button>'
  });

  $(".pedoman").slick({
    dots: true,
    autoplay: false,
    // autoplaySpeed: 3000,
    infinite: false,
    slidesToShow: countPedoman,
    // centerMode: true,
    slidesToScroll: 1,
    prevArrow: '<button class="slide-arrow prev-arrow"></button>',
    nextArrow: '<button class="slide-arrow next-arrow"></button>'
  });

 });