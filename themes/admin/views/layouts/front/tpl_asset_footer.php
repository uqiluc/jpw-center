<!-- jQuery -->
        <script src="<?php echo $baseUrl;?>/frontend/js/jquery-3.0.0.min.js"></script>
        <script src="<?php echo $baseUrl;?>/frontend/js/jquery-migrate-3.0.0.min.js"></script>

        <!-- popper.min -->
        <script src="<?php echo $baseUrl;?>/frontend/js/popper.min.js"></script>

        <!-- bootstrap -->
        <script src="<?php echo $baseUrl;?>/frontend/js/bootstrap.min.js"></script>

        <!-- scrollIt -->
        <script src="<?php echo $baseUrl;?>/frontend/js/scrollIt.min.js"></script>

        <!-- jquery.waypoints.min -->
        <script src="<?php echo $baseUrl;?>/frontend/js/jquery.waypoints.min.js"></script>

        <!-- owl carousel -->
        <script src="<?php echo $baseUrl;?>/frontend/js/owl.carousel.min.js"></script>

        <!-- jquery.magnific-popup js -->
        <script src="<?php echo $baseUrl;?>/frontend/js/jquery.magnific-popup.min.js"></script>

        <!-- stellar js -->
        <script src="<?php echo $baseUrl;?>/frontend/js/jquery.stellar.min.js"></script>

        <!-- isotope.pkgd.min js -->
        <script src="<?php echo $baseUrl;?>/frontend/js/isotope.pkgd.min.js"></script>

        <!-- YouTubePopUp.jquery -->
        <script src="<?php echo $baseUrl;?>/frontend/js/YouTubePopUp.jquery.js"></script>

        <!-- validator js -->
        <script src="<?php echo $baseUrl;?>/frontend/js/validator.js"></script>

        <!-- custom scripts -->
        <script src="<?php echo $baseUrl;?>/frontend/js/scripts.js"></script>

    </body>
</html>