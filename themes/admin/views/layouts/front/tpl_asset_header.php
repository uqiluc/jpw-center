<!doctype html>
<html lang="en">
  <head>
   
    <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $url = Yii::app()->baseUrl."/"; 
    $cs = Yii::app()->getClientScript();
    Yii::app()->clientScript->registerCoreScript('jquery');
    ?>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo Setting::model()->name(); ?></title>
    <meta name="description" content="<?php echo Setting::model()->description(); ?>">
    <meta property="og:description" content="<?php echo Setting::model()->description(); ?>" /> 
    <meta name="author" content="wefay">
    <meta name="keywords" content="<?php echo Setting::model()->keywords(); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">

    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo $baseUrl;?>/frontend/css/plugins.css" />

    <!-- Core Style Css -->
    <link rel="stylesheet" href="<?php echo $baseUrl;?>/frontend/css/style.css" />
    <link rel="stylesheet" href="<?php echo $baseUrl;?>/frontend/css/fontawesome.css" />
    <link rel="stylesheet" href="<?php echo $baseUrl;?>/frontend/css/solid.css" />
    <link rel="stylesheet" href="<?php echo $baseUrl;?>/frontend/css/brands.css" />


    <link rel="shortcut icon" href="<?php echo $baseUrl;?>/frontend/img/icoon.png" />
    <?php echo SMarketing::model()->analytics(); ?>
</head>
<body>
    <div class="loading">
        <div class="text-center middle">
            <div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
        </div>
    </div>