        <!-- =====================================
        ==== Start Footer -->

        <footer class="text-center pos-re">
            <div class="container">
                <!-- Logo -->
                <a class="logo" href="#">
                    <img src="<?php echo $baseUrl;?>/frontend/img/logo-light.png" style="width: 300px" alt="logo">
                </a>
                
                <div class="social">
                    <?php
                    $wa = "https://api.whatsapp.com/send?phone=".Setting::model()->whatsapp()."&text=Hallo, saya pengunjung dari website *www.sukuwi.com*, ingin tahu lebih banyak mengenai produk sumber kusen.."
                    ?>
                    <a href="<?php echo $wa; ?>" target="_blank"><i class="fab fa-whatsapp"></i></a>
                    <a href="<?php echo Setting::model()->facebook(); ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a href="<?php echo Setting::model()->instagram(); ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                </div>

                <p>&copy; 2019 Promo Yamaha Bandung. All Rights Reserved. | Powered by. <a href="https://wefay.com">wefay studio</a></p>
            </div>

            <div class="curve curve-top curve-right"></div>
        </footer>

        <!-- End Footer ====
        ======================================= -->