    <?php
    $baseUrl = Yii::app()->theme->baseUrl; 
    $url = Yii::app()->baseUrl.""; 
    $cs = Yii::app()->getClientScript();
    Yii::app()->clientScript->registerCoreScript('jquery');
    ?>

    <style type="text/css">
      @media only screen and (max-width: 400px) {
        .gogo {
          width:200px;
        }
      }

      @media only screen and (min-width: 499px) {
        .gogo {
          width:300px;
        }
      }
    </style>

		<!-- =====================================
        ==== Start Navbar -->

        <nav class="navbar navbar-expand-lg">
            <div class="container">

            <!-- Logo -->
            <a class="logo" href="#">
                <img src="<?php echo $baseUrl;?>/frontend/img/logo-light.png" class="gogo" alt="logo">
            </a>

              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar"><i class="fas fa-bars"></i></span>
              </button>

              <!-- navbar links -->
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                    <a class="nav-link active" href="#" data-scroll-nav="0">Home</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="1">Tipe Motor</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="2">Kredit Yamaha</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="<?php echo $url;?>/produk">Produk Yamaha</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="4">Daftar Harga</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#" data-scroll-nav="5">Pemesanan</a>
                  </li>                  
                </ul>
              </div>
            </div>
        </nav>

        <!-- End Navbar ====
        ======================================= -->

        <!-- =====================================
        ==== Start Header -->

        <header class="header pos-re slider-fade" data-scroll-index="0">

            <div class="owl-carousel owl-theme">
                <?php
                    foreach (SSlide::sli() as $data) {    
                ?>
                <div class="item bg-img" data-overlay-dark="5" data-background="<?php echo $url;?>/images/slide/<?php echo $data['image'];?>">
                    <div class="text-center v-middle caption mt-30">
                        <h2><?php echo $data['title'];?></h2>
                        <div class="row">
                            <div class="offset-md-1 col-md-10 offset-lg-2 col-lg-8">
                                <p><?php echo $data['text'];?></p>
                            </div>
                        </div>
                        <a href="#" data-scroll-nav="1" class="butn butn-light mt-30">
                            <span>Our Product</span>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="curve curve-white-b curve-bottom"></div>
        </header>

        <!-- End Header ====
        ======================================= -->