<!-- Require the header -->
<?php require_once('tpl_header.php') ?>
<!-- Navbar -->
<?php require_once('tpl_menu.php') ?>
<!-- Require the navigation -->
<?php 
if (Yii::app()->user->record->level == 1){
	require_once('tpl_navigation.php') ;
} else {
	require_once('tpl_navigation_guest.php') ;
}
?>
	<div class="content-wrapper">
	<!-- Main content -->
	<section class="content">
	<!-- Small boxes (Stat box) --> 
      <!-- <ol class="breadcrumb">
          <?php if(isset($this->breadcrumbs)):?>
              <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                  'links'=>$this->breadcrumbs,
                  'homeLink'=>CHtml::link('Dashboard',array('site/index')),
              )); ?>
          <?php endif?>
      </ol> -->
	  <?php echo $content; ?>		
	</section>
	<!-- /.content -->
	</div>
<?php require_once('tpl_credit.php') ?>	
<!-- Require the footer -->
<?php require_once('tpl_footer.php') ?>