<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="wefay studio">
    
    <?php
      $baseUrl = Yii::app()->theme->baseUrl; 
      $cs = Yii::app()->getClientScript();
      Yii::app()->clientScript->registerCoreScript('jquery');
    ?>

    <link rel="stylesheet" href="<?php echo $baseUrl; ?>/bootstrap/css/bootstrap.min.css"> 
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $baseUrl; ?>/ico/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo $baseUrl; ?>/ico/ionicons/css/ionicons.min.css">
    <!-- Theme style -->         
    <link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/AdminLTE.min.css"> 
    <link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/custome.css"> 
    <!-- <link rel="stylesheet" href="<?php echo $baseUrl; ?>/chart/Chart.min.css">  -->
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->         
    <link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/skins/_all-skins.min.css">     
    <script type="text/javascript">
    var clicks = 0;
    function onClick() {
        clicks += 1;
        document.getElementById("clicks").innerHTML = clicks;
    };
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>
    <!-- The fav and touch icons -->
    <link rel="shortcut icon" href="<?php echo $baseUrl;?>/img/logo.png">
    <!-- css slick -->
    <link href="<?php echo $baseUrl;?>/slick/slick.css?v.1.0.0.1" rel="stylesheet">
    <link href="<?php echo $baseUrl;?>/slick/slick-theme.css" rel="stylesheet">
  </head>

<body class="hold-transition skin-custom-light sidebar-mini">