<div class="wrapper">
    <header class="main-header">    <!-- Logo -->
    <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span></a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo Yii::app()->baseUrl; ?>/img/user/<?php echo Yii::app()->user->record->profilImage;?>" class="user-image" alt="User Image">
                <?php echo Yii::app()->user->record->username;?>
                </a>
                <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                <img src="<?php echo Yii::app()->baseUrl; ?>/img/user/<?php echo Yii::app()->user->record->profilImage;?>" class="img-circle" alt="User Image">
                <br><br><b style="color:#fff;text-align: center;"><?php echo Yii::app()->user->record->nama_lengkap;?></b>
                </li>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                <div class="pull-left">
                <?php echo CHtml::link('<i class="fa fa-user"></i> Profil',
                    array('User/view','id'=>Yii::app()->user->id),
                    array('class' => 'btn btn-info'));
                ?>
                </div>
                <div class="pull-right">
                <?php echo CHtml::link('<i class="fa fa-sign-out"></i> Keluar',
                    array('site/logout'),
                    array('class' => 'btn btn-success'));
                ?>
                </div>
                </li>
                </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
                <li>
                </li>
            </ul>
        </div>
        </nav>
    </header>