<?php 
    $tipe = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM goods_type")->queryScalar();
    $merek = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM goods_brand")->queryScalar();
    $stok = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM goods_stock")->queryScalar();
    $distributor = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM supplier")->queryScalar();
    $patient = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM patient")->queryScalar();
?>

<aside class="main-sidebar">
<section class="sidebar">
<!-- Sidebar user panel -->
<div class="user-panel">
  <div class="info">
  <p>JPW Center</p>
  <small>PUSBANGKOM JPW</small>
  </div>
</div>
<br>
  <ul class="sidebar-menu">
    <li class="treeview">
    <?php echo CHtml::link('<i class="fa fa-dashboard"></i> <span>Dashboard</span>',array('site/index')); ?>
    </li>    
    <li class="treeview">
    <?php echo CHtml::link('<i class="fa fa-user"></i> <span>Pegawai</span>',array('pegawai/admin')); ?>
    </li>  
    <li class="treeview">
    <?php echo CHtml::link('<i class="fa fa-graduation-cap"></i> <span>Karyasiswa</span>',array('karyasiswa/admin')); ?>
    </li>   
    <li class="treeview">  
    <?php echo CHtml::link('<i class="fa fa-gears"></i> <span>Pelatihan</span><i class="fa fa-angle-left pull-right"></i>',array('#')); ?>    
    <ul class="treeview-menu">
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Profil Pelatihan</span>',array('profilPelatihan/admin')); ?>
        </li>
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>KURMOD Pelatihan</span>',array('kurmodDetail/admin')); ?>
        </li>        
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Data Pelatihan</span>',array('pelaksanaanPlth/admin')); ?>
        </li>         
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Kalendar Pelatihan</span>',array('pelaksanaanPlth/index')); ?>
        </li> 
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Peserta Pelatihan</span>',array('pesertaPlthn/admin')); ?>
        </li> 
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Perpusatakaan</span>',array('/library')); ?>
        </li>
    </ul>
    </li>
    <li class="treeview">  
    <?php echo CHtml::link('<i class="fa fa-book"></i> <span>Dokumen</span><i class="fa fa-angle-left pull-right"></i>',array('#')); ?>    
    <ul class="treeview-menu">
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Renstra</span>',array('renstra/admin')); ?>
        </li>  
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>SOP</span>',array('sop/admin')); ?>
        </li> 
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Naskah Dinas</span>',array('naskahDinas/admin')); ?>
        </li>         
    </ul>
    </li>
    <li class="treeview">  
    <?php echo CHtml::link('<i class="fa fa-calendar"></i> <span>Kegiatan</span><i class="fa fa-angle-left pull-right"></i>',array('#')); ?>    
    <ul class="treeview-menu">
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Kalendar Kegiatan</span>',array('kegiatan/index')); ?>
        </li> 
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Laporan Kegiatan</span>',array('laporanKgt/admin')); ?>
        </li> 
    </ul>
    </li>    
    <li class="treeview">  
    <?php echo CHtml::link('<i class="fa fa-book"></i> <span>Media</span><i class="fa fa-angle-left pull-right"></i>',array('#')); ?>    
    <ul class="treeview-menu">        
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Media</span>',array('media/admin')); ?>
        </li> 
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Bahan Tayang</span>',array('bahanTayang/admin')); ?>
        </li>
    </ul>
    </li>
    <li class="treeview">
    <?php echo CHtml::link('<i class="fa fa-graduation-cap"></i> <span>Widyaiswara</span>',array('widyaiswara/admin')); ?>
    </li>   
<!--     <li class="treeview">
    <?php echo CHtml::link('<i class="fa fa-book"></i> <span>Bidang</span>',array('bidang/admin')); ?>
    </li> 
 -->  </ul>
</section>
<!-- /.sidebar -->
</aside>