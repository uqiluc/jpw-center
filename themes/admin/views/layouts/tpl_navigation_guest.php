<?php 
    $tipe = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM goods_type")->queryScalar();
    $merek = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM goods_brand")->queryScalar();
    $stok = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM goods_stock")->queryScalar();
    $distributor = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM supplier")->queryScalar();
    $patient = 1;//Yii::app()->db->createCommand("SELECT COUNT(id) FROM patient")->queryScalar();
?>

<aside class="main-sidebar">
<section class="sidebar">
<!-- Sidebar user panel -->
<div class="user-panel">
  <div class="info">
  <p>JPW Center</p>
  <small>PUSBANGKOM JPW</small>
  </div>
</div>
<br>
  <ul class="sidebar-menu">
    <li class="treeview">
    <?php echo CHtml::link('<i class="fa fa-dashboard"></i> <span>Dashboard</span>',array('site/index')); ?>
    </li>    
    <li class="treeview">  
    <?php echo CHtml::link('<i class="fa fa-gears"></i> <span>Pelatihan</span><i class="fa fa-angle-left pull-right"></i>',array('#')); ?>    
    <ul class="treeview-menu">
 
        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>KURMOD Pelatihan</span>',array('kurmodDetail/admin')); ?>
        </li>        

        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-circle-o"></i> <span>Kalendar Pelatihan</span>',array('pelaksanaanPlth/index')); ?>
        </li> 

    </ul>
    </li>

        <li class="treeview">
        <?php echo CHtml::link('<i class="fa fa-calendar"></i> <span>Kalendar Kegiatan</span>',array('kegiatan/index')); ?>
        </li> 

  </ul>
</section>
<!-- /.sidebar -->
</aside>