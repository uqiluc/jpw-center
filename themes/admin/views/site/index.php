<?php
  $kurmod=Yii::app()->db->createCommand("SELECT COUNT(kmd_id) FROM kurmod_detail")->queryScalar();
  $pelatihan=Yii::app()->db->createCommand("SELECT COUNT(pplth_id) FROM pelaksanaan_plth")->queryScalar();
  $kegiatan=Yii::app()->db->createCommand("SELECT COUNT(kg_id) FROM kegiatan")->queryScalar();

  $baseUrl = Yii::app()->theme->baseUrl; 
  $url = Yii::app()->baseUrl.""; 
  $cs = Yii::app()->getClientScript();
  Yii::app()->clientScript->registerCoreScript('jquery');
  date_default_timezone_set('Asia/Jakarta');

  if (isset($_GET['year'])){
    $year = $_GET['year'];
  } else {
    $year = date('Y');
  }
?>
<div class="row">
<div class="col-lg-8">
  <div class="pull-right">
    <?php echo date('D, d M Y');?>
  </div>
  <b style="font-size: 20px">Good Morning,</b><br>
    <?php if(Yii::app()->user->isAdministrator())
          {
            echo Yii::app()->user->record->nama_lengkap;
          }

          if(Yii::app()->user->isAdminbidang())
          {
            echo Yii::app()->user->record->nama_lengkap;
          }

          if(Yii::app()->user->isVisitor())
          {
            echo Yii::app()->user->record->nama_lengkap;
          }
    ?>

  <hr>
  
  <a href="http://project.kiprama.id/dmspu/kurmodDetail/admin" class="col-lg-4 col-xs-4">
    <div class="small-box bg-aqua" style="background-color: #19314c !important;border-radius: 10px;">
      <div class="inner">
        <h3><?php echo $kurmod;?></h3>
        <p>Kurmod</p>
      </div>
      <div class="icon">
        <img src="<?php echo Yii::app()->baseUrl;?>/img/kurmod.png" style="width: 115px;margin-top: -75px;">
      </div>  
    </div>
  </a>
  <a href="http://project.kiprama.id/dmspu/pelaksanaanPlth/admin" class="col-lg-4 col-xs-4">
    <div class="small-box bg-aqua" style="background-color: #ff9800 !important;border-radius: 10px;">
      <div class="inner">
        <h3><?php echo $pelatihan;?></h3>
        <p>Pelatihan</p>
      </div>
      <div class="icon">
        <img src="<?php echo Yii::app()->baseUrl;?>/img/pelatihan.png" style="width: 110px;margin-top: -37px;">
      </div>  
    </div>
  </a>  
  <a href="http://project.kiprama.id/dmspu/kegiatan/index" class="col-lg-4 col-xs-4">
    <div class="small-box bg-aqua" style="background-color: #19314c !important;border-radius: 10px;">
      <div class="inner">
        <h3><?php echo $kegiatan;?></h3>
        <p>Kegiatan</p>
      </div>
      <div class="icon">
        <img src="<?php echo Yii::app()->baseUrl;?>/img/kegiatan.png" style="width: 120px;margin-top: -60px;">
      </div>  
    </div>
  </a>
  <div class="col-lg-12">  
  <select id="year" name="year" class="form-control pull-right" style="width: 80px;font-size: 12px;font-weight: bold;padding: 5px;height: 30px;margin-bottom: 10px;" onchange="location = this.value;">
  <?php for ($x=2015;$x<=2030;$x++){?>
    <option value="http://project.kiprama.id/dmspu/site/index?year=<?php echo $x;?>" 
      <?php if ($year==$x){
        echo "selected";
      }?>>
      <?php echo $x;?>
    </option>
  <?php } ?>
  </select>
  </div>

  <div class="col-lg-7 col-xs-12">
    <div class="box">
      <div class="box-header">
        <canvas id="pelatihan" width="100" height="50"></canvas>
      </div>
    </div>
  </div>

  <div class="col-lg-5 col-xs-12">
    <div class="box">
      <div class="box-header">
        <canvas id="karyasiswa" width="100" height="50"></canvas>
      </div>
    </div>
  </div>

  <div class="col-lg-7 col-xs-12">
    <div class="box">
      <div class="box-header">
        <canvas id="peserta" width="100" height="50"></canvas>
      </div>
    </div>
  </div>

  <div class="col-lg-5 col-xs-12">
    <div class="box">
      <div class="box-header">
        <canvas id="pegawai" width="100" height="50"></canvas>
      </div>
    </div>
  </div>
  
</div>
<div class="col-lg-4" style="background-color: #ebf4ff;padding: 20px;height: 100vh;border-radius: 20px;">
  <button class="pu-fill" style="background-color: #19314c;color: #fff;border: none;border-radius: 5px;    padding: 2px 14px;">Home</button>
  <!-- <button class="pu-fill" style="background-color: #fff0;color: #333;border: none;border-radius: 5px;    padding: 2px 14px;">Search</button> -->
  <br>
  <br>
  <center><b><?php echo date('M Y');?></b></center>

  <div style="display: flex;margin: 10px 30px 30px 30px;">
    <?php 
    $now = date('Y-m-d');

    $dateNow1=date_create($now);
    date_add($dateNow1,date_interval_create_from_date_string("-3 days"));
    $dateNow2=date_create($now);
    date_add($dateNow2,date_interval_create_from_date_string("-2 days"));
    $dateNow3=date_create($now);
    date_add($dateNow3,date_interval_create_from_date_string("-1 days"));
    $dateNow4=date_create($now);
    date_add($dateNow4,date_interval_create_from_date_string("1 days"));
    $dateNow5=date_create($now);
    date_add($dateNow5,date_interval_create_from_date_string("2 days"));
    $dateNow6=date_create($now);
    date_add($dateNow6,date_interval_create_from_date_string("3 days"));    
    ?>
    <div class="no-today">
      <b><?php echo date_format($dateNow1,"d");;?></b><br>
      <?php echo substr(date_format($dateNow1,"D"),0,1);;?>
    </div>
    <div class="no-today">
      <b><?php echo date_format($dateNow2,"d");;?></b><br>
      <?php echo substr(date_format($dateNow2,"D"),0,1);;?>
    </div>
    <div class="no-today">
      <b><?php echo date_format($dateNow3,"d");;?></b><br>
      <?php echo substr(date_format($dateNow3,"D"),0,1);;?>
    </div>

    <div class="today">
      <b><?php echo date('d');?></b><br>
      <?php echo substr(date('D'),0,1);?>
    </div>
    <div class="no-today">
      <b><?php echo date_format($dateNow4,"d");;?></b><br>
      <?php echo substr(date_format($dateNow4,"D"),0,1);;?>
    </div>
    <div class="no-today">
      <b><?php echo date_format($dateNow5,"d");;?></b><br>
      <?php echo substr(date_format($dateNow5,"D"),0,1);;?>
    </div>
    <div class="no-today">
      <b><?php echo date_format($dateNow6,"d");;?></b><br>
      <?php echo substr(date_format($dateNow6,"D"),0,1);;?>
    </div>

  </div>

  <b>Schedule</b>
  <div class="pull-right"><a href="<?php echo $url;?>/pelaksanaanPlth/index">View All</a></div>
      <?php $ID = Yii::app()->db->createCommand("SELECT pplth_id FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); ?>
      <?php $nmPelatihan = Yii::app()->db->createCommand("SELECT nm_pelatihan FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); ?>
      <?php $bidang = Yii::app()->db->createCommand("SELECT bidang FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); ?>
      <?php if ($nmPelatihan != '') { ?>
  <div class="SchedulueNow">
      <div class="labelSchedul"></div>
      <div style="padding: 10px;">
      <?php 
        echo "<b><a href='".$url."/pelaksanaanPlth/viewDetail/".$ID."'>".$nmPelatihan."</a></b><br>";
        echo "Bidang ".Yii::app()->db->createCommand("SELECT nama_bidang FROM bidang WHERE bd_id = $bidang")->queryScalar()."<br><br>"; 
        echo Yii::app()->db->createCommand("SELECT lokasi_plthn FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); 
      ?></div>
  </div>
      <?php } ?>
  <hr>

  <div class="table table-responsive" style="font-size: 10px">
  <?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'pelaksanaan-plth-grid',
    'dataProvider'=>$model->getDashboard($year),
    'itemsCssClass'=>'table table-striped table-bordered table-hover',
    'summaryText'=>'',
    'enablePagination' => false,    
    'columns'=>array(
      array(
        'header'=>'No',
        'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
        'htmlOptions'=>array('width'=>'10px', 
        'style' => 'text-align: center')
      ),
      array('header'=>'Pelatihan','value'=>'$data->nm_pelatihan'),
      array('header'=>'Waktu','type'=>'raw','value'=>'PelaksanaanPlth::model()->getWaktu($data->tgl_plaksana,$data->tgl_akhir)'),
      array('header'=>'Lokasi','value'=>'$data->lokasi_plthn'),
      array('header'=>'Status','type'=>'raw','value'=>'PelaksanaanPlth::model()->status($data->sts_platihan)'),
    ),
  )); ?>
  </div>
</div>
</div>

<script type="text/javascript">
var ctx = document.getElementById("pelatihan");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
        datasets: [{
                label: 'Pelatihan',
                data: [
                  <?php
                  for ($i=1; $i <= 12; $i++) { 
                      echo PelaksanaanPlth::model()->monthReport($year,$i).",";
                  }
                  ?>
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
    },
    options: {
        scales: {
            yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        min: 0,
                        stepSize: 1,
                    }
                }]
        },
        responsive: true,
        title: {
            display: true,
            text: 'Capaian Pelatihan <?php echo $year;?>'
        },          
    }
});

var ctx = document.getElementById("pegawai");
var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: [
                  <?php
                    echo Pegawai::model()->reportPns().",";
                    echo Pegawai::model()->reportNonPns().",";
                    echo Pegawai::model()->reportKI().","
                  ?>
                ],
                backgroundColor: [
                    "#F7464A",
                    "#46BFBD",
                    "#FDB45C"
                ],
            }],
            labels: [
                "PNS",
                "Non PNS",
                "KI"
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'right',
            },
            title: {
                display: true,
                text: 'Data Pegawai'
            },
        }
});

var ctx = document.getElementById("karyasiswa");
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [
                  <?php
                    echo Karyasiswa::model()->reportLulus().",";
                    echo Karyasiswa::model()->reportGoing().",";
                    echo Karyasiswa::model()->reportBermasalah().",";
                  ?>
            ],
            backgroundColor: [
                "#F7464A",
                "#46BFBD",
                "#FDB45C",
            ],
            label: 'Dataset 1'
        }],
        labels: [
            "Alumni",
            "On Going",
            "Bermasalah",
        ]
    },
    options: {
        responsive: true,
        legend: {
            position: 'right',
        },
        title: {
            display: true,
            text: 'Karyasiswa'
        },
        animation: {
            animateScale: true,
            animateRotate: true
        }
    }
});

// Status Chart
var ctx = document.getElementById("peserta");
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
        datasets: [{
        label: 'Peserta Pelatihan',
        data: [
          <?php
          for ($i=1; $i <= 12; $i++) { 
              echo PesertaPlthn::model()->monthReport($year,$i).",";
          }
          ?>
        ],
        backgroundColor: '#ff98005c',
        borderColor: '#ff9800',
        borderWidth: 1
    }]
    },
    options: {
        elements: {
            point:{
                radius: 2
            }
        },      
        responsive: true,
        title:{
            display:true,
            text:'Capaian Peserta Pelatihan <?php echo $year;?>'
        },
        hover: {
            mode: 'dataset'
        },
    }
});

</script>