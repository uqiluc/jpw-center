<?php
/* @var $this KurmodDetailController */
/* @var $model KurmodDetail */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#kurmod-detail-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="row">
    <div class="col-lg-12">
	<div class="box box">
    <div class="box-header">
		<h3 class="box-title"><i class="fa fa-book"></i> Perpustakaan</h3>
    </div>
    <div class="box-body">
	<ul class="nav nav-tabs">
	  <li class="active">
	  	<a data-toggle="tab" href="#profile">Profile Pelatihan</a>
	  </li>
	  <li>
	  	<a data-toggle="tab" href="#kurmod">Kurikulum dan modul</a>
	  </li>
	</ul>
	<div class="tab-content">
	  	<div id="profile" class="tab-pane fade in active">
	   	<div class="table table-responsive">
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'logProfile-grid',
			'dataProvider'=>$LogProfile->search(),
			'filter'=>$LogProfile,
			'itemsCssClass'=>'table table-striped table-bordered table-hover',		
			'columns'=>array(
				array(
					'header'=>'No',
					'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
					'htmlOptions'=>array('width'=>'10px', 
					'style' => 'text-align: center; background-color: #3c8dbc; color:#ffffff;')
				),
				array(
					'header'=>'Profile','type'=>'raw',
					'value'=>'CHtml::link(" ",array("profilPelatihan/view","id"=>$data->profileId),array("class"=>"fa fa-search btn btn-info"))',
				),
				'initialName',
				'nameChange',
				'createTime',
				array(
					'name'=>'createdBy','type'=>'raw',
					'value'=>'$data->Users->nama_lengkap'
				)
			),
		)); 
		?>
		</div>
		</div>
	  	<div id="kurmod" class="tab-pane fade in">
	   	<div class="table table-responsive">
  		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'logKurmod-grid',
			'dataProvider'=>$LogKurmod->search(),
			'filter'=>$LogKurmod,
			'itemsCssClass'=>'table table-striped table-bordered table-hover',		
			'columns'=>array(
				array(
					'header'=>'No',
					'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
					'htmlOptions'=>array('width'=>'10px', 
					'style' => 'text-align: center; background-color: #3c8dbc; color:#ffffff;')
				),
				array(
					'header'=>'Kurmod','type'=>'raw',
					'value'=>'CHtml::link(" ",array("kurmodDetail/view","id"=>$data->kurmodId),array("class"=>"fa fa-search btn btn-info"))',
				),
				array(
					'name'=>'profileId','type'=>'raw',
					'value'=>'CHtml::link($data->Profile->nama_pelatihan,array("profilPelatihan/view","id"=>$data->profileId))',
				),
				'initialName',
				'nameChange',
				'initialFile',
				'fileChange',
				'createTime',
				array(
					'name'=>'createdBy','type'=>'raw',
					'value'=>'$data->Users->nama_lengkap'
				)
				),
			)); 
		?>
	  	</div>
	  	</div>
	</div>
	</div>
	</div>

	</div>
	</div>
</div>