<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
    'Login',
);

  $baseUrl = Yii::app()->theme->baseUrl; 
  $cs = Yii::app()->getClientScript();
  Yii::app()->clientScript->registerCoreScript('jquery');
?>

<body class="notice pace-done">
<div style="padding-top: 80px;">
    <div class="row">

        <div class="form-box" id="login-box" style="background:bg-white">
            
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableClientValidation'=>true,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            )); ?>
                <div class="header">
                    <img src="<?php echo $baseUrl;?>/frontend/img/logo.png" width="200px">
                </div>
                 <div class="body bg-gray">

                    <div class="form-group inputwrapper">
                    <div class="input-group">
                     <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user"></i></span>
                        <?php echo $form->textField($model,'username', array('class' => 'form-control span4', 'placeholder'=>'Enter your username')); ?>
                    </div>
                    <?php echo $form->error($model,'username', array('class'=>'label label-danger pull-right')); ?>
                    </div>

                    <div class="form-group inputwrapper">
                    <div class="input-group">
                     <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i></span>
                        <?php echo $form->passwordField($model,'password', array('class' => 'form-control span4','placeholder'=>'Enter your Password')); ?>
                     <span class="input-group-addon" onclick="myFunction()" style="cursor:pointer;"><i class="fa fa-eye"></i></span>
                    </div>          
                    <?php echo $form->error($model,'password', array('class'=>'label label-danger pull-right')); ?>
                    </div>

                    <?php echo $form->checkBox($model,'rememberMe'); ?>
                    <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                    <span class="text-inverse">Remember me</span>
                    <?php echo $form->error($model,'rememberMe'); ?>

                </div>
                <hr>
                <div class="footer">                                                               
                    <?php echo CHtml::submitButton('Sign in', array('class' =>'btn btn-primary btn-block')); ?>
                   <?php $this->endWidget(); ?>                
                </div>
            </form>
            </div>
            </div>
            </div>
</body>

<script>
function myFunction() {
    var x = document.getElementById("LoginForm_password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>