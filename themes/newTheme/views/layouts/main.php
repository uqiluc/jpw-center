<!-- Require the header -->
<?php require_once('tpl_header.php') ?>
<!-- Navbar -->
<?php require_once('tpl_menu.php') ?>
<!-- Require the navigation -->
<main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
<?php require_once('tpl_navigation.php') ?>
<?php echo $content; ?>		
<?php require_once('tpl_credit.php') ?>
</main>	
<!-- Require the footer -->
<?php require_once('tpl_footer.php') ?>