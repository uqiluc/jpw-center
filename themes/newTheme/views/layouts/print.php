<!DOCTYPE html>
<html lang="en">
  <head>

    <?php
      $baseUrl = Yii::app()->theme->baseUrl; 
      $cs = Yii::app()->getClientScript();
      Yii::app()->clientScript->registerCoreScript('jquery');
    ?>

    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    
    <style type="text/css">
      .img {
        position: relative;
        left: 23px;
        z-index: -1;
      }
      table{
        border-collapse: collapse;
      }
      table tr td{
        border-color: #000;
        padding : 5px;
      }
      body{
        font-size: 8px;
        font-family: Arial;
      }
    </style>
  </head>
  
<body style="margin: 0 10px 0 10px">
    <div>
        <table class="table" width="100%">
        <tr>
            <td style="vertical-align: middle;border-top-color: #fff" width="25%"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/podomoro.png" alt="User Image" width="250px"></td>
            <td style="font-size: 20px;text-align: center; vertical-align: middle;border-top-color: #fff" width="0%"><b>TINDAKAN PERBAIKAN DAN <br>PENCEGAHAN (PTPP)</b></td>
            <td style="vertical-align: middle;border-top-color: #fff" width="25%"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/img/feslink.png" alt="User Image" width="250px"></td>
        </tr>            
        </table>
        <HR style="border-width: 3px;border-color: #333;margin: 0 0 20px 0;">

			  <div style="font-size: 12px">
        <?php echo $content; ?>
        </div>
    </div>
  </body>
</html>


