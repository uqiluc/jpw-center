<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title><?php echo CHtml::encode($this->pageTitle); ?></title>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="google-signin-client_id" content="402871662388-7plvau9qsl2t2q0o5epvf3ed31mfe92m.apps.googleusercontent.com">

      <?php
          $baseUrl = Yii::app()->theme->baseUrl; 
          $cs = Yii::app()->getClientScript();
          Yii::app()->clientScript->registerCoreScript('jquery');
      ?>

      <!-- The fav and touch icons -->
      <link rel="shortcut icon" href="<?php echo $baseUrl;?>/img/logo.png">
      <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $baseUrl;?>/img/logo.png">
    
      <!--     Fonts and icons     -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
      <!-- Nucleo Icons -->
      <link href="<?php echo $baseUrl; ?>/css/nucleo-icons.css" rel="stylesheet" />
      <link href="<?php echo $baseUrl; ?>/css/nucleo-svg.css" rel="stylesheet" />
      <!-- Font Awesome Icons -->
      <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
      <link href="<?php echo $baseUrl; ?>/css/nucleo-svg.css" rel="stylesheet" />
      <!-- CSS Files -->
      <link id="pagestyle" href="<?php echo $baseUrl; ?>/css/soft-ui-dashboard.css?v=1.0.3" rel="stylesheet" />
      <script src="https://apis.google.com/js/platform.js" async defer></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.bundle.js"></script>

  <style>
    .g-signin2{
  width: 100%;
}

.g-signin2 > div{
  margin: 0 auto;
}
    </style>
  </head>

<body class="">
  <?php echo $content; ?>

  <script src="<?php echo $baseUrl; ?>/js/core/popper.min.js"></script>
  <script src="<?php echo $baseUrl; ?>/js/core/bootstrap.min.js"></script>
  <script src="<?php echo $baseUrl; ?>/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="<?php echo $baseUrl; ?>/js/plugins/smooth-scrollbar.min.js"></script>
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo $baseUrl; ?>/js/soft-ui-dashboard.min.js?v=1.0.3"></script>
</body>

</html>