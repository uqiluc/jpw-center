
  <script src="<?php echo $baseUrl; ?>/js/core/popper.min.js"></script>
  <script src="<?php echo $baseUrl; ?>/js/core/bootstrap.min.js"></script>
  <script src="<?php echo $baseUrl; ?>/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="<?php echo $baseUrl; ?>/js/plugins/smooth-scrollbar.min.js"></script>
  <!-- Control Center for Soft Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo $baseUrl; ?>/js/soft-ui-dashboard.min.js?v=1.0.3"></script>
</body>

</html>