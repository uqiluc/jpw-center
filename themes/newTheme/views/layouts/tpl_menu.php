<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 " id="sidenav-main">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0" href="https://demos.creative-tim.com/soft-ui-dashboard/pages/dashboard.html" target="_blank">
        <img src="<?php echo $baseUrl; ?>/img/logo.png" class="navbar-brand-img h-100" alt="main_logo">
      </a>
    </div>
    <hr class="horizontal dark mt-3">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100 h-100" id="sidenav-collapse-main">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Dashboard' ? 'active' : '';?>" href="<?php echo $url;?>/site/index">
            <div class="icon <?php echo $this->pageTitle=='Dashboard' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-home"></i>
            </div>
            <span class="nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Pegawai' ? 'active' : '';?>" href="<?php echo $url;?>/pegawai/admin">
            <div class="icon <?php echo $this->pageTitle=='Pegawai' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-user"></i>
            </div>
            <span class="nav-link-text ms-1">Pegawai</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Karyasiswa' ? 'active' : '';?>" href="<?php echo $url;?>/karyasiswa">
            <div class="icon <?php echo $this->pageTitle=='Karyasiswa' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-graduation-cap"></i>
            </div>
            <span class="nav-link-text ms-1">Karyasiswa</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Pelatihan' ? 'active' : '';?>" href="<?php echo $url;?>/pelatihan">
            <div class="icon <?php echo $this->pageTitle=='Pelatihan' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-gears"></i>
            </div>
            <span class="nav-link-text ms-1">Kurikulum & Modul</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Jadwal Pelatihan' ? 'active' : '';?>" href="<?php echo $url;?>/pelaksanaanPlth/admin">
            <div class="icon <?php echo $this->pageTitle=='Jadwal Pelatihan' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-book"></i>
            </div>
            <span class="nav-link-text ms-1">Pelatihan</span>
          </a>
        </li>
       <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Widyaiswara' ? 'active' : '';?>" href="<?php echo $url;?>/widyaiswara/">
            <div class="icon <?php echo $this->pageTitle=='Widyaiswara' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-user"></i>
            </div>
            <span class="nav-link-text ms-1">Widyaiswara</span>
          </a>
        </li>
       <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Program & Evaluasi' ? 'active' : '';?>" href="<?php echo $url;?>/programevaluasi">
            <div class="icon <?php echo $this->pageTitle=='Program & Evaluasi' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-book"></i>
            </div>
            <span class="nav-link-text ms-1">Program & Evaluasi</span>
          </a>
        </li>        
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Kerjasama' ? 'active' : '';?>" href="<?php echo $url;?>/kerjasama">
            <div class="icon <?php echo $this->pageTitle=='Kerjasama' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-link"></i>
            </div>
            <span class="nav-link-text ms-1">Kerjasama</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='LSP' ? 'active' : '';?>" href="<?php echo $url;?>/lsp">
            <div class="icon <?php echo $this->pageTitle=='LSP' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-file"></i>
            </div>
            <span class="nav-link-text ms-1">LSP</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Dokumen' ? 'active' : '';?>" href="<?php echo $url;?>/documents">
            <div class="icon <?php echo $this->pageTitle=='Dokumen' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-book"></i>
            </div>
            <span class="nav-link-text ms-1">Dokumen</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Kegiatan' ? 'active' : '';?>" href="<?php echo $url;?>/kalendar">
            <div class="icon <?php echo $this->pageTitle=='Kegiatan' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-calendar"></i>
            </div>
            <span class="nav-link-text ms-1">Kalendar</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $this->pageTitle=='Media' ? 'active' : '';?>" href="<?php echo $url;?>/media">
            <div class="icon <?php echo $this->pageTitle=='Media' ? 'icon-shape' : '';?> icon-sm shadow border-radius-md bg-white text-center me-2 d-flex align-items-center justify-content-center">
              <i class="fa fa-folder"></i>
            </div>
            <span class="nav-link-text ms-1">Media</span>
          </a>
        </li>        
      </ul>
    </div>
  </aside>