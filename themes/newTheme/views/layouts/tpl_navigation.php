<!-- Navbar -->
<nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
  <div class="container-fluid py-1 px-3">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb bg-transparent mb-0 pb-0 pt-1 px-0 me-sm-6 me-5">
        <?php if(isset($this->breadcrumbs)):?>
              <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                  'links'=>$this->breadcrumbs,
                  'homeLink'=>CHtml::link('Dashboard',array('site/index')),
              )); ?>
        <?php endif?>
      </ol>
      <h6 class="font-weight-bolder mb-0"><?php echo $this->pageTitle;?></h6>
    </nav>
    <div class="collapse navbar-collapse mt-sm-0 mt-2 me-md-0 me-sm-4" id="navbar">
      <div class="ms-md-auto pe-md-3 d-flex align-items-center">
      </div>
      <ul class="navbar-nav  justify-content-end">
        <li class="nav-item d-flex align-items-center">
          <a href="#" class="nav-link text-body font-weight-bold px-0" onclick="signOut();">
            <img src="<?php echo Yii::app()->baseUrl; ?>/img/user/<?php echo Yii::app()->user->record->profilImage;?>" class="me-sm-1" alt="User Image" style="width: 25px;border-radius: 100%;">
            <span class="d-sm-inline d-none"><?php echo Yii::app()->user->record->username;?> (Logout)</span>
          </a>
        </li>
        <li class="nav-item d-xl-none ps-3 d-flex align-items-center">
          <a href="javascript:;" class="nav-link text-body p-0" id="iconNavbarSidenav">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </a>
        </li>

      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar -->
<script>
  function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      window.location.href="<?php echo $url;?>/logout";
    });
  }
</script>