  <!-- CORE Data -->
  <!-- END -->
  <?php 
        $this->breadcrumbs=array(
            'index',
        );

        $first = 1;//Yii::app()->db->createCommand("SELECT COUNT(id_dprt) FROM master_departement")->queryScalar();
        $second = 2;//Yii::app()->db->createCommand("SELECT COUNT(id_mauditor) FROM master_auditor")->queryScalar();
        $third = 3;//Yii::app()->db->createCommand("SELECT COUNT(id_ptpp) FROM tp_ptpp")->queryScalar();
  ?>
  <div class="row">
    <div class="col-lg-3 col-xs-6">
    <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3><?php echo $first;?></h3>
          <p>Penyimpangan</p>
        </div>
        <div class="icon">
          <i class="fa fa-arrow-down"></i>
        </div>
        <?php 
        echo CHtml::link('Info Lengkap <i class="fa fa-arrow-circle-right"></i>',
        array('barangMasuk/kelola'),
        array('class' => 'small-box-footer'));
        ?>        
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
    <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo $second;?></h3>
          <p>Departemen</p>
        </div>
        <div class="icon">
          <i class="fa fa-briefcase"></i>
        </div>
        <?php 
        echo CHtml::link('Info Lengkap <i class="fa fa-arrow-circle-right"></i>',
        array('BarangStok/kelola'),
        array('class' => 'small-box-footer'));
        ?>  
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
    <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo $third;?></h3>
          <p>Auditor</p>
        </div>
        <div class="icon">
          <i class="fa fa-arrow-up"></i>
        </div>
        <?php 
        echo CHtml::link('Info Lengkap <i class="fa fa-arrow-circle-right"></i>',
        array('barangKeluar/kelola'),
        array('class' => 'small-box-footer'));
        ?>  
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
    <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3><?php echo $third;?></h3>
          <p>PTPP</p>
        </div>
        <div class="icon">
          <i class="fa fa-file"></i>
        </div>
        <?php 
        echo CHtml::link('Info Lengkap <i class="fa fa-arrow-circle-right"></i>',
        array('permintaanBarang/kelola'),
        array('class' => 'small-box-footer'));
        ?>  
        </div>
    </div>

  </div>

  <div class="row">
      <div class="col-lg-12">
        <section class="panel">

          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

        </section>
      </div>  
  </div>

  <script type="text/javascript">

    $(function () {
        Highcharts.chart('container', {
            chart: {
                type: 'areaspline'
            },
            title: {
                text: 'Laporan Temuan Tahun <?php echo date('Y')?>'
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            xAxis: {
                categories: [
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember',
                ],
                        plotBands: [{ // visualize the weekend
                            from: 200.5,
                            to: 100.5,
                            color: 'rgba(68, 170, 213, .2)'
                        }]
                    },
                    yAxis: {
                        title: {
                            text: 'Jumlah Data'
                        }
                    },
                    tooltip: {
                        shared: true,
                        valueSuffix: ' Data'
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        areaspline: {
                            fillOpacity: 0.5
                        }
                    },
                    series: [
                    {
                        name: 'Barang Masuk',
                        data: [
                        <?php
                        for ($i=1; $i <= 12; $i++) { 
                            // echo barangMasukDetail::model()->monthReport($i).",";
                            echo $i.",";
                        }
                        ?>
                        ]
                    },

                    {
                        name: 'Barang Keluar',
                        data: [
                        <?php
                        for ($i=1; $i <= 12; $i++) { 
                            echo $i.",";
                        }
                        ?>
                        ]
                    }


                    ]
                });
        });
    </script>                     