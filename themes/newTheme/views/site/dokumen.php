<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Dokumen";
$this->breadcrumbs=array(
	'Dokumen'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Renstra</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/viewDokumen?id=renstra" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>SOP</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/viewDokumen?id=sop" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>
        
        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Peraturan Perundangan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/viewDokumen?id=perundangan" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>BMN</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/bmn/admin" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Laporan Kegiatan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/viewDokumen?id=laporan" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

	  </div>
</div>