<?php
$this->pageTitle="Dashboard";
date_default_timezone_set('Asia/Jakarta');
$kurmod=Yii::app()->db->createCommand("SELECT COUNT(kmd_id) FROM kurmod_detail")->queryScalar();
$pelatihan=Yii::app()->db->createCommand("SELECT COUNT(pplth_id) FROM pelaksanaan_plth")->queryScalar();
$kegiatan=Yii::app()->db->createCommand("SELECT COUNT(kg_id) FROM kegiatan")->queryScalar();
$pegawai=Yii::app()->db->createCommand("SELECT COUNT(pg_id) FROM pegawai")->queryScalar();

$baseUrl = Yii::app()->theme->baseUrl; 
$url = Yii::app()->baseUrl.""; 
$cs = Yii::app()->getClientScript();
Yii::app()->clientScript->registerCoreScript('jquery');

$dateNow = date('Y-m');

if (isset($_GET['year'])){
  $year = $_GET['year'];
} else {
  $year = date('Y');
}
?>
<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
  // Enable pusher logging - don't include this in production
  Pusher.logToConsole = true;

  var pusher = new Pusher('1d95ff554dc388ad689c', {
    cluster: 'ap1'
  });

  var channel = pusher.subscribe('my-channel');
  channel.bind('my-event', function(data) {

    const li = document.createElement("li");
    const div1 = document.createElement("div");
    const div2 = document.createElement("div");
    const h6 = document.createElement("h6");
    const span = document.createElement("span");
    li.className  = 'list-group-item border-0 d-flex justify-content-between ps-0 pe-0 mb-2 border-radius-lg';
    div1.className = 'd-flex align-items-center';
    div2.className = 'd-flex flex-column';
    h6.className = 'mb-1 text-dark text-sm';
    span.className = 'text-xs';


    li.appendChild(div1);
    div1.appendChild(div2);
    div2.appendChild(h6);
    div2.appendChild(span);


    const text1 = document.createTextNode("PEMBIAYAAN MIKRO PERUMAHAN NEW");
    h6.appendChild(text1);
    const text2 = document.createTextNode("Bali (02 Aug 2021 - 18 Aug 2021)");
    span.appendChild(text2);

    const element = document.getElementById("jadwal");
    element.prepend(li);

    li.classList.add("box", "new-box");
  
});
</script>


<div class="container-fluid py-4">
<div class="row">
  <div class="col-12 col-xl-8">
  <div class="row">
    <div class="col-xl-6 col-sm-6 mb-xl-0 mb-4">
      <div class="card">
        <div class="card-body p-3">
          <div class="row">
            <div class="col-8">
              <div class="numbers">
                <p class="text-sm mb-0 text-capitalize font-weight-bold">Jumlah Kurmod</p>
                <h3 class="font-weight-bolder mb-0">
                  <?php echo $kurmod;?>
                </h3>
              </div>
            </div>
            <div class="col-4 text-end">
                <img src="<?php echo $url;?>/img/kurmod.png" width="100%">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-6 col-sm-6 mb-xl-0 mb-6">
      <div class="card">
        <div class="card-body p-3">
          <div class="row">
            <div class="col-8">
              <div class="numbers">
                <p class="text-sm mb-0 text-capitalize font-weight-bold">Pelatihan Tahun Ini</p>
                <h3 class="font-weight-bolder mb-0">
                  <?php echo $pelatihan;?>
                </h3>
              </div>
            </div>
            <div class="col-4 text-end">
                <img src="<?php echo $url;?>/img/pelatihan.png" width="100%">
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="row mt-4">
    <div class="col-sm-6">
      <div class="card overflow-hidden">
        <div class="card-header p-3 pb-0">
          <p class="text-sm mb-0 text-capitalize font-weight-bold">Capaian Peserta Pelatihan</p>
          <h5 class="font-weight-bolder mb-0">
            0
          </h5>
        </div>
        <div class="card-body p-0">
          <div class="chart">
            <canvas id="peserta" class="chart-canvas" height="100" width="213" style="display: block; box-sizing: border-box; height: 100px; width: 213.3px;"></canvas>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="card bg-gradient-dark">
        <div class="p-3 pb-0" style="color: #fff">
          <p class="text-sm mb-0 text-capitalize font-weight-bold">Capaian Pelatihan</p>
          <h5 class="font-weight-bolder mb-0" style="color: #fff">
            0
          </h5>
        </div>
        <div class="card-body p-0">
          <div class="chart">
            <canvas id="pelatihan" width="100" height="213" style="display: block; box-sizing: border-box; height: 100px; width: 213.3px;"></canvas>
          </div>
        </div>
      </div>
    </div>    
    </div>
    <div class="row mt-4">
    <div class="col-sm-6">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <div class="row">
            <div class="col-md-6">
              <h6 class="mb-0">Superspesialis</h6>
            </div>
          </div>
        </div>
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
              <div class="d-flex">
                <div class="d-flex align-items-center">
                  <button class="btn btn-icon-only btn-rounded btn-outline-info mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fa fa-graduation-cap" aria-hidden="true"></i></button>
                  <div class="d-flex flex-column">
                    <h6 class="mb-1 text-dark text-sm">Alumni</h6>
                    <span class="text-xs">Tahun Lulus 2019-2020</span>
                  </div>
                </div>
                <div class="d-flex align-items-center text-info text-gradient text-sm font-weight-bold ms-auto">
                  <?php echo Karyasiswa::model()->reportLulus();?>
                </div>
              </div>
              <hr class="horizontal dark mt-3 mb-2">
            </li>
            <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
              <div class="d-flex">
                <div class="d-flex align-items-center">
                  <button class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fa fa-tasks" aria-hidden="true"></i></button>
                  <div class="d-flex flex-column">
                    <h6 class="mb-1 text-dark text-sm">On Going</h6>
                    <span class="text-xs">Perkiraan Tahun Lulus 2018-2019</span>
                  </div>
                </div>
                <div class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold ms-auto">
                  <?php echo Karyasiswa::model()->reportGoing();?>
                </div>
              </div>
              <hr class="horizontal dark mt-3 mb-2">
            </li>
            <li class="list-group-item border-0 justify-content-between ps-0 mb-2 border-radius-lg">
              <div class="d-flex">
                <div class="d-flex align-items-center">
                  <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></button>
                  <div class="d-flex flex-column">
                    <h6 class="mb-1 text-dark text-sm">Perlu Perhatian</h6>
                    <span class="text-xs">Mulai Kuliah Tahun 2015</span>
                  </div>
                </div>
                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                  <?php echo Karyasiswa::model()->reportBermasalah();?>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>  
    <div class="col-sm-6">
      <div class="card h-100">
        <div class="card-header pb-0 p-3">
          <div class="row">
            <div class="col-md-6">
              <h6 class="mb-0">Data Pegawai</h6>
            </div>
            <div class="col-md-6">
              <div class="pull-right">Total <?php echo $pegawai;?></div>
            </div>
          </div>
        </div>
        <div class="card-body p-3">
          <ul class="list-group">
            <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
              <div class="d-flex">
                <div class="d-flex align-items-center">
                  <button class="btn btn-icon-only btn-rounded btn-outline-info mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fa fa-user-tie" aria-hidden="true"></i></button>
                  <div class="d-flex flex-column">
                    <h6 class="mb-1 text-dark text-sm">PNS</h6>
                  </div>
                </div>
                <div class="d-flex align-items-center text-info text-gradient text-sm font-weight-bold ms-auto">
                  <?php echo Pegawai::model()->reportPns();?>
                </div>
              </div>
              <hr class="horizontal dark mt-3 mb-2">
            </li>
            <li class="list-group-item border-0 justify-content-between ps-0 pb-0 border-radius-lg">
              <div class="d-flex">
                <div class="d-flex align-items-center">
                  <button class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fa fa-user" aria-hidden="true"></i></button>
                  <div class="d-flex flex-column">
                    <h6 class="mb-1 text-dark text-sm">Non PNS</h6>
                  </div>
                </div>
                <div class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold ms-auto">
                  <?php echo Pegawai::model()->reportNonPns();?>
                </div>
              </div>
              <hr class="horizontal dark mt-3 mb-2">
            </li>
            <li class="list-group-item border-0 justify-content-between ps-0 mb-2 border-radius-lg">
              <div class="d-flex">
                <div class="d-flex align-items-center">
                  <button class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 p-3 btn-sm d-flex align-items-center justify-content-center"><i class="fa fa-user-plus" aria-hidden="true"></i></button>
                  <div class="d-flex flex-column">
                    <h6 class="mb-1 text-dark text-sm">KI</h6>
                  </div>
                </div>
                <div class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold ms-auto">
                  <?php echo Pegawai::model()->reportKI();?>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>  
    </div>
  </div>


  <div class="col-12 col-xl-4">
    <div class="row">

      <div class="card">
        <div class="card-header p-3 pb-0">
          <p class="text-sm mb-0 text-capitalize font-weight-bold"><?php echo date('F Y');?></p>
        </div>

        <div class="card-body p-3">
          <div class="p-3 pb-0">
          <div style="display: flex;">
            <?php 
            $now = date('Y-m-d');

            $dateNow1=date_create($now);
            date_add($dateNow1,date_interval_create_from_date_string("-3 days"));
            $dateNow2=date_create($now);
            date_add($dateNow2,date_interval_create_from_date_string("-2 days"));
            $dateNow3=date_create($now);
            date_add($dateNow3,date_interval_create_from_date_string("-1 days"));
            $dateNow4=date_create($now);
            date_add($dateNow4,date_interval_create_from_date_string("1 days"));
            $dateNow5=date_create($now);
            date_add($dateNow5,date_interval_create_from_date_string("2 days"));
            $dateNow6=date_create($now);
            date_add($dateNow6,date_interval_create_from_date_string("3 days"));    
            ?>
            <div class="no-today col-2 ">
              <b><?php echo date_format($dateNow1,"d");;?></b><br>
              <?php echo substr(date_format($dateNow1,"D"),0,1);;?>
            </div>
            <div class="no-today col-2 ">
              <b><?php echo date_format($dateNow2,"d");;?></b><br>
              <?php echo substr(date_format($dateNow2,"D"),0,1);;?>
            </div>
            <div class="no-today col-2 ">
              <b><?php echo date_format($dateNow3,"d");;?></b><br>
              <?php echo substr(date_format($dateNow3,"D"),0,1);;?>
            </div>

            <div class="today col-2">
              <b><?php echo date('d');?></b><br>
              <?php echo substr(date('D'),0,1);?>
            </div>

            <div class="no-today col-2 ">
              <b><?php echo date_format($dateNow4,"d");;?></b><br>
              <?php echo substr(date_format($dateNow4,"D"),0,1);;?>
            </div>
            <div class="no-today col-2 ">
              <b><?php echo date_format($dateNow5,"d");;?></b><br>
              <?php echo substr(date_format($dateNow5,"D"),0,1);;?>
            </div>
            <div class="no-today col-2 ">
              <b><?php echo date_format($dateNow6,"d");;?></b><br>
              <?php echo substr(date_format($dateNow6,"D"),0,1);;?>
            </div>

          </div>
          </div>
        </div>
        <hr class="horizontal dark mt-3">
        <div class="card-header p-3 pb-0">
          <div class="pull-right text-sm mb-0 text-capitalize font-weight-bold"><a href="<?php echo $url;?>/pelaksanaanPlth/index">View All</a></div>
          <p class="text-sm mb-0 text-capitalize font-weight-bold">Schedule</p>
        </div>
        <div class="card-body p-3">
         <div class="p-3 pb-0">
              <?php $ID = Yii::app()->db->createCommand("SELECT pplth_id FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); ?>
              <?php $nmPelatihan = Yii::app()->db->createCommand("SELECT nm_pelatihan FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); ?>
              <?php $bidang = Yii::app()->db->createCommand("SELECT bidang FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); ?>
              <?php if ($nmPelatihan != '') { ?>
          <div class="SchedulueNow">
              <div class="labelSchedul"></div>
              <div style="padding: 10px;">
              <?php 
                echo "<b><a href='".$url."/pelaksanaanPlth/viewDetail/".$ID."'>".$nmPelatihan."</a></b><br>";
                echo "Bidang ".Yii::app()->db->createCommand("SELECT nama_bidang FROM bidang WHERE bd_id = $bidang")->queryScalar()."<br><br>"; 
                echo Yii::app()->db->createCommand("SELECT lokasi_plthn FROM pelaksanaan_plth WHERE tgl_plaksana = '".date('Y-m-d')."'")->queryScalar(); 
              ?></div>
          </div>
              <?php } ?>
          </div>
        </div>
      </div>
    </div>

    <div class="row mt-4">
    <div class="card h-100">
      <div class="card-header pb-0 p-3">
        <h6 class="mb-0">Jadwal Pelatihan Bulan <?php echo date('F');?></h6>
      </div>
      <div class="card-body p-3">
        <ul class="list-group" style="overflow: auto;height: 500px" id="jadwal">
          <?php foreach (PelaksanaanPlth::model()->findAll('tgl_plaksana LIKE "%'.$dateNow.'%" ORDER BY tgl_plaksana ASC') AS $pelatihan) { ?>
          <li class="list-group-item border-0 d-flex justify-content-between ps-0 pe-0 mb-2 border-radius-lg box">
            <div class="d-flex align-items-center">
              <div class="d-flex flex-column">
                <h6 class="mb-1 text-dark text-sm"><?php echo $pelatihan->nm_pelatihan;?></h6>
                <span class="text-xs"><?php echo $pelatihan->lokasi_plthn." (".PelaksanaanPlth::model()->getWaktu($pelatihan->tgl_plaksana,$pelatihan->tgl_akhir).")";?></span>
              </div>
            </div>
            <div class="d-flex">
              <div class="icon icon-shape icon-sm bg-gradient-dark shadow text-center" title="<?php echo PelaksanaanPlth::model()->status($pelatihan->sts_platihan);?>">
                <?php echo PelaksanaanPlth::model()->statusIcon($pelatihan->sts_platihan);?>
              </div>
            </div>
          </li>
          <?php } ?>
        </ul>
      </div>
    </div>
  </div>  
  </div>  
</div>  
</div>

  <script type="text/javascript">
var ctx = document.getElementById("pelatihan").getContext("2d");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
        datasets: [{
                label: 'Pelatihan',
                tension: 0.4,
                borderWidth: 0,
                borderRadius: 4,
                borderSkipped: false,
                backgroundColor: "#fff",
                data: [
                  <?php
                  for ($i=1; $i <= 12; $i++) { 
                      echo PelaksanaanPlth::model()->monthReport($year,$i).",";
                  }
                  ?>
                ],
                maxBarThickness: 6,
            },],
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
            y: {
                grid: {
                  drawBorder: false,
                  display: false,
                  drawOnChartArea: false,
                  drawTicks: false,
                },
                ticks: {
                    beginAtZero: true,
                    suggestedMin: 0,
                    padding: 15,
                    color:'#fff'
                }
            },
            x: {
                grid: {
                  drawBorder: false,
                  display: false,
                  drawOnChartArea: false,
                  drawTicks: false
                },
                ticks: {
                  display: true,
                    color:'#fff'
                },
            },
        },
    }
});


// Status Chart
var gradientStroke1 = ctx.createLinearGradient(0, 230, 0, 50);

gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.02)');
gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

var ctx = document.getElementById("peserta").getContext("2d");
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
        datasets: [{
        label: 'Peserta Pelatihan',
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
        // data: [
        //   <?php
        //   for ($i=1; $i <= 12; $i++) { 
        //       echo PesertaPlthn::model()->monthReport($year,$i).",";
        //   }
        //   ?>
        // ],
        tension: 0.5,
        borderWidth: 0,
        pointRadius: 0,
        borderWidth: 2,
        backgroundColor: gradientStroke1,
        borderColor: '#FF9800',
        borderWidth: 2,
        maxBarThickness: 6,
        fill: true
    }]
    },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#9ca2b7'
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#9ca2b7'
            }
          },
        },
      },
    });

    var gradientStroke1 = ctx1.createLinearGradient(0, 230, 0, 50);

    gradientStroke1.addColorStop(1, 'rgba(203,12,159,0.02)');
    gradientStroke1.addColorStop(0.2, 'rgba(72,72,176,0.0)');
    gradientStroke1.addColorStop(0, 'rgba(203,12,159,0)'); //purple colors

    var ctx2 = document.getElementById("chart-line-2").getContext("2d");

    new Chart(ctx1, {
      type: "line",
      data: {
        labels: ["Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Visitors",
          tension: 0.5,
          borderWidth: 0,
          pointRadius: 0,
          borderColor: "#cb0c9f",
          borderWidth: 2,
          backgroundColor: gradientStroke1,
          data: [50, 45, 60, 60, 80, 65, 90, 80, 100],
          maxBarThickness: 6,
          fill: true
        }],
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
          legend: {
            display: false,
          }
        },
        interaction: {
          intersect: false,
          mode: 'index',
        },
        scales: {
          y: {
            grid: {
              drawBorder: false,
              display: false,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#9ca2b7'
            }
          },
          x: {
            grid: {
              drawBorder: false,
              display: true,
              drawOnChartArea: true,
              drawTicks: false,
              borderDash: [5, 5]
            },
            ticks: {
              display: true,
              padding: 10,
              color: '#9ca2b7'
            }
          },
        },
      },
    });
</script>