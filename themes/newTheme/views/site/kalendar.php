<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Kegiatan";
$this->breadcrumbs=array(
	'Kegiatan'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <div class="col-6">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/kalendar_pelatihan.png" width="50%%">
            	</div>
            	<b>Kalendar Pelatihan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/pelaksanaanPlth/index" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-6">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/kalendar_kegaiatan.png" width="50%%">
            	</div>
            	<b>Kalendar Kegiatan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/kegiatan" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

	  </div>
</div>