<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Kerjasama";
$this->breadcrumbs=array(
	'Kerjasama'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <div class="col-6">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/dokumen.png" width="50%">
            	</div>
            	<b>Dokumen</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/viewKerjasama?id=dokumen" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

      <div class="col-6">
      <div class="card mb-4">
        <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/pendidikan.png" width="50%">
            	</div>
            	<b>Monitoring Pendidikan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/siswa/admin" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>
        
		</div>		
</div>