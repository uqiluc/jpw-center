<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
    'Login',
);

  $baseUrl = Yii::app()->theme->baseUrl; 
  $cs = Yii::app()->getClientScript();
  Yii::app()->clientScript->registerCoreScript('jquery');
?>

<main class="main-content  mt-0">
<section>
  <div class="page-header min-vh-100">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-lg-5 col-md-6 d-flex flex-column mx-auto">
          <div class="card card-plain mt-5">
            <div class="card-header pb-0 text-left bg-transparent">
              <h3 class="font-weight-bolder text-info text-gradient">JPW CENTER</h3>
              <p class="mb-0">Enter your account to sign in</p>
            </div>
            <div class="card-body">
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'login-form',
                    'enableClientValidation'=>true,
                    'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                    ),
                )); ?>
                <label>Username</label>
                <div class="mb-3">
                  <?php echo $form->textField($model,'username', array('class' => 'form-control', 'placeholder'=>'Enter your username', 'aria-label'=>'username','aria-describedby'=>'username-addon')); ?>
                </div>
                <label>Password</label>
                <div class="mb-3">
                  <?php echo $form->passwordField($model,'password', array('class' => 'form-control','placeholder'=>'Enter your Password', 'aria-label'=>'password','aria-describedby'=>'password-addon')); ?>
                </div>
                <div class="form-check form-switch">
                  <?php echo $form->checkBox($model,'rememberMe',array('class'=>'form-check-input')); ?>
                  <label class="form-check-label" for="LoginForm_rememberMe">Remember me</label>
                </div>
                <div class="text-center">
                  <?php echo CHtml::submitButton('Sign in', array('class' =>'btn bg-gradient-info w-100 mt-4 mb-0')); ?>
                  - Or - <br>
                  <div class="g-signin2" data-onsuccess="onSignIn" data-theme="light" data-width="300" data-height="50" data-longtitle="true"></div>
                </div>
               <?php $this->endWidget(); ?>    
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="oblique position-absolute top-0 h-100 d-md-block d-none me-n8">
            <div class="oblique-image bg-cover position-absolute fixed-top ms-auto h-100 z-index-0 ms-n6" style="background-image:url('<?php echo $baseUrl; ?>/img/BgLogin.jpg')"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  </main>
<script>

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();

  window.location.href="https://cikkan.com/project/jpw/site/autoLogin?username="+profile.getEmail();

  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function myFunction() {
    var x = document.getElementById("LoginForm_password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>