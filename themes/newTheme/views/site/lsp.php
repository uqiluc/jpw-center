<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="LSP";
$this->breadcrumbs=array(
	'LSP'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Administrasi</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/lspType?id=administrasi" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Skema</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/lspType?id=skema" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>
        
        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Sertifikasi</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/lspType?id=sertifikasi" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Manajemen Mutu</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/lspType?id=mutu" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Regulasi</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/lspType?id=regulasi" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/folder.png" width="50%">
            	</div>
            	<b>Dokumentasi</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/lspType?id=dokumentasi" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>		

	  </div>
</div>