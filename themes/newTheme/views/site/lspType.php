<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$type = $_GET['id'];

$this->pageTitle="LSP";
$this->breadcrumbs=array(
	'LSP'=>array('index'),
	$type
);

?>
<div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
            	<div class="pull-right">
				<?php 
				echo CHtml::link('Tambah Baru',
				array('naskahDinas/lsp?type='.$type),
				array('class' => 'btn btn-default','title'=>'Tambah Baru'));
				?>
				<?php 
				echo CHtml::link('Daftar LSP',
				array('/lsp'),
				array('class' => 'btn btn-default','title'=>'Daftar LSP'));
				?>							
	            </div>
	            <h6>LSP - <?php echo NaskahDinas::model()->getJns($type);?></h6>
	        </div>
	        </div>
	      </div>
	    </div>
</div>

<div class="container-fluid py-2">
    <div class="row">
    <div class="col-12">
        <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
	              <div class="dataTable-container">
		         	<?php $this->widget('zii.widgets.grid.CGridView', array(
						'id'=>'naskah-dinas-grid',
						'dataProvider'=>$model->search($type),
						'filter'=>$model,
						'summaryText'=>'<div class="summaryCustom">Showing {start} to {end} of {count} entries</div>',
						'itemsCssClass'=>'table table-flush dataTable-table',
						'columns'=>array(
							array(
								'name'=>'nama_file','type'=>'raw',
							  	'value'=>'CHtml::link($data->nama_file,array("naskahDinas/view","id"=>$data->nsk_id),array("class"=>"badge badge-sm bg-gradient-secondary"))',
				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'jns_dokumen','type'=>'raw','value'=>'$data->jns_dokumen',
  				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'bidang','type'=>'raw','value'=>'$data->Bidang->nama_bidang',
								'filter'=>CHtml::listData(Bidang::model()->findAll(),'bd_id', 'nama_bidang'),
  				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'name'=>'tgl_dokumen','type'=>'raw','value'=>'$data->tgl_dokumen',
  				  				'headerHtmlOptions'=>array('class'=>'text-uppercase text-secondary text-xxs font-weight-bolder opacity-7')
							),
							array(
								'header'=>'Aksi',
								'class'=>'CButtonColumn',
								'template'=>'{update}{delete}',
								'htmlOptions'=>array('width'=>'150px', 
								'style' => 'text-align: center;'),
							),
						),
					)); ?>
		 </div>
		</div>
	</div>
	</div>
</div>