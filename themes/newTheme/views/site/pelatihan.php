<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Pelatihan";
$this->breadcrumbs=array(
	'Pelatihan'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/Bidang/jalanjembatan.png" width="100%">
            	</div>
            	<b>Bidang Jalan & Jembatan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/kurmodDetail/admin?type=6" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>
        
        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/Bidang/Perumahan.png" width="100%">
            	</div>
            	<b>Bidang Perumahan</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/kurmodDetail/admin?type=8" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

        <div class="col-4">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/Bidang/Piw.png" width="100%">
            	</div>
            	<b>Bidang PIW</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/kurmodDetail/admin?type=10" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>
	  </div>
</div>