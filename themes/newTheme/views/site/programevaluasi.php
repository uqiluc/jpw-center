<?php
/* @var $this MediaController */
/* @var $dataProvider CActiveDataProvider */

$baseUrl = Yii::app()->baseurl;

$this->pageTitle="Program & Evaluasi";
$this->breadcrumbs=array(
	'Program & Evaluasi'=>array('index')
);

?>

 <div class="container-fluid py-4">
      <div class="row">

        <div class="col-6">
          <div class="card mb-4">
            <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/dokumen.png" width="50%">
            	</div>
            	<b>Program</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/viewKerjasama?id=dokumen" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>

      <div class="col-6">
      <div class="card mb-4">
        <div class="card-body px-0 pt-0 pb-2 text-center">
            	<div style="padding: 30px;">
	            	<img src="<?php echo $baseUrl; ?>/img/evaluasi.png" width="50%">
            	</div>
            	<b>Evaluasi</b><br>
          		<hr class="horizontal dark my-1">
          		<br>
            	<a href="<?php echo $baseUrl;?>/evaluasi" class="btn btn-primary">Pilih</a>
			</div>
		  </div>
		</div>
        
		</div>		
</div>