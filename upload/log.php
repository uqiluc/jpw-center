<?php
session_start();
include 'vendor/autoload.php';
 
// setting config untuk layanan akses ke google drive
$client = new Google_Client();
$client->setAuthConfig("oauth-credentials.json");
$client->addScope("https://www.googleapis.com/auth/drive");
$service = new Google_Service_Drive($client);
 
// jika token belum ada, maka lakukan login via oauth
$authUrl = $client->createAuthUrl();
header("Location:".$authUrl);
?>